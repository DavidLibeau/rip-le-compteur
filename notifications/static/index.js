var subscriptionButton = document.getElementById('subscriptionButton');

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('/static/service-worker.js').then(function (registration) {
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
            subscriptionButton.removeAttribute('disabled');
            return registration.pushManager.getSubscription();
        }, function (err) {
            console.log('ServiceWorker registration failed: ', err);
        }).then(function (subscription) {
            if (subscription) {
                console.log('Already subscribed', subscription.endpoint);
                setUnsubscribeButton();
            } else {
                setSubscribeButton();
            }
        });;
    });
}

function subscribe() {
    if(confirm("(Beta) Attention : le service de notifications permet d'envoyer à votre navigateur des notifications même quand vous n'avez pas le site ouvert. Pour ce faire, il utilise un service tiers fourni automatiquement par votre navigateur. Par exemple, si vous utilisez Google Chrome ou que vous êtes sur l'application Android, ce service de notification va utiliser les serveurs de Google. Si vous utilisez le navigateur Firefox, les serveurs de Mozilla seront utilisés. Vous pouvez en savoir plus en consultant la page FAQ ou les mentions légales de RIP, le compteur.\nConfirmez-vous votre souhait de vous abonner aux notifications ? Vous pourrez vous désabonner à tout moment en un clic.")){
        navigator.serviceWorker.register('/static/service-worker.js').then(async function (registration) {
            // Get the server's public key
            const response = await fetch('./vapidPublicKey');
            const vapidPublicKey = await response.text();
            // Chrome doesn't accept the base64-encoded (string) vapidPublicKey yet
            // urlBase64ToUint8Array() is defined in /tools.js
            const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
            // Subscribe the user
            return registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: convertedVapidKey
            });
        }).then(function (subscription) {
            console.log('Subscribed', subscription.endpoint);
            var endpoint=new URL(subscription.endpoint);
            alert("Vous avez bien été abonné aux notifications de RIP, le compteur via le serveur tiers "+endpoint.hostname+" !");
            return fetch('register', {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    subscription: subscription
                })
            });
        }).then(setUnsubscribeButton);
    }
}

function unsubscribe() {
    navigator.serviceWorker.register('/static/service-worker.js').then(function (registration) {
            return registration.pushManager.getSubscription();
        }).then(function (subscription) {
            return subscription.unsubscribe()
                .then(function () {
                    console.log('Unsubscribed', subscription.endpoint);
                    alert("Vous avez bien supprimé votre abonnement aux notifications !");
                    return fetch('unregister', {
                        method: 'post',
                        headers: {
                            'Content-type': 'application/json'
                        },
                        body: JSON.stringify({
                            subscription: subscription
                        })
                    });
                });
        }).then(setSubscribeButton);
}

function setSubscribeButton() {
    subscriptionButton.onclick = subscribe;
    subscriptionButton.textContent = "S\'abonner aux notifications";
}

function setUnsubscribeButton() {
    subscriptionButton.onclick = unsubscribe;
    subscriptionButton.textContent = "Se désabonner des notifications";
}