self.addEventListener('install', function (evt) {
    console.log('The service worker is being installed.');
});

self.addEventListener('push', function (event) {
    var payload = event.data ? event.data.text() : "Nouvelle notification !";
    event.waitUntil(self.registration.showNotification("RIP, le compteur.", {
        body: payload,
        lang: "fr",
        icon: "https://rip-le-compteur.dav.li/img/icon-192.png",
    }));
});

self.addEventListener('pushsubscriptionchange', function (event) {
    console.log('Subscription expired');
    event.waitUntil(
        self.registration.pushManager.subscribe({
            userVisibleOnly: true
        })
        .then(function (subscription) {
            console.log('Subscribed after expiration', subscription.endpoint);
            return fetch('register', {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    endpoint: subscription.endpoint
                })
            });
        })
    );
});

self.addEventListener('notificationclick', function (event) {
    event.waitUntil(
        self.clients.matchAll().then(function (clientList) {
            return self.clients.openWindow("https://rip-le-compteur.dav.li");
        })
    );
});