var express = require('express');
var https = require('https');
var path = require('path');
var fs = require('fs');
var app = express();
var webPush = require('web-push');
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var tokens = require("./tokens.json");
process.env.VAPID_PUBLIC_KEY = tokens.publicKey;
process.env.VAPID_PRIVATE_KEY = tokens.privateKey;

if (!process.env.VAPID_PUBLIC_KEY || !process.env.VAPID_PRIVATE_KEY) {
    console.log("You must set the VAPID_PUBLIC_KEY and VAPID_PRIVATE_KEY environment variables. You can use the following ones:");
    console.log(webPush.generateVAPIDKeys());
    return;
}

webPush.setVapidDetails(
    "https://notifications.rip-le-compteur.dav.li",
    process.env.VAPID_PUBLIC_KEY,
    process.env.VAPID_PRIVATE_KEY
);


var subscriptions = JSON.parse(fs.readFileSync('bdd_abonnements.json', 'utf8'));;
var notifications = JSON.parse(fs.readFileSync('bdd_notifications.json', 'utf8'));;


function sendNotification(subscription) {
    webPush.sendNotification(subscription, payload)
        .then(function () {
            //console.log('Push Application Server - Notification sent to ' + subscription.endpoint);
        }).catch(function () {
            //console.log('ERROR in sending Notification, endpoint removed ' + subscription.endpoint);
            delete subscriptions[subscription.endpoint];
        });
}


var payload = "";
setInterval(function () {
    var d = new Date();
    var h = d.getHours();
    if(h>7 && h<22){
        https.get("https://rip-le-compteur.dav.li/notifications?format=json", (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end', () => {
                if(data && data[0]!="<"){
                    var remote_notifications = JSON.parse(data);
                    for(var id in remote_notifications){
                        if(notifications[id]){
                            //console.log("Notif déjà publiée : "+id);
                        }else{
                            console.log("Nouvelle notification : "+id);
                            payload=remote_notifications[id].title.replace("&#160;"," ")+" !";
                            Object.values(subscriptions).forEach(sendNotification);
                        }
                    }
                    notifications=remote_notifications;
                    fs.writeFile("bdd_notifications.json", JSON.stringify(remote_notifications), 'utf8', function (err) {
                        if (err) {
                            console.log("Erreur lors de la sauvegrade de la base de données notifications.");
                            return console.log(err);
                        }
                    });
                }
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    }
}, 300000); //5 minutes



app.use(function forceLiveDomain(req, res, next) {
    var host = req.get('Host');
    if (host === 'serviceworker-cookbook.herokuapp.com') {
        return res.redirect(301, "https://notifications.rip-le-compteur.dav.li");
    }
    return next();
});

app.use(function forceSSL(req, res, next) {
    var host = req.get('Host');
    var localhost = 'localhost';

    if (host.substring(0, localhost.length) !== localhost) {
        res.header('Strict-Transport-Security', 'max-age=15768000');
        if (req.headers['x-forwarded-proto'] !== 'https') {
            return res.redirect('https://' + host + req.url);
        }
    }
    return next();
});

app.use(function corsify(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept');
    next();
});

app.use(function setServiceWorkerHeader(req, res, next) {
    var file = req.url.split('/').pop();
    if (file === 'service-worker.js' || file === 'worker.js') {
        res.header('Cache-control', 'public, max-age=0');
    }
    next();
});


var port = process.env.PORT || 3003;
var ready = new Promise(function willListen(resolve, reject) {
    app.listen(port, function didListen(err) {
        if (err) {
            reject(err);
            return;
        }
        console.log('app.listen on http://localhost:%d', port);
        resolve();
    });
});


app.get('/vapidPublicKey', function (req, res) {
    res.send(process.env.VAPID_PUBLIC_KEY);
});

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, "/", "index.html"));
});

app.use('/static', express.static('static'));

app.post('/register', function (req, res) {
    var subscription = req.body.subscription;
    if (!subscriptions[subscription.endpoint]) {
        //console.log('Subscription registered ' + subscription.endpoint);
        subscriptions[subscription.endpoint] = subscription;
        fs.writeFile("bdd_abonnements.json", JSON.stringify(subscriptions), 'utf8', function (err) {
            if (err) {
                console.log("Erreur lors de la sauvegrade de la base de données abonnements.");
                return console.log(err);
            }
        });
    }
    res.sendStatus(201);
});

app.post('/unregister', function (req, res) {
    var subscription = req.body.subscription;
    if (subscriptions[subscription.endpoint]) {
        //console.log('Subscription unregistered ' + subscription.endpoint);
        delete subscriptions[subscription.endpoint];
        fs.writeFile("bdd_abonnements.json", JSON.stringify(subscriptions), 'utf8', function (err) {
            if (err) {
                console.log("Erreur lors de la sauvegrade de la base de données abonnements.");
                return console.log(err);
            }
        });
    }
    res.sendStatus(201);
});