# rip-le-compteur

RIP, le compteur.

## Open data

L'intégralité des données sont en libre accès :
- la base de données compteur : https://rip-le-compteur.dav.li/data.txt
- la base de données erreur : https://rip-le-compteur.dav.li/error.txt

Ainsi que via l'API :
- le cache : https://rip-le-compteur.dav.li/api.php?endpoint=cache
- ou encore le total : https://rip-le-compteur.dav.li/api.php?endpoint=total

## Contributions au code

Toutes les contributions sont les bienvenues.

Pour éviter les contributions inutilisées, merci de demander l'attribution d'une issue ou créez une issue avant de commencer à coder. Réservez les Merge Request sans attribution d'issue pour les changements vraiment mineurs (quelques lignes de code). Merci !