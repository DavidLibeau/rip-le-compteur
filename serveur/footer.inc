    <footer>
        Données CC-BY David Libeau &amp; <a href="/statistiques_contributions"><?php echo($contribUnique); ?></a> contributeur·rice·s / Code GNU AGPLv3 (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank" rel="noopener noreferrer">open source</a>)
    </footer>
    <script src="//dav.li/jquery/3.4.1.min.js"></script>
    <script src="//dav.li/jquery/ui/jquery-ui.min.js"></script>
    <script src="/script.js"></script>
    <?php if(!isset($isXml) && !$isXml){ ?>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "url": "https://rip-le-compteur.dav.li/",
      "logo": "https://rip-le-compteur.dav.li/img/icon-192.png"
    }
    </script>
    <?php if(!empty($title)){ ?>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Compteur RIP ADP",
        "item": "https://rip-le-compteur.dav.li/"
      },{
        "@type": "ListItem",
        "position": 2,
        "name": "<?php echo($title); ?>",
        "item": "https://rip-le-compteur.dav.li<?php echo($_SERVER["REQUEST_URI"]); ?>"
      }]
    }
    </script>
    <?php } ?>
    <script>
        $(function() {
            //service worker
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/service-worker.js');
                navigator.serviceWorker.onmessage = function (evt) {
                    var message = JSON.parse(evt.data);
                    if(message.url==window.location.href){
                        //console.log(message);
                        if(message.body){
                            var parser = new DOMParser();
                            var doc = parser.parseFromString(message.body, "text/html");
                            var messageCacheId=$(doc).find('[name="cacheId"]').attr("content");
                            var messageBody = $(doc).find("body").html();
                            console.log($('[name="cacheId"]').attr("content")+":CaheId:cache");
                            console.log(messageCacheId+":CaheId:fetch");
                            if(messageBody && messageCacheId!=$('[name="cacheId"]').attr("content")){
                                $("body").html(messageBody);
                                afficherAlerte("Page rafraîchie");
                            }
                        }
                    }
                }
            }
        });
    </script>
    <?php } ?>