<?php 
include("preprocess.php");
$title="Informations sur le référendum ADP";
$description="Toutes les informations utiles sur le référendum ADP.";
$custom_header='<link  href="lib/fotorama-4.6.4/fotorama.css" rel="stylesheet">';
include("head.inc");

$now=new DateTime("now");
if(intval($now->format("H"))>10){
    $aujourdhuiapres10h=$now->format("d\/m\/y");
}else{
    $hier=$now;
    $hier=$hier->modify("-1 day");
    $aujourdhuiapres10h=$hier->format("d\/m\/y");
}

?>
<style>
    .fotorama__arr, .fotorama__fullscreen-icon {
        background-color: #3c3c3c;
    }
    .fotorama__stage{
        background-color:#f0f0f0;
    }
</style>

<main>

    <section class="centered">
        <h2><img src="twemoji/signer.png" alt="emoji signer" /> Informations sur le RIP</h2>
        <p>Questions/réponses</p>
    </section>
    <section>
    <?php
        $faqReferendum=[
            "Sur quoi porte le RIP~?"=>"<p>Le référendum ADP porte sur la privatisation des aéroports de Paris. Cette proposition de loi vise à affirmer le caractère de service public national de l’exploitation des aérodromes de Paris. Elle s'oppose à la <a href=\"https://www.economie.gouv.fr/plan-entreprises-pacte\" target=\"_blank\" rel=\"noopener noreferrer\">loi PACTE</a> (aussi nommée <a href=\"http://www.assemblee-nationale.fr/dyn/15/dossiers/Croissance_transformation_des_entreprises\" target=\"_blank\" rel=\"noopener noreferrer\">projet de loi relatif à la croissance et la transformation des entreprises</a>) adoptée en avril 2019.</p><p>Le référendum ADP est un référendum d'initiative partagée (RIP) lancé par un groupe de parlementaire de l'opposition. Cette proposition de loi référendaire a été déposée le 10 avril 2019 et validée le 9 mai 2019 par le Conseil constitutionnel.</p>",
            "Qu'est-ce qu'un référendum d'initiative partagée~?"=>"<p>Un <a href=\"https://www.vie-publique.fr/focus/decrypter-actualite/quoi-consiste-referendum-initiative-partagee.html\" target=\"_blank\" rel=\"noopener noreferrer\">référendum d'initiative partagée</a> est une proposition de loi d’un type particulier. Elle doit être signée par au moins un cinquième des parlementaires (députés et sénateurs), validée par le Conseil constitutionnel puis être signée par 10~% du corps électoral pour ensuite, si elle n'a pas été examinée par le parlement dans un délai de six mois, être transformé en référendum par le président de la République.</p><p>Entré en vigueur le 1er janvier 2015, le référendum d’initiative partagée a été prévu par la réforme constitutionnelle du 23 juillet 2008. Une loi ordinaire et une loi organique du 6 décembre 2013 ont fixé ses modalités d’organisation.</p>",
            "Combien de personnes ont signé le référendum ADP à ce jour~?"=>"<p>Le $aujourdhuiapres10h nous comptions ".number_format($total, 0, ',', '~')." signatures pour le référendump ADP, soit ".number_format($pourcentage, 2, ',', '~')."~% des 4~717~396 signatures nécessaires.</p><p>Le ministère de l'Intérieur n'ayant pas mis en place de compteur officiel, ce comptage est une estimation faite par le compteur collaboratif disponible sur <a href=\"https://rip-le-compteur.dav.li/\">rip-le-compteur.dav.li</a>.</p>",
            "Combien de signatures doivent être réunies au total~?"=>"<p>Le conseil Constitutionel a fixé le nombre de soutiens a aquérir à 4~717~396. Cela représente 10~% des électeurs inscrits sur les listes électorales.",
            "Quand se termine la pétition pour le référendum d'ADP~?"=>"<p>La pétition pour le référendum ADP se termine le 12 mars 2020.</p><p>Elle a été lancée le 13 juin 2019 et dure neuf mois au total.</p>",
            "Où peut-on signer le référendum contre la privatisation d'ADP~?"=>"<p>Pour soutenir le référendum et signer la proposition de loi, il faut se rendre sur le site web du ministère de l'Intérieur à l'adresse suivante : <a href=\"https://www.referendum.interieur.gouv.fr/soutien/etape-1\" target=\"_blank\" rel=\"noopener noreferrer\">www.referendum.interieur.gouv.fr</a>.</p>"
        ];
    ?>
        <div class="fotorama" data-width="100%" data-height="600" data-allowfullscreen="true" data-nav="thumbs">
            <img src="img/RIP_carrousel_ConseilConst/RIP_ADP_actualite_procedure.jpg" alt="Schema sur le référendum d'initiative partagée concernant ADP"/>
            <img src="img/RIP_carrousel_ConseilConst/RIP_definition.jpg" alt="Quelques informations sur le référendum d'initiative partagée"/>
            <img src="img/RIP_carrousel_ConseilConst/RIP_procedure.jpg" alt="Frise chronologique d'un référendum d'initiative partagée"/>
            <img src="img/RIP_carrousel_ConseilConst/RIP_deposer_soutien.jpg" alt="Tuto pour déposer un soutien sur le RIP"/>
        </div>
        <?php
        foreach($faqReferendum as $question => $reponse){
        ?>
            <h3><?php echo(str_replace("~","&nbsp;",$question)); ?></h3>
            <?php echo(str_replace("~","&nbsp;",$reponse)); ?>
        <?php
        }
        ?>
        <h3>Plus d'informations...</h3>
        <p>Retrouvez davantage d'informations en suivant ces liens :</p>
        <ul class="linkList">
            <li><a href="https://www.interieur.gouv.fr/RIP/Referendum-d-initiative-partagee-foire-aux-questions" target="_blank" rel="noopener noreferrer">Foire aux questions du ministère de l'Intérieur</a></li>
            <li><a href="http://www.assemblee-nationale.fr/15/propositions/pion1867.asp" target="_blank" rel="noopener noreferrer">La proposition de loi intégrale sur le site web de l'Assemblée Nationale</a></li>
            <li><a href="https://www.conseil-constitutionnel.fr/dossier-referendum-d-initiative-partagee-rip" target="_blank" rel="noopener noreferrer">Le dossier complet du Conseil constitutionel avec toutes ses décisions</a></li>
            <li><a href="http://www.lcp.fr/emissions/lcp-le-mag/294046-rip-le-referendum-impossible" target="_blank" rel="noopener noreferrer">Un reportage vidéo de LCP (La Chaine Parlementaire) : "R.I.P. : le référendum impossible ?"</a></li>
            <li><a href="https://fr.wikipedia.org/wiki/Projet_de_r%C3%A9f%C3%A9rendum_d%27initiative_partag%C3%A9e_sur_les_a%C3%A9roports_de_Paris" target="_blank" rel="noopener noreferrer">Page Wikipédia du projet de référendum d'initiative partagée sur les aéroports de Paris</a></li>
        </ul>
    </section>
    <section class="centered">
        <p><a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer" class="btn">Signer la pétiton <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [
    <?php
    $quetionNb=0;
    foreach($faqReferendum as $question => $reponse){
        if($quetionNb!=0){
            echo(",");
        }
        $quetionNb++;
        ?>
        {
            "@type": "Question",
            "name": "<?php echo(str_replace("~","&nbsp;",$question)); ?>",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "<?php echo(str_replace("~","&nbsp;",htmlspecialchars($reponse))); ?>"
            }
        }
        <?php
    }
    ?>]
}
</script>
<script src="lib/fotorama-4.6.4/fotorama.js"></script>
    <script>
        $(function() {
        });
    </script>
</body>

</html>