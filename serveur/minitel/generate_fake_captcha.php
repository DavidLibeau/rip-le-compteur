<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require_once("../lib/securimage/securimage.php");

$img = new Securimage();

if(!empty($_GET["content"])){
    $mot_captcha=$_GET["content"];
}else{
    $mot_captcha="RIP le compteur";
}


$img->captcha_type = Securimage::SI_CAPTCHA_CUSTOM;

if(!empty($_GET["sign"])){
    $img->image_signature = $_GET["sign"];
}else{
    $img->image_signature = "Startup Nation - référendum d'initiative partagée";
}

$img->font_ratio = 0.3;

if(!empty($_GET["w"]) && is_numeric($_GET["w"]) && !empty($_GET["h"]) && is_numeric($_GET["h"])){
    $img->image_width = $_GET["w"];
    $img->image_height  = $_GET["h"];
}else{
    $img->image_height = 150;
    $img->image_width  = 180 * M_E;
}

$img->show();