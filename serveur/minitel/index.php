<html>
    <head>
        <title>3615 le RIP</title>
        <link href="minitel/style.css" rel="stylesheet"/>
        <meta name="robots" content="noindex">
    </head>
    <body>
        <header>
            <h1>Référendum d'initiative partagée</h1>
        </header>
        <main>
            <h1><img src="minitel/generate_fake_captcha.php?content=<?php echo(number_format($total, 0, ',', '&nbsp;')); ?> signatures" alt="<?php echo(number_format($total, 0, ',', '&nbsp;')); ?> signatures" /></h1>
            <h2>Soit <?php echo(number_format($pourcentage, 2, ',', ' ')); ?>&nbsp;% des 4&nbsp;717&nbsp;396 signatures nécessaires!!!</h2>
            <marquee behavior="scroll" direction="left" id="description">Bienvenue sur mon site</marquee>
            <br/>
            <br/>
            <br/>
            <table>
                <tr>
                    <td>Vous pouvez <a href="/"><button>En savoir plus sur le compteur</button></a></td>
                    <td>ou encore <a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer"><button>Signer le réféérendum maintenant!!</button></a></td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <strong>Merci de votre visite :p</strong>
            <br/>
            <br/>
            <br/>
            <br/>
        </main>
        <footer><blink>&copy; David Libeau</blink></footer>
        
    </body>

</html>