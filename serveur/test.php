<?php

/*function Corr($x, $y){ //from https://www.php.net/manual/en/function.stats-stat-correlation.php#116529
    $length= count($x);
    $mean1=array_sum($x) / $length;
    $mean2=array_sum($y) / $length;

    $a=0;
    $b=0;
    $axb=0;
    $a2=0;
    $b2=0;

    for($i=0;$i<$length;$i++){
        $a=$x[$i]-$mean1;
        $b=$y[$i]-$mean2;
        $axb=$axb+($a*$b);
        $a2=$a2+ pow($a,2);
        $b2=$b2+ pow($b,2);
    }

    $corr= $axb / sqrt($a2*$b2);

    return $corr;
}

function getChiSquare($x, $n) { //from https://statistical-research.com/index.php/2012/09/18/chi-square-distribution-php/
    if ( ($n==1) && ($x > 1000) ) {
        return 0;
    }

    if ( ($x>1000) || ($n>1000) ) {
        $q = getChiSquare(($x-$n)*($x-$n)/(2*$n),1) / 2;
        if($x > $n) {
            return $q;
        } else {
            return 1 - $q;
        }
    }
    $p = exp(-0.5 * $x);
    if(($n % 2) == 1) {
        $p = $p * sqrt(2*$x/pi());
    }
    $k = $n;
    while($k >= 2) {
        $p = $p * ($x/$k);
        $k = $k - 2;
    }
    $t = $p;
    $a = $n;
    while($t > 0.0000000001 * $p) {
        $a = $a + 2;
        $t = $t * ($x / $a);
        $p = $p + $t;
    }

    $retval = 1-$p;
    return $retval;
}



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$data_nom=[];
$bdd_nom_apres_norma=[];
$bdd_nom_avant_norma=[];

ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("noms2008nat_txt.txt",'r');
while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
    $data_nom[]=$data;
}
fclose($handle);

$total_insee=0;
$i=0;
foreach($data_nom as $ligne){
    if($i>0){
        $ligne=str_replace("	",",",$ligne);
        $ligne_array=explode(",",$ligne[0]);
        $total_ligne=0;
        foreach($ligne_array as $compte){
            if(is_numeric($compte)){
                $total_ligne+=$compte;
                $total_insee+=$compte;
            }
        }
        if($ligne_array[0]=="AUTRES NOMS"){
            $section_apres_norma="AUTRES";
            $section_avant_norma="AUTRES";
        }else if(strlen($ligne_array[0])==1 || $ligne_array[0][1]==" " || $ligne_array[0][1]=="'" || $ligne_array[0][1]=="-"){
            $section_apres_norma=$ligne_array[0][0]."_";
            if(isset($ligne_array[0][2])){
                $section_avant_norma=$ligne_array[0][0].$ligne_array[0][2];
            }else{
                $section_avant_norma=$ligne_array[0][0].$ligne_array[0][0];
            }
        }else{
            $section_apres_norma=substr($ligne_array[0], 0, 2);
            $section_avant_norma=substr($ligne_array[0], 0, 2);
        }
        
        if(isset($bdd_nom_apres_norma[$section_apres_norma])){
            $bdd_nom_apres_norma[$section_apres_norma]+=$total_ligne;
        }else{
            $bdd_nom_apres_norma[$section_apres_norma]=$total_ligne;
        }
        
        if(isset($bdd_nom_avant_norma[$section_avant_norma])){
            $bdd_nom_avant_norma[$section_avant_norma]+=$total_ligne;
        }else{
            $bdd_nom_avant_norma[$section_avant_norma]=$total_ligne;
        }
    }
    $i++;
}
//$bdd_nom["TOTAL"]=$total_insee;


$bdd_comptage_17=[];
$total_comptage_17=0;
$handle = fopen("comptage_signatures_RIP.csv",'r');
while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
    $bdd_comptage_17[$data[0]]=intval($data[1]);
    $total_comptage_17+=intval($data[1]);
}
fclose($handle);

$data_comptage_11=json_decode(file_get_contents("comptage_11.json"),true)["bdd_best"];
$bdd_comptage_11=[];
$total_comptage_11=0;
foreach($data_comptage_11 as $digramme => $data){
    if($data==null){
        $bdd_comptage_11[$digramme]=0;
    }else{
        $bdd_comptage_11[$digramme]=$data["compteur"];
        $total_comptage_11+=$data["compteur"];
    }
}

$data_comptage_25=json_decode(file_get_contents("comptage_25.json"),true)["bdd_best"];
$bdd_comptage_25=[];
$total_comptage_25=0;
foreach($data_comptage_25 as $digramme => $data){
    if($data==null){
        $bdd_comptage_25[$digramme]=0;
    }else{
        $bdd_comptage_25[$digramme]=$data["compteur"];
        $total_comptage_25+=$data["compteur"];
    }
}

$bdd=[];
$pourcentages_insee=[];
$pourcentages_comptage_11=[];
$pourcentages_comptage_17=[];

$chi_square_25=0;
$chi_square_11=0;
$chi_square_17=0;


$alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
for($i=0; $i<26; $i++){
    $digramme=$alphabet[$i]."_";
    if(isset($bdd_nom_apres_norma[$digramme])){
        $compte_apres_norma=$bdd_nom_apres_norma[$digramme];
        $chi_square_17_local=pow(($bdd_comptage_17[$digramme]/$total_comptage_17)-($compte_apres_norma/$total_insee),2)/($compte_apres_norma/$total_insee);
    }else{
        $compte_apres_norma=0;
        $chi_square_17_local=0;
    }
    $pourcentages_insee_apres_norma[]=$compte_apres_norma/$total_insee*100;
    $pourcentages_comptage_17[]=$bdd_comptage_17[$digramme]/$total_comptage_17*100;
    $chi_square_17+=$chi_square_17_local;
    $bdd[$digramme]=[
        "comptage_insee_apres_norma"=>$compte_apres_norma,
        "pourcentage_insee_apres_norma"=>$compte_apres_norma/$total_insee*100,
        "comptage_rip-17-08"=>$bdd_comptage_17[$digramme],
        "pourcentage_rip-17-08"=>$bdd_comptage_17[$digramme]/$total_comptage_17*100,
        "chi_square_17-08"=>$chi_square_17_local
    ];
    for($j=0; $j<26; $j++){
        $digramme=$alphabet[$i].$alphabet[$j];
        if(isset($bdd_nom_apres_norma[$digramme])){
            $compte_apres_norma=$bdd_nom_apres_norma[$digramme];
            $chi_square_17_local=pow(($bdd_comptage_17[$digramme]/$total_comptage_17)-($compte_apres_norma/$total_insee),2)/($compte_apres_norma/$total_insee);
        }else{
            $compte_apres_norma=0;
            $chi_square_17_local=0;
        }
        $pourcentages_insee_apres_norma[]=$compte_apres_norma/$total_insee*100;
        $pourcentages_comptage_17[]=$bdd_comptage_17[$digramme]/$total_comptage_17*100;
        $chi_square_17+=$chi_square_17_local;
        if(isset($bdd_nom_avant_norma[$digramme])){
            $compte_avant_norma=$bdd_nom_avant_norma[$digramme];
            $chi_square_11_local=pow(($bdd_comptage_11[$digramme]/$total_comptage_11)-($compte_avant_norma/$total_insee),2)/($compte_avant_norma/$total_insee);
            $chi_square_25_local=pow(($bdd_comptage_25[$digramme]/$total_comptage_25)-($compte_avant_norma/$total_insee),2)/($compte_avant_norma/$total_insee);
        }else{
            $compte_avant_norma=0;
            $chi_square_11_local=0;
            $chi_square_25_local=0;
        }
        $pourcentages_insee_avant_norma[]=$compte_avant_norma/$total_insee*100;
        $pourcentages_comptage_11[]=$bdd_comptage_11[$digramme]/$total_comptage_11*100;
        $pourcentages_comptage_25[]=$bdd_comptage_25[$digramme]/$total_comptage_25*100;
        $chi_square_11+=$chi_square_11_local;
        $chi_square_25+=$chi_square_25_local;
        $bdd[$digramme]=[
            "comptage_insee_apres_norma"=>$compte_apres_norma,
            "pourcentage_insee_apres_norma"=>$compte_apres_norma/$total_insee*100,
            "comptage_rip-17-08"=>$bdd_comptage_17[$digramme],
            "pourcentage_rip-17-08"=>$bdd_comptage_17[$digramme]/$total_comptage_17*100,
            "chi_square_17-08"=>$chi_square_17_local,
            "comptage_insee_avant_norma"=>$compte_avant_norma,
            "pourcentage_insee_avant_norma"=>$compte_avant_norma/$total_insee*100,
            "comptage_rip-25-06"=>$bdd_comptage_25[$digramme],
            "pourcentage_rip-25-06"=>$bdd_comptage_25[$digramme]/$total_comptage_25*100,
            "chi_square_25-06"=>$chi_square_25_local,
            "comptage_rip-11-08"=>$bdd_comptage_11[$digramme],
            "pourcentage_rip-11-08"=>$bdd_comptage_11[$digramme]/$total_comptage_11*100,
            "chi_square_11-08"=>$chi_square_11_local
        ];
    }
}

/*foreach($bdd_nom_apres_norma as $digramme => $compte){
    if($digramme!="AUTRES"){
        $pourcentages_insee_apres_norma[]=$compte/$total_insee*100;
        $pourcentages_comptage_17[]=$bdd_comptage_17[$digramme]/$total_comptage_17*100;
        $chi_square_17_local=pow(($bdd_comptage_17[$digramme]/$total_comptage_17)-($compte/$total_insee),2)/($compte/$total_insee);
        $chi_square_17+=$chi_square_17_local;
        $bdd[$digramme][]=[
            "comptage_insee_apres_norma"=>$compte,
            "pourcentage_insee_apres_norma"=>$compte/$total_insee*100,
            "comptage_rip-17-08"=>$bdd_comptage_17[$digramme],
            "pourcentage_rip-17-08"=>$bdd_comptage_17[$digramme]/$total_comptage_17*100,
            "chi_square_17-08"=>$chi_square_17_local
        ];
    }
}
foreach($bdd_nom_avant_norma as $digramme => $compte){
    if($digramme!="AUTRES"){
        $pourcentages_insee_avant_norma[]=$compte/$total_insee*100;
        $pourcentages_comptage_11[]=$bdd_comptage_11[$digramme]/$total_comptage_11*100;
        $pourcentages_comptage_17[]=$bdd_comptage_17[$digramme]/$total_comptage_17*100;
        $pourcentages_comptage_25[]=$bdd_comptage_25[$digramme]/$total_comptage_25*100;
        $chi_square_11_local=pow(($bdd_comptage_11[$digramme]/$total_comptage_11)-($compte/$total_insee),2)/($compte/$total_insee);
        $chi_square_11+=$chi_square_11_local;
        $chi_square_25_local=pow(($bdd_comptage_25[$digramme]/$total_comptage_25)-($compte/$total_insee),2)/($compte/$total_insee);
        $chi_square_25+=$chi_square_25_local;
        $bdd[$digramme][]=[
            "comptage_insee_avant_norma"=>$compte,
            "pourcentage_insee_avant_norma"=>$compte/$total_insee*100,
            "comptage_rip-25-06"=>$bdd_comptage_25[$digramme],
            "pourcentage_rip-25-06"=>$bdd_comptage_25[$digramme]/$total_comptage_25*100,
            "chi_square_25-06"=>$chi_square_25_local,
            "comptage_rip-11-08"=>$bdd_comptage_11[$digramme],
            "pourcentage_rip-11-08"=>$bdd_comptage_11[$digramme]/$total_comptage_11*100,
            "chi_square_11-08"=>$chi_square_11_local
        ];
    }
}
var_dump(Corr($pourcentages_insee,$pourcentages_comptage_11));
var_dump(Corr($pourcentages_insee,$pourcentages_comptage_17));
var_dump(Corr($pourcentages_insee,$pourcentages_comptage_25));
var_dump(getChiSquare($pourcentages_insee,$pourcentages_comptage_11));
var_dump(getChiSquare($pourcentages_insee,$pourcentages_comptage_17));
var_dump(getChiSquare($pourcentages_insee,$pourcentages_comptage_25));

header('Content-Type: application/json');
echo(json_encode([
    "total-25-06"=>$total_comptage_25,
    "total-11-08"=>$total_comptage_11,
    "total-17-08"=>$total_comptage_17,
    "correlation-25-06"=>Corr($pourcentages_insee_avant_norma,$pourcentages_comptage_25),
    "correlation-11-08"=>Corr($pourcentages_insee_avant_norma,$pourcentages_comptage_11),
    "correlation-17-08"=>Corr($pourcentages_insee_apres_norma,$pourcentages_comptage_17),
    "chi-quare-25-06"=>$chi_square_25,
    "chi-quare-11-08"=>$chi_square_11,
    "chi-quare-17-08"=>$chi_square_17,
    "details"=>$bdd
]));


/*
$bdd_history=[];

ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("history.txt",'r');
while ( ($data = fgetcsv($handle, 1000,";")) !== FALSE ) {
    $date=new DateTime("@$data[0]");
    //var_dump($date->format("d\/m\/y H:i"));
    $bdd_history[]=[$date->format("d\/m\/y H:i"),$data[1]];
    echo($date->format("d\/m\/y H:i").",".$data[1]."\r");
}
fclose($handle);


//var_dump($bdd_history);

*/

include("preprocess.php");

header('Content-Type: application/json');
echo(json_encode($bdd_maj_priorite));