<?php include($_SERVER['DOCUMENT_ROOT']."/preprocess.php");

$title="Page d'erreur";
include($_SERVER['DOCUMENT_ROOT']."/head.inc");

if(!empty($_GET["code"])){
    $code=$_GET["code"];
}else{
    $code="0";
}

$codeMessage=[
    "400"=>"T'es mauvaise comme requête",
    "401"=>"Où est votre badge ?",
    "402"=>"Il faut payer ?",
    "403"=>"Interdit d'entrer",
    "404"=>"On s'est perdu ?",
    "405"=>"Change de méthode !",
    "406"=>"Ceci n'est pas acceptable.",
    "407"=>"Il faut le badge du proxy...",
    "408"=>"Trop long, trop vite, pas bien.",
    "409"=>"Trump vient de tweeter.",
    "410"=>"C'est la démission.",
    "411"=>"Il nous faut de la longueur.",
    "412"=>"C'est un \"Oui, mais...\" sur Parcoursup.",
    "413"=>"Trop lourd pour moi.",
    "414"=>"L'adresse comporte trop de trucs.",
    "415"=>"C'est quoi ce merdia ?",
    "416"=>"On n'est pas à la page...",
    "417"=>"Quand tu ouvres un colis commandé sur internet.",
    "421"=>"On vous a mal renseigné.",
    "422"=>"C'est quoi ce truc ?",
    "423"=>"Fermé, revenez plus tard.",
    "424"=>"Indépendant de notre volonté.",
    "426"=>"Mets toi à jour.",
    "428"=>"C'est un \"Non, mais...\" sur Parcoursup.",
    "429"=>"C'est pas ici. Ni là. Et pas là non plus.",
    "431"=>"Chapeau trop gros.",
    "444"=>"Guichet fermé juste devant nous alors qu'on a poiroté toute la journée.",
    "451"=>"Bloqué par le gouvernement, srûrement...",
    "500"=>"On a un petit problème...",
    "501"=>"Pas encore codé.",
    "502"=>"On a demandé, mais on n'a pas eu de réponse correcte.",
    "503"=>"Fermé, revenez demain.",
    "504"=>"On a demandé, mais on n'a pas eu de réponse dans les temps.",
    "505"=>"No comprendo la demande.",
    "506"=>"L'indice du CAC40 variant est négocié.",
    "507"=>"On n'a plus de place !!",
    "508"=>"Bonjour. Bonjour. Bonjour. Bonjour.",
    "510"=>"Achète le DLC.",
    "511"=>"Il faut le badge de la ligne.",
];

?>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/ampoule.png" alt="emoji ampoule" /> Erreur <?php echo($code); ?></h2>
        <p><?php if(isset($codeMessage[$code])){ echo($codeMessage[$code]); }else{ echo("Le site ne fonctionne pas."); } ?></p>
    </section>
    
    <section class="centered">
        <p>
            <img src="/img/aurevoir.gif" alt="GIF aurevoir" style="max-width:100%" />
        </p>
    </section>
    
    <section class="centered">
        <p>Pas de panique. Vous aussi, vous pouvez faire une sortie théâtrale en cliquant sur le bouton ci-dessous.</p>
        <p><a href="/" class="btn"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil</a></p>
    </section>

</main>
<?php include($_SERVER['DOCUMENT_ROOT']."/footer.inc"); ?>
    <script>
        $(function() {

        });
    </script>
</body>
</html>