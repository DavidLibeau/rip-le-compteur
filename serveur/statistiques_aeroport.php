<?php include("preprocess.php");

$title="Signatures du RIP par aéroport";
$custom_header='<link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>';
$description="Les signatures par aéroport de France.";
include("head.inc");

?>

<style>
    #communes_paginate, .dts_label{
        display: none!important;
    }
    div.dts div.dataTables_scrollBody {
        background: repeating-linear-gradient(45deg, #f0f0f0, #f0f0f0 10px, #fff 10px, #fff 20px)!important;
    }
    #communes_wrapper .fg-toolbar {
        padding: 10px;
        background-color: white!important;
        border: 0;
        border-radius: 5px;
    }
    #communes_filter{
        float: none;
        text-align: left;
    }
    .dataTables_scrollHead {
        margin-top: 5px;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
        background:none;
    }
    .dts_label{
        display: none;
    }
    #communes_wrapper tbody td {
        padding: 3px;
        text-align: right;
    }
    #communes_wrapper tbody td:first-child{
        text-align: center;
    }
    #communes_wrapper tbody td:nth-child(2){
        text-align: left;
    }
    #communes_wrapper tbody>tr:not(.info_commune)>td>div{
        position: relative;
        top: 4px;
        display: inline-block;
        max-width: 30vw;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    #communes_wrapper tbody td>.badge{
        position: relative;
        top: -1px;
    }
    #communes_wrapper tbody tr{
        background-color: #f0f0f0;
    }
    #communes_wrapper tbody tr:hover{
        background: white;
        cursor: pointer;
    }
    #communes_wrapper tbody tr.selected{
        background: white;
        font-weight: bold;
    }
    #communes_wrapper tbody tr.selected+tr td{
        background: white;
        padding: 10px;
    }
    .info_commune{
        display: none;
        text-align: left;
    }
    .info_commune>td {
        white-space: normal!important;
    }
    tbody tr.selected+.info_commune{
        display: table-row;
    }
    .info_commune *:not(.chip) {
        width: initial;
        margin: initial;
    }
    
    #communes_wrapper thead tr th {
        background-color: #3c3c3c;
        border: 2px solid #f0f0f0;
        border-radius: 6px;
    }
    #communes_wrapper .DataTables_sort_wrapper{
        color: #f0f0f0!important;
    }
    .DataTables_sort_icon{
        background-color: #f0f0f0;
        border-radius: 50%;
        margin-right: 5px;
    }
    
    
    #load_communes{
        position: relative;
        margin-bottom: -200px;
        padding: 80px 0;
        text-align: center;
        z-index: 9;
        background: -moz-radial-gradient(center, ellipse cover,  rgba(240,240,240,1) 0%, rgba(240,240,240,1) 27%, rgba(240,240,240,0.63) 74%, rgba(240,240,240,0.02) 99%, rgba(240,240,240,0) 100%);
        background: -webkit-radial-gradient(center, ellipse cover,  rgba(240,240,240,1) 0%,rgba(240,240,240,1) 27%,rgba(240,240,240,0.63) 74%,rgba(240,240,240,0.02) 99%,rgba(240,240,240,0) 100%);
        background: radial-gradient(ellipse at center,  rgba(240,240,240,1) 0%,rgba(240,240,240,1) 27%,rgba(240,240,240,0.63) 74%,rgba(240,240,240,0.02) 99%,rgba(240,240,240,0) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f0f0f0', endColorstr='#00f0f0f0',GradientType=1 );
    }
    #fake_communes{
        width: 100%;
    }
    #fake_communes td{
        text-align: right;
    }
    #fake_communes td:first-child{
        text-align: left;
    }
    
    #communes_info{
        padding-top: 0;
    }
    .info_commune,.info_commune *{
        text-align: left!important;
    }
    
</style>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/avion_atterrissage.png" alt="emoji avion atterrissage" /> Statistiques par aéroport</h2>
    </section>
    
    <section>
        <p>En croisant les <a href="liste_communes">données par commune annalysées</a> et le <a href="https://www.data.gouv.fr/fr/datasets/aeroports-francais-coordonnees-geographiques/" target="_blank" rel="noopener noreferrer">fichier des aéroports de France</a>, on peut potentiellement voir une corrélation entre habiter près d'un aéroport et signer le référendum contre la privatisation du groupe ADP.</p>
        <p>Les données par communes proviennent du compteur <a href="https://compteur.rip/" target="_blank" rel="noopener noreferrer">Compteur.RIP</a>.</p>
    </section>
    
    <section>
        <p>Cette page est en cours de construction ! Plus de données et de calculs statistiques bientôt.</p>
    </section>
    
    <section>
        <h3>Tableau des villes ayant un aéroport</h3>
        <div class="content">
            <div id="load_communes">
                <button class="btn blanc">Charger le tableau complet</button>
            </div>
            <table id="fake_communes">
                <thead>
                    <tr>
                        <th>Aéroport</th>
                        <th>Commune</th>
                        <th>Signatures</th>
                        <th>Pourcentage</th>
                        <th>Inscrits</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span class="badge">AAA</span></td>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><span class="badge">AAA</span></td>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><span class="badge">AAA</span></td>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><span class="badge">AAA</span></td>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><span class="badge">AAA</span></td>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                </tbody>
            </table>
            <table id="communes">
            </table>
        </div>
        <p id="communes_customFilters" style="display:none"><span style="display:inline-block; background:white; border-radius: 5px; padding: 10px;">Filtrer les données par la taille des communes :<br/><input type="number" id="min" placeholder="Minimum"/> &lt; nombre d'inscrits  &lt; <input type="number" id="max" placeholder="Maximum"/></span></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script src="lib/DataTables/datatables.js"></script>
<script src="lib/PapaParse-5.0.0/papaparse.min.js"></script>
<script>
    $(function() {  
        $("#load_communes").click(function(){
            $(this).off("click").css("cursor","wait");
            $("#load_communes button").html('<i class="fa fa-cog fa-spin fa-fw"></i> Chargement du tableau en cours...').css("cursor","wait");;
            Papa.parse("geo/bdd_list_aeroports.csv", {
                download: true,
                complete: function(results) {
                    console.log("Finished:", results.data);

                    var table = $("#communes").on( 'init.dt', function () {
                            $("#load_communes, #fake_communes").hide();
                            $("#communes_customFilters").show();
                        }).DataTable( {
                        data: results.data,
                        columns: [
                            {
                                title: "Aéroport",
                                render: function ( data, type, row ) {
                                    return '<span class="badge">'+data+'</span>';
                                }
                            },
                            {
                                title: "Commune",
                                render: function ( data, type, row ) {
                                    return '<i class="fa fa-caret-right" aria-hidden="true"></i> <div>'+data+'</div>'+[row[5] ? ' <span class="badge">'+row[5]+'</span>' : ""];
                                },
                                "width": "20%",
                            },
                            {
                                title: "Signatures",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '&nbsp;', '&nbsp;'),

                            },
                            {
                                title: "Pourcentage",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '&nbsp;', '&nbsp;%'),
                            },
                            {
                                title: "Inscrits",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '&nbsp;', '&nbsp;'),
                            },
                        ],
                        select: 'single',
                        pageLength:50,
                        deferRender: true,
                        scrollY: "500px",
                        scrollCollapse: false,
                        scroller: {
                            rowHeight: 28,
                            boundaryScale: 0.75,
                        },
                        pagingType:"simple",
                        language:{
                            //"decimal":",",
                            //"thousands": " ",
                            "emptyTable": "Aucune donnée.",
                            "info": "De _START_ à _END_ sur un total de _TOTAL_ communes",
                            "infoEmpty": "Aucune donnée.",
                            "infoFiltered": "(filtré de _MAX_ communes totales)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "",
                            "loadingRecords": "Chargement en cours...",
                            "processing": "Génération en cours...",
                            "search": "Recherche rapide : ",
                            "zeroRecords": "Aucun résultat.",
                            "paginate": {
                                "first": "Premier",
                                "last": "Dernier",
                                "next": "Suivant",
                                "previous": "Précédent"
                            },
                            "aria": {
                                "sortAscending":  ": activate to sort column ascending",
                                "sortDescending": ": activate to sort column descending"
                            }
                        }
                    }).on('select', onSelect).on('deselect', onDeselect).on( 'draw.dt', function () {
                        if(table){
                            table.on('select', onSelect).on('deselect', onDeselect);
                        }
                    }).draw();

                    $('#min').keyup( function() { table.draw(); } );
                    $('#max').keyup( function() { table.draw(); } );

                    function onSelect ( e, dt, type, indexes ) {
                        var nom_commune=table.rows(indexes).data().toArray()[0][1].split(" / ")[0];
                        $.ajax({
                            url: "geo/search.php?q="+nom_commune,
                        }).done(function(data) {
                            $(".info_commune td").html(data);
                        });
                        table.rows(indexes).every( function () {
                            this.child($('<tr class="info_commune">'+
                                '<td colspan="5"><i class="fa fa-cog fa-spin fa-fw"></i> Chargement des données en cours...</td>'+
                            '</tr>')).show();
                            this.nodes().to$().find(".fa-caret-right").removeClass("fa-caret-right").addClass("fa-caret-down");
                        });
                    }
                    function onDeselect ( e, dt, type, indexes ) {
                        table.rows(indexes).every( function () {
                            if(this.child()){
                                this.child().remove();
                                this.nodes().to$().find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
                            }
                        });
                    }
                }
            });

            jQuery.fn.dataTableExt.afnFiltering.push( //from https://github.com/DataTables/Plugins/blob/master/filtering/row-based/range_numbers.js
                function( oSettings, aData, iDataIndex ) {
                    var iColumn = 4;
                    var iMin = document.getElementById('min').value * 1;
                    var iMax = document.getElementById('max').value * 1;

                    var iVersion = aData[iColumn] == "-" ? 0 : aData[iColumn]*1;
                    if ( iMin == "" && iMax == "" )
                    {
                        return true;
                    }
                    else if ( iMin == "" && iVersion <= iMax )
                    {
                        return true;
                    }
                    else if ( iMin <= iVersion && "" == iMax )
                    {
                        return true;
                    }
                    else if ( iMin <= iVersion && iVersion <= iMax )
                    {
                        return true;
                    }
                    return false;
                }
            );         
        });

    });
</script>
</body>
</html>