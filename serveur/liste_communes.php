<?php include("preprocess.php");

$title="Signatures du RIP par commune";
$custom_header='<link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>';
$description="Les signatures par communes.";
include("head.inc");

?>

<style>
    #communes_paginate, .dts_label{
        display: none!important;
    }
    div.dts div.dataTables_scrollBody {
        background: repeating-linear-gradient(45deg, #f0f0f0, #f0f0f0 10px, #fff 10px, #fff 20px)!important;
    }
    #communes_wrapper .fg-toolbar {
        padding: 10px;
        background-color: white!important;
        border: 0;
        border-radius: 5px;
    }
    #communes_filter{
        float: none;
        text-align: left;
    }
    .dataTables_scrollHead {
        margin-top: 5px;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
        background:none;
    }
    .dts_label{
        display: none;
    }
    #communes_wrapper tbody td {
        padding: 3px;
        text-align: right;
    }
    #communes_wrapper tbody td:first-child{
        text-align: left;
    }
    #communes_wrapper tbody>tr:not(.info_commune)>td>div{
        position: relative;
        top: 4px;
        display: inline-block;
        max-width: 30vw;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    #communes_wrapper tbody td>.badge{
        position: relative;
        top: -1px;
    }
    #communes_wrapper tbody tr{
        background-color: #f0f0f0;
    }
    #communes_wrapper tbody tr:hover{
        background: white;
        cursor: pointer;
    }
    #communes_wrapper tbody tr.selected{
        background: white;
        font-weight: bold;
    }
    #communes_wrapper tbody tr.selected+tr td{
        background: white;
        padding: 10px;
    }
    .info_commune{
        display: none;
    }
    .info_commune>td {
        white-space: normal!important;
    }
    tbody tr.selected+.info_commune{
        display: table-row;
    }
    .info_commune *:not(.chip) {
        width: initial;
        margin: initial;
    }
    
    #communes_wrapper thead tr th {
        background-color: #3c3c3c;
        border: 2px solid #f0f0f0;
        border-radius: 6px;
    }
    #communes_wrapper .DataTables_sort_wrapper{
        color: #f0f0f0!important;
    }
    .DataTables_sort_icon{
        background-color: #f0f0f0;
        border-radius: 50%;
        margin-right: 5px;
    }
    
    
    #load_communes{
        position: relative;
        margin-bottom: -200px;
        padding: 80px 0;
        text-align: center;
        z-index: 9;
        background: -moz-radial-gradient(center, ellipse cover,  rgba(240,240,240,1) 0%, rgba(240,240,240,1) 27%, rgba(240,240,240,0.63) 74%, rgba(240,240,240,0.02) 99%, rgba(240,240,240,0) 100%);
        background: -webkit-radial-gradient(center, ellipse cover,  rgba(240,240,240,1) 0%,rgba(240,240,240,1) 27%,rgba(240,240,240,0.63) 74%,rgba(240,240,240,0.02) 99%,rgba(240,240,240,0) 100%);
        background: radial-gradient(ellipse at center,  rgba(240,240,240,1) 0%,rgba(240,240,240,1) 27%,rgba(240,240,240,0.63) 74%,rgba(240,240,240,0.02) 99%,rgba(240,240,240,0) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f0f0f0', endColorstr='#00f0f0f0',GradientType=1 );
    }
    #fake_communes{
        width: 100%;
    }
    #fake_communes td{
        text-align: right;
    }
    #fake_communes td:first-child{
        text-align: left;
    }
    
    #communes_info{
        padding-top: 0;
    }
    
    #resultats *{
        width: initial;
    }
    #recherche_communes [type="text"], #recherche_communes [type="submit"]{
        padding: 10px 20px;
        border: 0;
        border: 2px solid #f0f0f0;
        font-size: 16px;
        background: none;
    }
    #recherche_communes [type="text"]{
        width: 500px;
        max-width: 50vw;
        border-top-left-radius: 20px;
        border-bottom-left-radius: 20px;
    }
    #recherche_communes [type="submit"]{
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }
    #recherche_communes [type="submit"]:hover{
        background: #f0f0f0;
    }
    #recherche_communes [type="submit"]:active{
        background: white;
    }
    
    
    .ui-autocomplete {
        position: relative;
        margin-left: 20px;
        margin-top: -2px;
        top: 0!important;
        left: 0!important;
        width: calc(500px + 20px)!important;
        max-width: calc(50vw + 20px);
        max-height: 200px;
        overflow-y: auto;
        overflow-x: hidden;
        border: 2px solid #f0f0f0!important;
    }
</style>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/village.png" alt="emoji village" /> Liste des communes</h2>
    </section>
    
    <section>
        <p>En croisant le nombre d'inscrits sur les listes électorales (via les <a href="https://www.data.gouv.fr/fr/datasets/resultats-des-elections-europeennes-2019/" target="_blank" rel="noopener noreferrer">résultats des élections européennes</a> ou de la <a href="https://statistiques-locales.insee.fr/" target="_blank" rel="noopener noreferrer">base de données de l'INSEE au 1er janvier 2019</a>) et le nombre de signatures du référendum d'initiative partagée par commune, nous pouvons obtenir un taux de participation pour chaque commune.</p>
        <p>Les signatures comptées sur le site web du ministère de l'Intérieur fournissent des données de mauvaise qualité. Les communes ayant un nom similaire ou les nouvelles communes ne sont pas correctement listées. Dans le tableau ci-dessous, ces communes ne sont pas divisées (aucune approximation n'est faite). Il est affiché les données telles que le site web du ministère de l'Intérieur les affiche. En revanche, les nouvelles communes ou les homonymes sont spécifiés. Les <a href="https://www.data.gouv.fr/fr/datasets/communes-nouvelles-1/" target="_blank" rel="noopener noreferrer">bases de données des nouvelles communes</a> et le <a href="https://nominatim.openstreetmap.org/" target="_blank" rel="noopener noreferrer">service Nominatim d'OpenStreetMap</a> ont été utilisés. Dans le tableau ci-dessous, seul les communes ayant au moins une signature sont affichées. Il reste quelques communes problématiques (en cours de résolution). La commune vide peut potentiellement représenter les français·es établi·e·s hors de France.</p>
        <p>Les données du tableau complet proviennent du compteur <a href="https://compteur.rip/" target="_blank" rel="noopener noreferrer">Compteur.RIP</a>. Les résulats via la barre de recherche affichent les données de plusieurs compteurs.</p>
    </section>
    
    <section>
        <h3>Rechercher dans la base de données</h3>
        <?php
            if(!empty($_GET["q"])){
                $searchResults=curl_download("https://rip-le-compteur.dav.li/geo/search.php?q=".$_GET["q"]);
                $query=htmlspecialchars($_GET["q"], ENT_QUOTES, 'UTF-8');
            }
        ?>
        <form id="recherche_communes" class="content" method="get">
            <label>
                <input name="q" type="text" placeholder="Montreuil, Angers, Saint-Frézal-de-Ventalon..." <?php
                    if(!empty($_GET["q"])){
                        echo('value="'.$query.'"');
                    }
                ?> />
            </label>
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            <div id="autocomplete"></div>
        </form>
        <div id="resultats" class="content">
            <?php
            if(!empty($_GET["q"])){
                echo($searchResults);
            }
            ?>
        </div>
    </section>
    
    <section>
        <h3>Tableau des signatures par commune</h3>
        <div class="content">
            <div id="load_communes">
                <button class="btn blanc">Charger le tableau complet</button>
            </div>
            <table id="fake_communes">
                <thead>
                    <tr>
                        <th>Commune</th>
                        <th>Signatures</th>
                        <th>Pourcentage</th>
                        <th>Inscrits</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-right" aria-hidden="true"></i> ...</td>
                        <td>...</td>
                        <td>... %</td>
                        <td>...</td>
                    </tr>
                </tbody>
            </table>
            <table id="communes">
            </table>
        </div>
        <p id="communes_customFilters" style="display:none"><span style="display:inline-block; background:white; border-radius: 5px; padding: 10px;">Filtrer les données par la taille des communes :<br/><input type="number" id="min" placeholder="Minimum"/> &lt; nombre d'inscrits  &lt; <input type="number" id="max" placeholder="Maximum"/></span></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "WebSite",
  "url": "https://rip-le-compteur.dav.li/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://rip-le-compteur.dav.li/liste_communes?q={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<script src="lib/DataTables/datatables.js"></script>
<script src="lib/PapaParse-5.0.0/papaparse.min.js"></script>
<script>
    $(function() {  
        $("#load_communes").click(function(){
            $(this).off("click").css("cursor","wait");
            $("#load_communes button").html('<i class="fa fa-cog fa-spin fa-fw"></i> Chargement du tableau en cours...').css("cursor","wait");;
            Papa.parse("geo/bdd_list.csv", {
                download: true,
                complete: function(results) {
                    console.log("Finished:", results.data);
                    
                    var autocomplete = [];
                    results.data.forEach(function(commune){
                        commune[0].split(" / ").forEach(function(nom){
                            autocomplete.push(nom);
                        });
                    })
                    $('[name="q"]').autocomplete({
                        source: autocomplete,
                        minLength: 3,
                        autoFocus: true,
                        appendTo: "#autocomplete"
                    });

                    var table = $("#communes").on( 'init.dt', function () {
                            $("#load_communes, #fake_communes").hide();
                            $("#communes_customFilters").show();
                        }).DataTable( {
                        data: results.data,
                        columns: [
                            {
                                title: "Commune",
                                render: function ( data, type, row ) {
                                    return '<i class="fa fa-caret-right" aria-hidden="true"></i> <div>'+data+'</div>'+[row[4] ? ' <span class="badge">'+row[4]+'</span>' : ""];
                                },
                                "width": "20%",
                            },
                            {
                                title: "Signatures",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '&nbsp;', '&nbsp;'),

                            },
                            {
                                title: "Pourcentage",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '&nbsp;', '&nbsp;%'),
                            },
                            {
                                title: "Inscrits",
                                render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '&nbsp;', '&nbsp;'),
                            },
                        ],
                        select: 'single',
                        pageLength:50,
                        deferRender: true,
                        scrollY: "500px",
                        scrollCollapse: false,
                        scroller: {
                            rowHeight: 28,
                            boundaryScale: 0.75,
                        },
                        pagingType:"simple",
                        language:{
                            //"decimal":",",
                            //"thousands": " ",
                            "emptyTable": "Aucune donnée.",
                            "info": "De _START_ à _END_ sur un total de _TOTAL_ communes",
                            "infoEmpty": "Aucune donnée.",
                            "infoFiltered": "(filtré de _MAX_ communes totales)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "",
                            "loadingRecords": "Chargement en cours...",
                            "processing": "Génération en cours...",
                            "search": "Recherche rapide : ",
                            "zeroRecords": "Aucun résultat.",
                            "paginate": {
                                "first": "Premier",
                                "last": "Dernier",
                                "next": "Suivant",
                                "previous": "Précédent"
                            },
                            "aria": {
                                "sortAscending":  ": activate to sort column ascending",
                                "sortDescending": ": activate to sort column descending"
                            }
                        }
                    }).on('select', onSelect).on('deselect', onDeselect).on( 'draw.dt', function () {
                        if(table){
                            table.on('select', onSelect).on('deselect', onDeselect);
                        }
                    }).draw();

                    $('#min').keyup( function() { table.draw(); } );
                    $('#max').keyup( function() { table.draw(); } );

                    function onSelect ( e, dt, type, indexes ) {
                        var nom_commune=table.rows(indexes).data().toArray()[0][0].split(" / ")[0];
                        $.ajax({
                            url: "geo/search.php?q="+nom_commune,
                        }).done(function(data) {
                            $(".info_commune td").html(data);
                        });
                        table.rows(indexes).every( function () {
                            this.child($('<tr class="info_commune">'+
                                '<td colspan="4"><i class="fa fa-cog fa-spin fa-fw"></i> Chargement des données en cours...</td>'+
                            '</tr>')).show();
                            this.nodes().to$().find(".fa-caret-right").removeClass("fa-caret-right").addClass("fa-caret-down");
                        });
                    }
                    function onDeselect ( e, dt, type, indexes ) {
                        table.rows(indexes).every( function () {
                            if(this.child()){
                                this.child().remove();
                                this.nodes().to$().find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
                            }
                        });
                    }
                }
            });

            jQuery.fn.dataTableExt.afnFiltering.push( //from https://github.com/DataTables/Plugins/blob/master/filtering/row-based/range_numbers.js
                function( oSettings, aData, iDataIndex ) {
                    var iColumn = 3;
                    var iMin = document.getElementById('min').value * 1;
                    var iMax = document.getElementById('max').value * 1;

                    var iVersion = aData[iColumn] == "-" ? 0 : aData[iColumn]*1;
                    if ( iMin == "" && iMax == "" )
                    {
                        return true;
                    }
                    else if ( iMin == "" && iVersion <= iMax )
                    {
                        return true;
                    }
                    else if ( iMin <= iVersion && "" == iMax )
                    {
                        return true;
                    }
                    else if ( iMin <= iVersion && iVersion <= iMax )
                    {
                        return true;
                    }
                    return false;
                }
            );         
        });


        $("#recherche_communes").on("submit",function(evt){
            evt.preventDefault();
            $("#resultats").html('<p><i class="fa fa-cog fa-spin fa-fw"></i> Recherche en cours...</>');
            $.ajax({
                url: "geo/search.php?q="+$(this).find("[type='text']").val(),
            }).done(function(data) {
                $("#resultats").html(data);
            });
        });


    });
</script>
</body>
</html>