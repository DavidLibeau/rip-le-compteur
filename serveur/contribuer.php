<?php include("preprocess.php");

$title="Tableau de bord pour contributer";
$description="Contribuer au comptage des signatures du référendum ADP.";

if(isset($_GET["popup"])){
    $reste_total=sizeof($bdd_maj_priorite);
    shuffle($bdd_maj_priorite);
    $section=str_replace("_","",$bdd_maj_priorite[0]);
    $jsonData=[
        "a_faire"=>sizeof($bdd_maj_priorite),
        "digramme"=>$bdd_maj_priorite[0],
        "lien"=>'https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$bdd_last[$bdd_maj_priorite[0]]["nb_pages"]
    ];
    header('Content-Type: application/json');
    echo(json_encode($jsonData));
}else{

include("head.inc");

?>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/poing.png" alt="emoji poing" /> Contribuer</h2>
        <p>Pour information, sur ce compteur, <a href="statistiques_contributions"><?php echo(number_format($totalContrib, 0, ',', '&nbsp;')); ?> contributions ont été enregistrées par <?php echo(number_format($contribUnique, 0, ',', '&nbsp;')); ?> contributeur·rice·s uniques</a>. Merci pour votre aide !</p>
        <p>Avant de contribuer, soyez sûr d'avoir installé l'extension :</p>
        <p><a href="https://addons.mozilla.org/en-US/firefox/addon/rip-le-compteur/" target="_blank" rel="noopener noreferrer" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox</a> ou <a href="https://chrome.google.com/webstore/detail/rip-le-compteur/hoopnflmlpfbbmmfiocfghejeecenbkn" target="_blank" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Chrome</a></p>
        <p>Version 2.1 (mise à jour le 19/08/19)</p>
        <p><strong>Pour rappel, il ne faut visiter que la dernière page de chaque digramme.</strong> Il faut visiter chaque digramme (AA, AB, AC...) mais il est <strong>inutile</strong> de visiter chaque page (1, 2, 3...). Cliquez sur le bouton "<strong>>></strong>" en bas de la page pour accéder à la dernière page !</p>
    </section>

    <section>
        <p>
            <?php
            if(sizeof($bdd_maj_priorite)==0){
                if($total_a_maj!=0){
                    echo('Il n\'y a plus aucun digramme à mettre à jour en priorité. Bravo ! En revanche, il reste toujours '.$total_a_maj.' digrammes peu populaires. Il n\'est pas forcement nécessaire de vérifier tous les digrammes. La page "<a href="verifier">Verifier</a>" permet de comparer les données entres les compteurs et de voir les différences. Vérifier un à un tous les digrammes est un travail peu utile car les statistiques ne changent que très peu chaque jour. En plus, cela remplit la base de données inutillement.');
                }else{
                    echo("Il n'y a plus aucun digramme à mettre à jour. Bravo !");
                }
            }else{
                echo('Il reste <span class="badge">'.sizeof($bdd_maj_priorite).'</span> digrammes à mettre à jour en priorité !<br/>');
                if(sizeof($bdd_maj_priorite)>=5){
                    echo("Voici 5 digrammes pour vous : ");
                    shuffle($bdd_maj_priorite);
                    $bdd_maj_priorite=array_slice($bdd_maj_priorite, 0, 5);
                }else{
                    echo("Hop ! Voici les derniers digrammes : ");
                }
                foreach ($bdd_maj_priorite as $s) {
                    $section=str_replace("_","",$s);
                    echo('<a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank">'.$s.'</a> ');
                }
                echo('<button id="ouvrirTout" class="btn">Tout ouvrir <i class="fa fa-arrow-right" aria-hidden="true"></i></button>');
            }
            ?>
        </p>
    </section>
    <section>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Alphabétique</a></li>
                <li><a href="#tabs-2">Popularité</a></li>
                <li><a href="#tabs-3">A mettre à jour <span class="badge"><?php echo($total_a_maj); ?></span></a></li>
                <li><a href="#tabs-4">A préciser <span class="badge"><?php echo($preciser); ?></span></a></li>
            </ul>
            <div id="tabs-1">
                <table class="stats">
                    <?php
                    foreach ($bdd_best as $s => $donnees) {
                        $section=str_replace("_","",$s);
                        echo('<tr>');
                        echo('<td><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$donnees["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></td>');
                        echo('<td class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</td>');
                        echo('</tr>'."\n");
                    }
                    ?>
                </table>
            </div>
            <div id="tabs-2">
                <table class="stats">
                    <?php
                    echo($table_popularite);
                    ?>
                </table>
            </div>
            <div id="tabs-3">
                <label for="slider">Signatures :</label>
                <input type="text" id="slider" readonly style="border:0; font-weight:bold;">
                <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                </div>
                <table class="stats" style="margin-top:20px;">
                    <?php
                    echo($table_maj);
                    ?>
                </table>
            </div>
            <div id="tabs-4">
                <table class="stats">
                <?php
                echo($table_preciser);
                ?>
                </table>
            </div>
        </div>
    </section>

</main>
<?php include("footer.inc"); ?>
    <script>
        $(function() {
            $("#tabs").tabs();
            
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 10000,
                values: [0, 10000],
                slide: function(event, ui) {
                    if(ui.values[1]==10000){
                        var max="TOUT";
                    }else{
                        var max=ui.values[1];
                    }
                    var min=ui.values[0];
                    $("#slider").val(min + " - " + max);
                    $("#slider~.stats .badge").each(function() {
                        if (parseInt($(this).text()) > min && (max=="TOUT" || parseInt($(this).text()) < max)) {
                            $(this).parents("tr").show();
                        } else {
                            $(this).parents("tr").hide();
                        }
                    })
                }
            });
            $("#slider").val("0 - TOUT");
        
            $("#ouvrirTout").click(function(){
                $(this).siblings("a").each(function(){
                    window.open($(this).attr("href"));
                });
            });

        });
    </script>
</body>
</html>
<?php } ?>