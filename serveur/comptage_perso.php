<?php include("preprocess.php");

$title="Comptage décentralisé";
$description="Votre propre comptage.";
include("head.inc");

?>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/dossiers.png" alt="emoji dossiers" /> Comptage décentralisé</h2>
        <p>Votre propre comptage</p>
    </section>
    
    <section>
        <p>En installant l'extention de navigateur <i>RIP, le compteur</i> vous pouvez vous-même, sur votre ordinateur, extraire les données de la liste publique des signataires du RIP et donc avoir vos propres données.</p>
        <p>Les données que vous pouvez télécharger ici (sur la page <i>https://rip-le-compteur.dav.li/comptage_perso</i>) sont uniquement vos propres données. Par conséquent, vous pouvez les utilisez comme vous le souhaitez. Aucune licence n'est apposée.</p>
        <p style="font-weight:bold"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Après avoir désactivé les systèmes anti-robots le 13 août, le ministère de l'Intérieur les a réactivés le 20 août, rendant le comptage décentralisé impossible à faire.</p>
    </section>
    
    <section id="contenu_custom">
        <p style="font-weight:bold"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Vous n'avez pas installé l'extension sur votre navigateur.</p>
        <p>L'extension (uniquement disponible pour ordinateur) permet d'extraire automatiquement la liste de signataires du RIP. Pour des raisons légales, les noms et prénoms ne sont pas extraits.</p>
        <div class="content centered">
            <div class="demi">
                <h3>Etape 1</h3>
                <p><a href="rip_le_compteur-2.0-fx.xpi" class="btn centered" style="text-align:center"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox<br/>(téléchargement direct)</a></p>
                <p><a href="https://addons.mozilla.org/en-US/firefox/addon/rip-le-compteur/" target="_blank" rel="noopener noreferrer me" class="btn" style="text-align:center"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox<br/>(via le magasin)</a></p>
                <p>ou</p>
                <p><a href="https://chrome.google.com/webstore/detail/rip-le-compteur/hoopnflmlpfbbmmfiocfghejeecenbkn" target="_blank" rel="noopener noreferrer me" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Chrome</a></p>
                <p>Version 2.0 (mise à jour le 15/08/19)</p>
            </div><div class="demi">
                <h3>Etape 2</h3>
                <p>Cliquez sur l'icone de l'extension en haut à droite du navigateur. Allez sur l'onglet "Extraire", puis lancez l'extraction.</p>
                <p>Vous pouvez paramétrer une extraction complète (beaucoup plus long) ou rester sur un comptage simple (les données géographiques ne seront pas téléchargées). En ajoutant plusieurs onglets, l'extraction sera plus rapide.</p>
                <p>Revenez ensuite sur cette page pour télécharger vos données.</p>
            </div>
        </div>
    </section>

</main>
<script src="//dav.li/jquery/3.4.1.min.js"></script>
<script src="//dav.li/jquery/ui/jquery-ui.min.js"></script>
<script src="/script.js"></script>
</body>
</html>