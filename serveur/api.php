<?php

/* DOCS :

# total
- date=d/m/y-H:i:s

ex: https://rip-le-compteur.dav.li/dev/api.php?endpoint=total&date=19/06/19-01:00:00

# cache

ex: https://rip-le-compteur.dav.li/dev/api.php?endpoint=cache

# stats

# aggregate

# contrib

*/

include("func.php");

$jsonData=[
    "status"=>"error",
    "message"=>"Unknown error"
];

if(isset($_GET["endpoint"])){
    switch($_GET["endpoint"]){
        case "total":
            if(!empty($_GET["date"])){
                if(DateTime::createFromFormat('d/m/y H:i:s', str_replace("-"," ",$_GET["date"]))){
                    $getData=getData(str_replace("-"," ",$_GET["date"]),false);
                    $total=$getData["total"];
                    $pourcentage=round($total/4717396*100,2);
                    $date=DateTime::createFromFormat('d/m/y H:i:s', str_replace("-"," ",$_GET["date"]));
                }else{
                    $error=true;
                    $errorMsg="Invalid date format";
                }
            }else{
                $getData=getData();
                $total=$getData["total"];
                $pourcentage=$getData["pourcentage"];
                $date=$getData["date"];
            }

            if(isset($error) && $error){
                $jsonData=[
                    "status"=>"error",
                    "message"=>$errorMsg
                ];
            }else{
                $jsonData=[
                    "total"=>$total,
                    "pourcentage"=>$pourcentage,
                    "date"=>$date
                ];
            }
            break;
        case "cache":
            if(!empty($_GET["date"])){
                if(DateTime::createFromFormat('d/m/y H:i:s', str_replace("-"," ",$_GET["date"]))){
                    $jsonData=getData(str_replace("-"," ",$_GET["date"]),false);
                    $jsonData["date"]=str_replace("-"," ",$_GET["date"]);
                }else{
                    $error=true;
                    $errorMsg="Invalid date format";
                }
            }else{
                $jsonData=getData();
            }
            break;
        case "stats":
            $jsonData=getStats();
            break;
        case "aggregate":
            $jsonData=getAggregate();
            break;
    }
}

header('Content-Type: application/json');
echo(json_encode($jsonData));