<?php include("preprocess.php");

if(!empty($_GET["download"]) && $_GET["download"]=="historique_signatures_rip.csv"){
    $historiqueCsv="\ndate,signatures,augmentation,pourcentage\n";
    $prevStatTotal=0;
    foreach($stats as $date => $stat){
        $historiqueCsv.=$date.",".$stat["total"].",".($stat["total"]-$prevStatTotal).",".$stat["pourcentage"]."\n";
    }
    $historiqueCsv.="Maintenant,".$total.",".($total-end($stats)["total"]).",".$pourcentage."\n//Données CC-BY David Libeau & contributeurs de https://rip-le-compteur.dav.li/,,,";
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=historique_signatures_rip.csv");
    header("Pragma: no-cache");
    header("Expires: 0");
    
}else{

$title="Historique des signatures du référendum ADP";
$description="Le comptage des signatures jour par jour depuis le début de la pétition contre la privatisation d'ADP.";
$custom_header='<link rel="stylesheet" type="text/css" href="lib/chartist/chartist.min.css"/>';
include("head.inc");

$stats_array=[];
foreach($stats as $date => $stat){
    $stat["date"]=$date;
    $stats_array[]=$stat;
}

?>
<style>
    #graph {
        overflow-y: auto;
    }

    #graph>svg {
        min-width: 800px;
    }

    .ct-label.ct-horizontal {
        position: relative;
        transform: rotate(45deg);
        bottom: -8px;
        left: -8px;
    }

    .ct-series-a .ct-bar,
    .ct-series-a .ct-line,
    .ct-series-a .ct-point,
    .ct-series-a .ct-slice-donut {
        stroke: #777676;
    }

    .ct-series-b .ct-bar,
    .ct-series-b .ct-line,
    .ct-series-b .ct-point,
    .ct-series-b .ct-slice-donut {
        stroke: #4e6eba;
    }
</style>

<main>

    <section class="centered">
        <h2><img src="twemoji/stats.png" alt="emoji poing" /> Historique</h2>
    </section>

    <section>
        <?php $joursRestants=DateTime::createFromFormat('d/m/y H:i:s', "12/03/20 23:59:59")->diff($now)->format("%a");
        $joursDepuisLeDebut=DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 00:00:00")->diff($now)->format("%a");
        ?>
        <p>Les premières signatures ont été validées le <strong>18 juin 2019</strong>. Il y a <strong><?php echo($joursDepuisLeDebut); ?> jours</strong>.</p>
        <p>La pétition se termine le <strong>12 mars 2020</strong>. Il reste donc <strong><?php echo($joursRestants); ?> jours</strong> avant la fin de la pétition.</p>
        <p>Il faudrait maintenir une augmentation de <strong><?php echo(number_format((4717396-$total)/$joursRestants, 0, ',', '&nbsp;')); ?> signatures par jour</strong> pour pouvoir atteindre les 4&nbsp;717&nbsp;396 nécessaires.</p>
    </section>

    <section>
        <h3>Graphique</h3>
        <div class="content" id="graph"></div>
        <p>Ce graphique montre la courbe théorique (en gris) qu'il faudrait suivre pour atteindre les 4,7 millions de signatures et la courbe des signatures (en bleu) chaque jour.</p>
    </section>

    <section>
        <h3>Estimations</h3>
        <?php $signaturesManquantes=4717396-$total; ?>
        <p>Il manque <strong><?php echo(number_format($signaturesManquantes, 0, ',', '&nbsp;')); ?> signatures</strong> pour atteindre les 4&nbsp;717&nbsp;396 signatures nécessaires.</p>
        <p>Si on réunit autant de signatures que depuis le début, il faudrait encore <strong><?php echo(number_format($joursDepuisLeDebut/$total*$signaturesManquantes, 0, ',', '&nbsp;')); ?> jours</strong> pour obtenir les 4&nbsp;717&nbsp;396 signatures nécessaires.</p>
        
        <?php $derniersJours=30; $augmentation=0; for($s=sizeof($stats_array)-1;$s>sizeof($stats_array)-($derniersJours+1);$s--){ $augmentation+=$stats_array[$s]["total"]-$stats_array[$s-1]["total"]; } $augmentation=$augmentation/$derniersJours; ?>
        <p>Si on garde la même augmentation de signatures que lors de ces <?php echo($derniersJours); ?> derniers jours, il faudrait encore <strong><?php echo(number_format((4717396-$total)/$augmentation, 0, ',', '&nbsp;')); ?> jours</strong> pour obtenir les 4&nbsp;717&nbsp;396 signatures nécessaires. Et à la date du 12 mars 2020, on aurait réunit <strong><?php echo(number_format($total+$joursRestants*$augmentation, 0, ',', '&nbsp;')); ?> signatures</strong>.</p>
        
        <?php $derniersJours=15; $augmentation=0; for($s=sizeof($stats_array)-1;$s>sizeof($stats_array)-($derniersJours+1);$s--){ $augmentation+=$stats_array[$s]["total"]-$stats_array[$s-1]["total"]; } $augmentation=$augmentation/$derniersJours; ?>
        <p>Si on garde la même augmentation de signatures que lors de ces <?php echo($derniersJours); ?> derniers jours, il faudrait encore <strong><?php echo(number_format((4717396-$total)/$augmentation, 0, ',', '&nbsp;')); ?> jours</strong> pour obtenir les 4&nbsp;717&nbsp;396 signatures nécessaires. Et à la date du 12 mars 2020, on aurait réunit <strong><?php echo(number_format($total+$joursRestants*$augmentation, 0, ',', '&nbsp;')); ?> signatures</strong>.</p>
        
        <?php $derniersJours=7; $augmentation=0; for($s=sizeof($stats_array)-1;$s>sizeof($stats_array)-($derniersJours+1);$s--){ $augmentation+=$stats_array[$s]["total"]-$stats_array[$s-1]["total"]; } $augmentation=$augmentation/$derniersJours; ?>
        <p>Si on garde la même augmentation de signatures que lors de ces <?php echo($derniersJours); ?> derniers jours, il faudrait encore <strong><?php echo(number_format((4717396-$total)/$augmentation, 0, ',', '&nbsp;')); ?> jours</strong> pour obtenir les 4&nbsp;717&nbsp;396 signatures nécessaires. Et à la date du 12 mars 2020, on aurait réunit <strong><?php echo(number_format($total+$joursRestants*$augmentation, 0, ',', '&nbsp;')); ?> signatures</strong>.</p>
        
        <?php $derniersJours=3; $augmentation=0; for($s=sizeof($stats_array)-1;$s>sizeof($stats_array)-($derniersJours+1);$s--){ $augmentation+=$stats_array[$s]["total"]-$stats_array[$s-1]["total"]; } $augmentation=$augmentation/$derniersJours; ?>
        <p>Si on garde la même augmentation de signatures que lors de ces <?php echo($derniersJours); ?> derniers jours, il faudrait encore <strong><?php echo(number_format((4717396-$total)/$augmentation, 0, ',', '&nbsp;')); ?> jours</strong> pour obtenir les 4&nbsp;717&nbsp;396 signatures nécessaires. Et à la date du 12 mars 2020, on aurait réunit <strong><?php echo(number_format($total+$joursRestants*$augmentation, 0, ',', '&nbsp;')); ?> signatures</strong>.</p>
        
    </section>

    <section>
        <h3>Tableau complet</h3>
        <table id="historique" class="content">
            <thead>
                <tr>
                    <td>Date</td>
                    <td>Total</td>
                    <td>Augmentation</td>
                    <td>Pourcentage</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $historiqueCsv="date,signatures,augmentation,pourcentage\n";
                    $prevStatTotal=0;
                    $graphDate="";
                    $graphSignatures="";
                    $signaturesParJourTheorique=4717396/DateTime::createFromFormat('d/m/y H:i:s', "12/03/20 23:59:59")->diff(DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 01:00:00"))->format("%a");
                    $jours=0;
                    $graphTheorique="";
                    foreach($stats as $date => $stat){
                        echo('<tr><td>'.$date.'</td><td>'.number_format($stat["total"], 0, ',', '&nbsp;').'</td><td>+'.number_format($stat["total"]-$prevStatTotal, 0, ',', '&nbsp;').'</td><td>'.number_format($stat["pourcentage"], 2, ',', ' ').'&nbsp;%</td></tr>');
                        $historiqueCsv.=$date.",".$stat["total"].",".($stat["total"]-$prevStatTotal).",".$stat["pourcentage"]."\n";
                        $jours++;
                        $prevStatTotal=$stat["total"];
                        $graphDate.='"'.$date.'",';
                        $graphSignatures.=$stat["total"].",";
                        $graphTheorique.=($signaturesParJourTheorique*$jours).",";
                    }
                    echo('<tr><td>Maintenant</td><td>'.number_format($total, 0, ',', '&nbsp;').'</td><td>+'.number_format($total-end($stats)["total"], 0, ',', '&nbsp;').'</td><td>'.number_format($pourcentage, 2, ',', ' ').'&nbsp;%</td></tr>');
                    $historiqueCsv.="Maintenant,".$total.",".($total-end($stats)["total"]).",".$pourcentage."\n";
                    $jours++;
                    $graphDate.='"Maintenant"';
                    $graphSignatures.=$total;
                    $graphTheorique.=$signaturesParJourTheorique*$jours;
                ?>
            </tbody>
        </table>
        <p><i class="fa fa-download" aria-hidden="true"></i> <a href="data:text/csv;charset=utf-8,<?php echo(urlencode($historiqueCsv)); ?>//Données CC-BY David Libeau & contributeurs de https://rip-le-compteur.dav.li/,,," download="historique_signatures_rip.csv">Télécharger ce tableau au format CSV</a></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Dataset",
        "name": "Signatures pour le référendum d'ADP par jour",
        "description": "Historique des signatures pour le référendum d'ADP jour par jour depuis le début de la pétition.",
        "url": "https://rip-le-compteur.dav.li/historique",
        "keywords": [
            "aeroport",
            "aéroport",
            "aeroports-de-paris",
            "aéroports de paris",
            "referendum",
            "référendum",
            "referendum-initiative-partagee",
            "référendum d'initiative partagée",
            "RIP",
            "ADP"
        ],
        "creator": {
            "@type": "Person",
            "url": "https://davidlibeau.fr",
            "name": "David Libeau"
        },
        "distribution": [{
                "@type": "DataDownload",
                "encodingFormat": "CSV",
                "contentUrl": "https://rip-le-compteur.dav.li/historique?download=historique_signatures_rip.csv"
            }
        ],
        "temporalCoverage": "2019-06-18/20<?php echo(date("y-m-d")); ?>",
        "spatialCoverage": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "France"
            }
        },
        "license":"https://creativecommons.org/licenses/by/4.0/"
    }
</script>
<script src="lib/chartist/chartist.min.js"></script>
<script>
    $(function() {
        new Chartist.Line("#graph", {
            labels: [<?php echo($graphDate); ?>],
            series: [
                [<?php echo($graphTheorique); ?>],
                [<?php echo($graphSignatures); ?>],
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                bottom: 50,
                right: 50,
                left: 20,
            },
            height: "500px"
        });

        setTimeout(function() {
            $(document).tooltip({
                items: ".ct-point",
                content: function() {
                    return $(".ct-labels>:nth-child(" + $(this).index() + ")").text() + " : " + Math.round($(this).attr("ct:value")).toLocaleString("fr-FR") + " signatures";
                }
            });
        }, 1000);

    });
</script>
</body>

</html>

<?php } ?>