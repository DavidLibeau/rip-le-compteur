<?php include("preprocess.php");

if(!empty($_POST["digrammes"])){
    $postDigrammes=explode("\n",$_POST["digrammes"]);
    $userDigrammes=[];
    foreach($postDigrammes as $line){
        $digramme=explode(",",$line);
        if(ctype_alpha($digramme[0]) && strlen($digramme[0])==2){
            $userDigrammes[$digramme[0]]=intval($digramme[1]);
        }
    }
}

$title="Vérificateur des autres compteurs";
$description="Vérifier le nombre de signatures affiché par les autres compteurs (adprip.fr, compteur.rip ou adp.sonntag.fr).";
include("head.inc"); ?>

<style>
table thead{
    background: none;
}
table th{
    border: 0;
}
</style>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/verification.png" alt="emoji verification"/> Vérifier</h2>
        <?php if(!empty($_GET["source"])){ 
            switch($_GET["source"]){
                case "compteurrip":
                    $source_txt='de <a href="https://compteur.rip/" target="_blank">Compteur.RIP</a>';
                    $source_description="Compteur.RIP est le compteur mis en place par des membres du Discord Insoumis. Il utilise un algorithme de reconnaissance d'image automatique pour résoudre les captchas. Il propose également un moteur de recherche nom/prénom simplifié permettant de retrouver un signataire bien plus facilement que sur le site web du ministère.";
                    $source_licence="Les données sont proposées sous licence CC-BY (licence temporaire, autorisation d'utilisation de leur API donnée pour rip-le-compteur.dav.li).";
                    break;
                case "adpripfr":
                    $source_txt='de <a href="https://adprip.fr/" target="_blank">adprip.fr</a>';
                    $source_description="Adprip.fr est un compteur mis en place un groupe de citoyens. Il a longtemps utilisé des travailleurs pauvres pour résoudre les captchas mais utilise actuellement un algorithme de reconnaissance d'image automatique. Il propose différentes représentations dont des cartes interactives.";
                    $source_licence='Les données sont proposées sous la Licence Ouverte (v1.0).';
                    break;
                case "vincib":
                    $source_txt='du <a href="https://adp.sonntag.fr/" target="_blank">compteur de Benjamin Sonntag</a>';
                    $source_description="Ce compteur est l'un des tout premier compteur créé. L'initiative provient de Benjamin Sonntag, membre de La Quadrature du Net. Il propose des données ouvertes mais peu mises à jour.";
                    $source_licence="Les données sont proposées sous licence CC0 (domaine public).";
                    break;
            }
        ?>
        <p>Vérifier les données <?php echo($source_txt); ?>.</p>
        <p style="font-style:italic; font-weight:bold"><?php echo($source_description); ?></p>
        <p style="font-style:italic"><?php echo($source_licence); ?></p>
        <? }else{ ?>
        <p>Vérifier les données de <a href="https://compteur.rip/" target="_blank">Compteur.RIP</a>, de <a href="https://adprip.fr/" target="_blank">adprip.fr</a> et du <a href="https://adp.sonntag.fr/" target="_blank">compteur de Benjamin Sonntag</a>.</p>
        <?php } ?>
    </section>
    
    <section>
        <p>Le premier onglet permet de remarquer les différence entre chaque compteur. Le deuxième calcule le total. Et pour finir, vous pouvez importer vos propres données dans le dernier onglet.</p>
    </section>

    <section>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Différence</a></li>
                <li><a href="#tabs-2">Total</a></li>
                <li><a href="#tabs-3">Vos propres données</a></li>
            </ul>
            <div id="tabs-1">
                <label id="affichageCompact"><input type="checkbox"> Afficher seulement les digrammes comportant une différence positive (affichage compact)</label>
                <table class="stats">
                    <thead>
                        <tr>
                            <th><span>Digramme</span></th>
                            <th><span>rip-le-compteur</span></th>
                            <?php if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){ ?>
                            <th><span>Compteur.RIP</span></th>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){ ?>
                            <th><span>adprip.fr</span></th>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){ ?>
                            <th><span>ADP.sonntag.fr</span></th>
                            <?php } if(!empty($_POST["digrammes"])){ ?>
                            <th><span>Vos données</span></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $diff_compteurrip=0;
                        $diff_adprip=0;
                        $diff_vincib=0;
                        $diff_userDigrammes=0;
                        foreach ($bdd_best as $s => $donnees) {
                            if(!empty($ctrip_bdd[$s]) && $donnees["compteur"]<$ctrip_bdd[$s] && (empty($_GET["source"]) || $_GET["source"]=="compteurrip") || !empty($vincib_bdd[$s]) && $donnees["compteur"]<$vincib_bdd[$s] && (empty($_GET["source"]) || $_GET["source"]=="vincib") || !empty($adprip_bdd[$s]) && $donnees["compteur"]<$adprip_bdd[$s] && (empty($_GET["source"]) || $_GET["source"]=="adpripfr") || !empty($_POST["digrammes"]) && !empty($userDigrammes[$s]) && $donnees["compteur"]<$userDigrammes[$s]){
                                echo('<tr class="diff">');
                            }else{
                                echo('<tr>');
                            }
                            
                            $section=str_replace("_","",$s);
                            echo('<td><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$donnees["nb_pages"].'" target="_blank">'.$s.'</a></td>');
                            echo('<td class="main">'.$donnees["compteur"].'</td>');
                            
                            if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){
                                if(isset($ctrip_bdd[$s])){
                                    if($donnees["compteur"]<$ctrip_bdd[$s]){
                                        $diff_compteurrip+=$ctrip_bdd[$s]-$donnees["compteur"];
                                        echo('<td class="diff sup">'.$ctrip_bdd[$s].'</td>');
                                    }else if($donnees["compteur"]>$ctrip_bdd[$s]){
                                        echo('<td class="diff inf">'.$ctrip_bdd[$s].'</td>');
                                    }else{
                                        echo('<td>'.$ctrip_bdd[$s].'</td>');
                                    }
                                }else{
                                    echo('<td>NaN</td>');
                                }
                            }


                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){
                                if(isset($adprip_bdd[$s])){
                                    if($donnees["compteur"]<$adprip_bdd[$s]){
                                        $diff_adprip+=$adprip_bdd[$s]-$donnees["compteur"];
                                        echo('<td class="diff sup">'.$adprip_bdd[$s].'</td>');
                                    }else if($donnees["compteur"]>$adprip_bdd[$s]){
                                        echo('<td class="diff inf">'.$adprip_bdd[$s].'</td>');
                                    }else{
                                        echo('<td>'.$adprip_bdd[$s].'</td>');
                                    }
                                }else{
                                    echo('<td>NaN</td>');
                                }
                            }
                            
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){
                                if(isset($vincib_bdd[$s])){
                                    if($donnees["compteur"]<$vincib_bdd[$s]){
                                        $diff_vincib+=$vincib_bdd[$s]-$donnees["compteur"];
                                        echo('<td class="diff sup">'.$vincib_bdd[$s].'</td>');
                                    }else if($donnees["compteur"]>$vincib_bdd[$s]){
                                        echo('<td class="diff inf">'.$vincib_bdd[$s].'</td>');
                                    }else{
                                        echo('<td>'.$vincib_bdd[$s].'</td>');
                                    }
                                }else{
                                    echo('<td>NaN</td>');
                                }
                            }
                            
                            if(!empty($_POST["digrammes"])){
                                if(isset($userDigrammes[$s])){
                                    if($donnees["compteur"]<$userDigrammes[$s]){
                                        $diff_userDigrammes+=$userDigrammes[$s]-$donnees["compteur"];
                                        echo('<td class="diff sup">'.$userDigrammes[$s].'</td>');
                                    }else if($donnees["compteur"]>$userDigrammes[$s]){
                                        echo('<td class="diff inf">'.$userDigrammes[$s].'</td>');
                                    }else{
                                        echo('<td>'.$userDigrammes[$s].'</td>');
                                    }
                                }else{
                                    echo('<td>NaN</td>');
                                }
                            }
                            
                            echo('</tr>'."\n");
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td title="Total des différences positives">Total</td>
                            <td></td>
                            <?php if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){ ?>
                            <td><?php echo($diff_compteurrip); ?></td>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){ ?>
                            <td><?php echo($diff_adprip); ?></td>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){ ?>
                            <td><?php echo($diff_vincib); ?></td>
                            <?php } if(!empty($_POST["digrammes"])){ ?>
                            <td><?php echo($diff_userDigrammes); ?></td>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="tabs-2">
                <table class="stats">
                    <thead>
                        <tr>
                            <th><span></span></th>
                            <th><span>rip-le-compteur</span></th>
                            <?php if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){ ?>
                            <th><span>Compteur.RIP</span></th>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){ ?>
                            <th><span>adprip.fr</span></th>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){ ?>
                            <th><span>ADP.sonntag.fr</span></th>
                            <?php } if(!empty($_POST["digrammes"])){ ?>
                            <th><span>Vos données</span></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_riplecompteur=0;
                        $total_compteurrip=0;
                        $total_adprip=0;
                        $total_vincib=0;
                        $total_userDigrammes=0;
                        foreach ($bdd_best as $s => $donnees) {
                            $total_riplecompteur+=$donnees["compteur"];
                            
                            if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){
                                if(isset($ctrip_bdd[$s])){
                                    $total_compteurrip+=$ctrip_bdd[$s];
                                }
                            }
                            
                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){
                                if(isset($adprip_bdd[$s])){
                                    $total_adprip+=$adprip_bdd[$s];
                                }
                            }
                            
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){
                                if(isset($vincib_bdd[$s])){
                                    $total_vincib+=$vincib_bdd[$s];
                                }
                            }
                            
                            if(!empty($_POST["digrammes"])){
                                if(isset($userDigrammes[$s])){
                                    $total_userDigrammes+=$userDigrammes[$s];
                                }
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total</td>
                            <td><?php echo($total_riplecompteur); ?></td>
                            <?php if(empty($_GET["source"]) || $_GET["source"]=="compteurrip"){ ?>
                            <td><?php echo($total_compteurrip); ?></td>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="adpripfr"){ ?>
                            <td><?php echo($total_adprip); ?></td>
                            <?php }
                            if(empty($_GET["source"]) || $_GET["source"]=="vincib"){ ?>
                            <td><?php echo($total_vincib); ?></td>
                            <?php } if(!empty($_POST["digrammes"])){ ?>
                            <td><?php echo($total_userDigrammes); ?></td>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="tabs-3">
                <form action="" method="post">
                    <p>Importer vos propres données au format CSV :</p>
                    <p>
                        <input type="submit"/>
                    </p>
                    <p>
                        <textarea name="digrammes"><?php
                            if(!empty($_POST["digrammes"])){
                                foreach($userDigrammes as $digramme => $value){
                                    echo($digramme.",".$value."\n");
                                }
                            }else{
                                $alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                                for ($i=0; $i<26; $i++) {//par ordre alphabéthique
                                    $section=$alphabet[$i]."_";
                                    echo($section.",0\n");
                                    for ($j=0; $j<26; $j++) {
                                        $section=$alphabet[$i].$alphabet[$j];
                                        echo($section.",0\n");
                                    }
                                }   
                            }
                        ?></textarea>
                    </p>
                    <p>
                        <input type="submit"/>
                    </p>
                </form>
            </div>
        </div>
    </section>

</main>
<?php include("footer.inc"); ?>
    <script>
        function affichageCompactFilter(){
            if($("#affichageCompact input").is(":checked")){
                $(".stats tbody tr").hide();
                $(".stats tbody tr.diff").show();
            }else{
                $(".stats tbody tr").show();
            }
        }
        $(function() {
            $("#tabs").tabs();
            
            $("#affichageCompact").click(affichageCompactFilter);
            affichageCompactFilter();
        });
    </script>
</body>
</html>