<?php include("preprocess.php");

$title="Statistiques des contributions au compteur";
$description="Toutes les statistiques des contribution au compteur de signatures RIP, le compteur.";
$custom_header='<link rel="stylesheet" type="text/css" href="lib/chartist/chartist.min.css"/>';
include("head.inc");

$stats_pres=json_decode(file_get_contents("geo/cache-stats-geo.txt"),true)["stats_pres"];

?>
<style>
    #graph {
        overflow-y: auto;
    }

    #graph>svg {
        min-width: 800px;
    }

    .ct-label.ct-horizontal {
        position: relative;
        transform: rotate(45deg);
        bottom: -8px;
        left: -8px;
    }

    .ct-series-a .ct-bar,
    .ct-series-a .ct-line,
    .ct-series-a .ct-point,
    .ct-series-a .ct-slice-donut {
        stroke: #4e6eba;
    }
</style>


<main>
    
    <section class="centered">
        <h2><img src="twemoji/contributions.png" alt="emoji stats contributions" /> Statistiques des contributions</h2>
    </section>
    
    <section>
        <h3>Statistiques générales</h3>
        <p>Sur ce compteur, <strong><?php echo(number_format($contribUnique, 0, ',', '&nbsp;')); ?></strong> contributeur·rice·s ont déposé <strong><?php echo(number_format($totalContrib, 0, ',', '&nbsp;')); ?></strong> contributions !</p>
        <p>Cela signifie que <?php echo(number_format($totalContrib, 0, ',', '&nbsp;')); ?> captcha (systèmes anti robot) ont été résolus par des bénévoles sur le <a href="https://www.referendum.interieur.gouv.fr/" target="_blank" rel="noopener noreferrer">site web ADP RIP du ministère de l'Intérieur</a> pour fournir chaque jour le nombre total de signatures affiché sur <em>RIP, le compteur</em>.</p>
        <h3>Statistiques personnalisées</h3>
        <?php if(!empty($_POST["consent"]) && $_POST["consent"]=="true"){ 
        $totalContribPerso=getStatPerso();
        $tweet="J'ai résolu $totalContribPerso captcha au total pour compter les signatures du #ReferendumADP via le compteur collaboratif";
        $tweet_html="J'ai résolu $totalContribPerso captcha au total pour compter les signatures du <em>#ReferendumADP</em> via le compteur collaboratif <em>rip-le-compteur.dav.li</em>";
        ?>    
        <p>Vous avez personnellement envoyé <strong><?php echo($totalContribPerso); ?></strong> contributions sur ce compteur.</p>
        <p style="margin: 30px auto; text-align:center;"><a id="tweetButton" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Frip-le-compteur.dav.li/&ref_src=twsrc%5Etfw&text=<?php echo(urlencode($tweet)); ?>&tw_p=tweetbutton&url=https%3A%2F%2Frip-le-compteur.dav.li/" target="_blank" rel="noopener noreferrer"><span><?php echo($tweet_html); ?></span><button>Partager votre statistique sur les réseaux sociaux <i class="fa fa-twitter" aria-hidden="true"></i></button></a></p>
        <?php }else{ ?>
        <form id="afficherStatsPerso" method="post" class="content"><input name="consent" value="true" type="hidden"/>Vous avez personnellement envoyé <button class="btn blanc">Afficher la statistique</button> contributions sur ce compteur.</form>
        <?php } ?>
    </section>

    <section>
        <h3>Graphique</h3>
        <p>Ce graphique montre l'évolution les contributions déposées chaque jour.</p>
        <div class="content" id="graph"></div>

        <h3>Tableau complet</h3>
        <table id="historique" class="content">
            <thead>
                <tr>
                    <td>Date</td>
                    <td>Contributions totales</td>
                    <td>Contributions nouvelles</td>
                    <td>Contributeur·rice·s total</td>
                    <td>Contributeur·rice·s nouveaux</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $contribCsv="date,contributions,contributeur-rice-s unique\n";
                    $prevStatTotal=0;
                    $prevStatUnique=0;
                    $graphDate="";
                    $graphContribTotal="";
                    $signaturesParJourTheorique=4717396/DateTime::createFromFormat('d/m/y H:i:s', "12/03/20 23:59:59")->diff(DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 01:00:00"))->format("%a");
                    $jours=0;
                    $graphTheorique="";
                    foreach($stats as $date => $stat){
                        echo('<tr><td>'.$date.'</td><td>'.number_format($stat["totalContrib"], 0, ',', '&nbsp;').'</td><td>+'.number_format($stat["totalContrib"]-$prevStatTotal, 0, ',', '&nbsp;').'</td><td>'.number_format($stat["contribUnique"], 0, ',', '&nbsp;').'</td><td>+'.number_format($stat["contribUnique"]-$prevStatUnique, 0, ',', '&nbsp;').'</td></tr>');
                        $contribCsv.=$date.",".$stat["totalContrib"].",".$stat["contribUnique"]."\n";
                        $graphDate.='"'.$date.'",';
                        $graphContributions.=($stat["totalContrib"]-$prevStatTotal).",";
                        $prevStatTotal=$stat["totalContrib"];
                        $prevStatUnique=$stat["contribUnique"];
                        $jours++;
                    }
                    echo('<tr><td>Maintenant</td><td>'.number_format($totalContrib, 0, ',', '&nbsp;').'</td><td>+'.number_format($totalContrib-end($stats)["totalContrib"], 0, ',', '&nbsp;').'</td><td>'.number_format($contribUnique, 0, ',', '&nbsp;').'</td><td>+'.number_format($contribUnique-end($stats)["contribUnique"], 0, ',', '&nbsp;').'</td></tr>');
                    $historiqueCsv.="Maintenant,".$totalContrib.",".$contribUnique."\n";
                    $jours++;
                    $graphDate.='"Maintenant"';
                    $graphContributions.=$totalContrib-$prevStatTotal;
                ?>
            </tbody>
        </table>
        <p><i class="fa fa-download" aria-hidden="true"></i> <a href="data:text/csv;charset=utf-8,<?php echo(urlencode($contribCsv)); ?>//Données CC-BY David Libeau & contributeurs de https://rip-le-compteur.dav.li/,," download="contrib_rip-le-compteur.csv">Télécharger ce tableau au format CSV</a></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script src="lib/chartist/chartist.min.js"></script>
<script>
    $(function() {
        new Chartist.Line("#graph", {
            labels: [<?php echo($graphDate); ?>],
            series: [
                [<?php echo($graphContributions); ?>]
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                bottom: 50,
                right: 50,
            },
            height: "500px"
        });

        setTimeout(function() {
            $(document).tooltip({
                items: ".ct-point",
                content: function() {
                    return $(".ct-labels>:nth-child(" + $(this).index() + ")").text() + " : " + Math.round($(this).attr("ct:value")).toLocaleString("fr-FR") + " contributions";
                }
            });
        }, 1000);

        $("#afficherStatsPerso>button").click(function(){
            return confirm("Pour voir votre statistique personnalisée, ce site web doit utiliser votre adresse IP pour identifier les contributions envoyées avec la même IP.\nDonnez-vous votre consentement à l'utilisation de votre adresse IP pour afficher un contenu personnalisé sur ce site web ?\n(Toutes les IP sont anonymisées. Votre adresse IP est uniquement utilisée pour cette page et votre consentement est effacé lorsque vous changez de page).");
        });
    });
</script>
</body>
</html>