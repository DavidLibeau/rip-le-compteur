<?php 
include("preprocess.php");

header("Content-type: text/xml");
echo('<?xml version="1.0"?>');
?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">

<?php
$title="Notifications";
$description="Infos et nouvelles concernant le RIP d'ADP.";
$isXml=true;
include("head.inc");

?>
<style>
    .timeline{
        padding-left: 25%;
        width: 75%;
        list-style: none;
    }
    .timeline>li{
        position: relative;
        padding: 10px;
        border-left: 2px solid #3c3c3c;
    }
    .timeline>li>*{
        width: auto;
        margin: 0!important;
    }
    .timeline>li:before{
        content: "";
        position: absolute;
        width: 10px;
        height: 10px;
        top: 10px;
        left: -9px;
        border-radius: 50%;
        background-color: #3c3c3c;
        border: 3px solid #fdfdfd;
    }
    .timeline>li:hover:before{
        background-color: black;
    }
    .timeline>li>.date{
        position: absolute;
        top: 10px;
        right: calc(100% + 20px);
        width: calc(25% - 20px);
        text-align: right;
    }
    .hashtag{
        font-weight: normal;
        color: #b7b7b7;
        margin-right: 6px;
    }
    .hashtag:before{
        content: "#";
    }
    
    #abonnement input{
        padding: 5px;
        background-color: #f0f0f0;
        border: 2px solid white;
        border-radius: 5px;
        text-align: right;
        text-overflow: ellipsis;
    }
    
    iframe{
        border: none;
        float: right;
        margin-top: -10px;
    }
    @media screen and (max-width: 800px) {
        #abonnement>p {
            text-align: right;
        }
        iframe{
            display: block;
            width: 100%;
            float: none;
            margin-top: 20px;
            margin-bottom: -10px;
        }
    }
</style>
<xsl:if test="//channel/category = 'SoloItem'">
    <style>
    .timeline>li:before{
        border-color: #f0f0f0;
    }
    </style>
</xsl:if>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/cloche.png" alt="emoji cloche" />&#160;<xsl:value-of select="//channel/title"/></h2>
        <p><xsl:value-of select="//channel/description"/></p>
    </section>
    <xsl:if test="boolean(//channel/category) = false or //channel/category != 'SoloItem'">
    <section id="abonnement">
        <p><label for="rss">S'abonner au flux RSS : </label>
            <xsl:choose>
            <xsl:when test="//channel/category">
                <xsl:element name="input" >
                    <xsl:attribute name="value">https://rip-le-compteur.dav.li/notifications_<xsl:value-of select="//channel/category"/>.rss</xsl:attribute>
                    <xsl:attribute name="type">
                      <xsl:text>text</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="name">
                      <xsl:text>rss</xsl:text>
                    </xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="input" >
                    <xsl:attribute name="value">https://rip-le-compteur.dav.li/notifications.rss</xsl:attribute>
                    <xsl:attribute name="type">
                      <xsl:text>text</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="name">
                      <xsl:text>rss</xsl:text>
                    </xsl:attribute>
                </xsl:element>
                <iframe src="https://notifications.rip-le-compteur.dav.li/" height="40" width="270">Service de notifications hors-ligne</iframe>
            </xsl:otherwise>
            </xsl:choose>
        </p>
    </section>
    </xsl:if>
    <section>
        <div class="content">
            <ul class="timeline">
            <xsl:for-each select="//item">
                <li>
                    <h3>
                        <a>
                            <xsl:attribute name="href">notifications_<xsl:value-of select="guid"/></xsl:attribute>
                            <xsl:value-of select="title"/>
                        </a>
                    </h3>
                    <p class="date">
                        <xsl:value-of select="pubDate"/>
                    </p>
                    <p>
                        <xsl:for-each select="./category">
                            <a class="hashtag">
                                <xsl:attribute name="href">notifications_<xsl:value-of select="."/></xsl:attribute>
                                <xsl:value-of select="."/>
                            </a>
                        </xsl:for-each>
                    </p>
                    <p><xsl:value-of select="description"/></p>
                </li>
            </xsl:for-each>
            </ul>
        </div>
    </section>
    <section class="centered">
            <xsl:choose>
            <xsl:when test="//channel/category">
                <p><a href="notifications" class="btn"><i class="fa fa-arrow-left" aria-hidden="true"></i> Afficher toutes les notifications</a></p>
            </xsl:when>
            <xsl:otherwise>
                <p><a href="/" class="btn"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil</a></p>
            </xsl:otherwise>
            </xsl:choose>
    </section>

</main>
<?php include("footer.inc"); ?>
<xsl:if test="//channel/category = 'SoloItem'">
<xsl:element name="script" >
    <xsl:attribute name="type">
        <xsl:text>application/ld+json</xsl:text>
    </xsl:attribute>
    {
    "@context": "https://schema.org",
    "@type": "NewsArticle",
    "headline": "<xsl:value-of select="//item/title"/>",
    "datePublished": "<xsl:value-of select="//item/dateIso"/>",
    "author": {
        "@type": "Person",
        "name": "David Libeau"
      },
    "publisher": {
        "@type": "Organization",
        "name": "RIP, le compteur.",
        "logo": {
            "@type": "ImageObject",
            "url":"https://rip-le-compteur.dav.li/img/icon-192.png"
        }
      },
    "image": [
        "https://rip-le-compteur.dav.li/compteur-adp-rip-image.png"
    ]
    }
</xsl:element>
</xsl:if>
    <script>
        $(function() {
        });
    </script>
</body>
</html>

<?php 
echo('</xsl:template>');
echo('</xsl:stylesheet>');
?>