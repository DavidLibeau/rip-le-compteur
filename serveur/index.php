<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

include("preprocess.php");

$now=new DateTime("now");
if(intval($now->format("H"))>10){
    $aujourdhuiapres10h=$now->format("d\/m\/y");
}else{
    $hier=$now;
    $hier=$hier->modify("-1 day");
    $aujourdhuiapres10h=$hier->format("d\/m\/y");
}

$description="Le ".$aujourdhuiapres10h." nous comptions ".number_format($total, 0, ',', ' ')." signatures pour le référendum ADP. Toutes les données sur rip-le-compteur.dav.li !";


if(isset($_GET["cahierdescharges"]) && $_GET["cahierdescharges"]=="minitel"){
include("minitel/index.php"); 
}else{
include("head.inc"); 
?>

    <a id="signer" href="https://signer.rip" target="_blank" rel="noopener noreferrer" title="Signer la pétition sur www.referendum.interieur.gouv.fr"><span>Signer la pétition</span><span><img src="twemoji/signer.png" alt="emoji signer" class="emoji" /></span></a>
    <main>
        <div id="description"><h1>Compteur collaboratif des signatures du référendum ADP</h1></div>
        <section class="centered rounded">
            <div>
                <?php if(sizeof($bdd_maj_priorite)!=0 && intval($now->format("H"))<12){ ?> 
                    <div id="majencours" class="alert content"><h3><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Attention, chiffre temporaire en cours de mise à jour !</h3>Il reste <?php echo(sizeof($bdd_maj_priorite)); ?> digrammes à mettre à jour pour avoir un chiffre d'une bonne précision. <a href="contribuer">Aidez-nous en contribuant <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                <?php } ?>
                <h1>
                    <span data-total="<?php echo($total); ?>"><?php echo(number_format($total, 0, ',', '&nbsp;')); ?></span>
                </h1>
                <p>Signatures comptées collaborativement pour le <a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer">référendum ADP</a>. Soit <?php echo(number_format($pourcentage, 2, ',', ' ')); ?>&nbsp;% des 4&nbsp;717&nbsp;396 signatures nécessaires.</p>
                <div class="content">
                    <div id="progressbar"><span style="width:<?php echo($pourcentage); ?>%"><span><?php echo(number_format($pourcentage, 2, ',', ' ')); ?>&nbsp;%</span> </span>
                    </div>
                </div>

                <?php
                    $tweet="";
                    $tweet_html="";
                    for($i=1;$i<=10;$i++){
                        if($i<=round($pourcentage,-1)/10){
                            $tweet.="🟩";
                            $tweet_html.='<img src="twemoji/carre_vert.png" alt="emoji carré vert"/>';
                        }else{
                            $tweet.="⬜️";
                            $tweet_html.='<img src="twemoji/carre_blanc.png" alt="emoji carré blanc"/>';
                        }
                    }
                    $tweet.=" ".number_format($pourcentage, 2, ',', ' ')." %\n".number_format($total, 0, ',', ' ')." signatures pour le #ReferendumADP🛫 selon le compteur collaboratif #CompteurADP";
                    $tweet_html.=" ".number_format($pourcentage, 2, ',', ' ').'&nbsp;%<br/>'.number_format($total, 0, ',', ' ').' signatures pour le <em>#ReferendumADP</em><img src="twemoji/avion_decolle.png" alt="emoji avion qui décolle"/> selon le compteur collaboratif <em>#CompteurADP</em><br/><em>https://rip-le-compteur.dav.li/</em>';
                ?>
                <p style="margin: 30px auto"><a id="tweetButton" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Frip-le-compteur.dav.li/&ref_src=twsrc%5Etfw&text=<?php echo(urlencode($tweet)); ?>&tw_p=tweetbutton&url=https%3A%2F%2Frip-le-compteur.dav.li/" target="_blank" rel="noopener noreferrer"><span><?php echo($tweet_html); ?></span><button>Partager ce compteur collaboratif sur les réseaux sociaux <i class="fa fa-twitter" aria-hidden="true"></i></button></a></p>
                <a id="minitel" href="?cahierdescharges=minitel" title="Cliquez pour voir la version minitel de ce site web" rel="nofollow"></a>
            </div>
            <ul id="sources">
                <li>
                    <span class="date"><?php if(sizeof($bdd_maj_priorite)!=0  && intval($now->format("H"))<12){ ?><span class="badge warning">En cours de mise à jour</span> <?php }else{ echo($aujourdhuiapres10h); ?> <span class="badge success">À jour</span><?php } ?></span>
                    <span class="banner"><img src="minitel/generate_fake_captcha.php?content=<?php echo(number_format($total_digramme, 0, ',', '&nbsp;')); ?> signatures&w=270&h=135&sign=référendum d'initiative partagée" alt="<?php echo(number_format($total_digramme, 0, ',', '&nbsp;')); ?> signatures" /></span>
                    <h3>Comptage collaboratif</h3>
                    <p>Participer au comptage collaboratif mis en place par ce compteur.</p>
                    <a href="contribuer" class="btn">Contribuer</a>
                </li>
                <li>
                    <span class="date">Maintenant</span>
                    <span class="banner"><i class="fa fa-search" aria-hidden="true"></i></span>
                    <h3>Vérification personnalisée</h3>
                    <p>Rechercher un digramme spécifique et/ou vérifier avec vos propres données.</p>
                    <a href="verifier" class="btn">Vérifier</a>
                </li>
                <li>
                    <span class="date">11/09/19</span>
                    <span class="banner" style="background-image:url(img/Conseil_Constitutionnel_photo-CC-BY-Mbzt.jpg)"></span>
                    <h3>Conseil constitutionnel</h3>
                    <p>Lire le dernier communiqué du Conseil constitutionnel.</p>
                    <a href="https://www.conseil-constitutionnel.fr/dossier-referendum-d-initiative-partagee-rip" class="btn" target="_blank" rel="noopener noreferrer">Lire</a>
                </li>
                <li>
                    <span class="date"><?php echo(date_create($aggregate["liberation"]["date"])->format("d\/m\/y H:i"));?></span>
                    <span class="banner" style="background-image:url(img/liberation-compteur-adp.jpg)"></span>
                    <h3>Libé</h3>
                    <p>Comparer avec le comptage quotidien des journalistes de Libération.</p>
                    <a href="<?php echo($aggregate["liberation"]["link"]); ?>" class="btn" target="_blank" rel="noopener noreferrer">Lire</a>
                </li>
                <li>
                    <span class="date"><?php echo($aujourdhuiapres10h); ?></span>
                    <span class="banner" style="background-image:url(img/compteurrip.gif)"></span>
                    <h3>Compteur.RIP</h3>
                    <p>Vérifier les données du compteur du Discord Insoumis.</p>
                    <a href="verifier?source=compteurrip" class="btn">Vérifier</a>
                </li>
                <li>
                    <span class="date"><?php echo($aujourdhuiapres10h); ?></span>
                    <span class="banner" style="background-image:url(img/adpripfr.jpg)"></span>
                    <h3>adprip.fr</h3>
                    <p>Vérifier les données du compteur adprip.fr.</p>
                    <a href="verifier?source=adpripfr" class="btn">Vérifier</a>
                </li>
                <li>
                    <span class="date"><?php echo(date_create($aggregate["adp.sonntag.fr"]["date"])->format("d\/m\/y")); ?></span>
                    <span class="banner" style="background-image:url(img/adpsonntagfr.jpg)"></span>
                    <h3>ADP.sonntag.fr</h3>
                    <p>Vérifier les données du compteur de Benjamin Sonntag (membre de La Quadrature Du Net).</p>
                    <a href="verifier?source=vincib" class="btn">Vérifier</a>
                </li>
            </ul>
        </section>
        <section id="avionGraph">
            <?php
                $stats_array=[];
                foreach($stats as $date => $stat){
                    $stat["date"]=$date;
                    $stats_array[]=$stat;
                }
            
                $chaqueJours=round(sizeof($stats_array)/15);
                $dayDistance=290/sizeof($stats_array);
                $signatureDistance=140/end($stats_array)["total"];
                $svgCourbe="";
                $svgPoints="";
                
                for($d=2;$d<sizeof($stats_array);$d+=$chaqueJours){
                    $x=round(($d+1)*$dayDistance);
                    $y=200-round($stats_array[$d]["total"]*$signatureDistance);
                    $svgCourbe.=" L ".$x." ".$y;
                    $svgPoints.='<circle title="'.explode(" ",$stats_array[$d]["date"])[0].' : '.number_format($stats_array[$d]["total"], 0, ',', ' ').' signatures" data-total="'.$stats_array[$d]["total"].'" data-date="'.explode(" ",$stats_array[$d]["date"])[0].'" cx="'.$x.'" cy="'.$y.'" r="3"/>';
                }
            ?>
                <svg class="content" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 350 210" style="enable-background:new 0 0 378.9 268.1;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:#687680;}
                    .st1{fill:#64ABDE;}
                    .st2{fill:#CCD6DD;}
                    .st3{fill:#9AAAB4;}
                </style>
                <g id="courbe">
                    <path d="<?php echo("M 10 200".$svgCourbe." L 310 52 M 310 200 Z"); ?>" stroke="#3c3c3c" stroke-width="2" fill="none"/>
                    <circle title="Début des données" data-total="0" data-date="17/06/19" cx="10" cy="200" r="3"></circle>
                    <?php echo($svgPoints); ?>
                </g>
                <g id="avion">
                    <circle class="st0" cx="318.2" cy="57" r="1.5"/>
                    <circle class="st0" cx="327.7" cy="53.4" r="1.5"/>
                    <path class="st1" d="M315.5,51.7c1,1.1,8-1,5.7-2.3c-7.1-4.6-12.9-4.5-11.2-2.8C312,48.4,313.4,49.8,315.5,51.7z"/>
                    <path class="st2" d="M334.2,47.9c0.4,1.9-3.8,3.9-5.7,4.3l-17.1,3.7c-1.9,0.4-9.2,0.4-9.6-1.5 c-0.4-2,4.2-4.5,6.1-4.9l19.1-4.1C328.9,45,333.8,46,334.2,47.9z"/>
                    <path class="st0" d="M330,45.5c-0.4,0.4-1,0.6-1.4,0.9c-0.5,0.4-0.4,1.8,0.8,1.4c1-0.4,1.9-1.2,2.4-1.8 C331.3,45.8,330.7,45.6,330,45.5z"/>
                    <path class="st1" d="M301.8,54c-0.2-0.8-1.3-5.9-1.5-6.7c-0.5-2.5,3.6,0.7,5.8,2.8C304.2,51.1,302.6,52.1,301.8,54z M302.6,54.2c0.9-0.8,4-1.4,2.4,1c-2.3,3.6-4.5,4-4.1,2.9C301.4,56.8,302,55.5,302.6,54.2z M314.8,53.5c0.5-1.4,7.7-2.4,6.1-0.3 c-4.6,7.1-9.9,9.4-9,7.2C313,57.9,313.6,56.1,314.8,53.5z"/>
                    <circle class="st3" cx="326.1" cy="48.6" r="1"/>
                    <circle class="st3" cx="323.2" cy="49.2" r="1"/>
                    <circle class="st3" cx="320.3" cy="49.9" r="1"/>
                    <circle class="st3" cx="317.3" cy="50.5" r="1"/>
                    <circle class="st3" cx="314.4" cy="51.2" r="1"/>
                    <circle class="st3" cx="311.5" cy="51.8" r="1"/>
                </g>
                </svg>
                <svg version="1.1" id="clouds" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 253.8 841.9 103.5" style="enable-background:new 0 253.8 841.9 103.5;" xml:space="preserve">
                    <style type="text/css">
                        .cloud0{fill:#F7FCFF;}
                        .cloud1{fill:#F1F9FE;}
                        .cloud2{fill:#fdfdfd;}
                    </style>
                    <path class="cloud0" d="M0,296.4c0,0,190.3-38.1,299.7-38.1c101.5,0,143.6,26.8,209,26.1c98.9-1.1,179.1-19.3,259-19.3
                        c46.2,0,74.1,17.6,74.1,17.6v74.6H0V296.4z"/>
                    <path class="cloud1" d="M365.3,326.8c0,0,194.3-33,295.7-33s180.9,42,180.9,42v21.4H280.5L365.3,326.8z"/>
                    <path class="cloud2" d="M0,341.5c0,0,90.2-44.6,178.7-44.6c111,0,256.9,33.6,379.9,33.6s283.3-17.1,283.3-17.1v43.9H0V341.5z"/>
                </svg>
                <svg version="1.1" id="stars" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 245.8 841.9 103.5" style="enable-background:new 0 245.8 841.9 103.5;" xml:space="preserve">
                    <style type="text/css">
                        .stars1{fill:#fdfdfd;}
                    </style>
                    <circle class="stars1" cx="59.8" cy="272.5" r="2"/>
                    <circle class="stars1" cx="542.3" cy="323.4" r="2"/>
                    <circle class="stars1" cx="557.4" cy="290.9" r="1"/>
                    <circle class="stars1" cx="18.9" cy="340.2" r="1"/>
                    <circle class="stars1" cx="725.5" cy="255.7" r="1"/>
                    <circle class="stars1" cx="659.6" cy="273.5" r="1"/>
                    <circle class="stars1" cx="513.9" cy="262.5" r="2"/>
                    <circle class="stars1" cx="780.6" cy="295.2" r="2.3"/>
                    <circle class="stars1" cx="390.8" cy="311.1" r="1.2"/>
                    <circle class="stars1" cx="497.4" cy="297.5" r="2"/>
                    <circle class="stars1" cx="612.5" cy="254.7" r="2"/>
                    <circle class="stars1" cx="420.5" cy="262.2" r="2"/>
                    <circle class="stars1" cx="831.2" cy="273.5" r="0.9"/>
                    <circle class="stars1" cx="84.4" cy="298.7" r="1.1"/>
                    <circle class="stars1" cx="211.9" cy="325.3" r="2"/>
                    <circle class="stars1" cx="242.1" cy="255.1" r="1.4"/>
                    <circle class="stars1" cx="176.3" cy="273" r="1.4"/>
                    <circle class="stars1" cx="296.4" cy="293.8" r="2"/>
                    <circle class="stars1" cx="129.1" cy="254.1" r="2.3"/>
                    <circle class="stars1" cx="348.4" cy="273.5" r="2"/>
                </svg>
            <div>
                <p><a class="btn" href="historique">Voir l'historique complet <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
            </div>
        </section>
        <section>
            <p><img src="twemoji/soleil.png" alt="emoji soleil" class="emoji"/> <strong>Un compteur collaboratif.</strong> Celles et ceux qui veulent savoir combien il y a de signatures comptent.</p>
            <p><img src="twemoji/cadenas_ouvert.png" alt="emoji cadenas ouvert" class="emoji"/> <strong>Des données ouvertes.</strong> Toutes les données sont accessibles pour tout le monde.</p>
            <p><img src="twemoji/10h.png" alt="emoji horloge 10h" class="emoji"/> <strong>Des mises à jour en continu.</strong> Dès que le site web du ministère est mis à jour, vers 10h tous les jours.</p>
            <p><img src="twemoji/aggregate.png" alt="emoji aggregateur" class="emoji"/> <strong>Un aggregateur de compteurs.</strong> Utilise les données des autres compteurs comme sources supplémentaires.</p>
            <p><img src="twemoji/verification.png" alt="emoji verification" class="emoji"/> <strong>Un vérificateur.</strong> Chacun peut vérifier ses propres données ou les données des autres compteurs.</p>
            <p class="centered" style="margin: 70px auto;"><a href="faq" class="btn">En savoir plus...</a></p>
        </section>
        <section>
            <h2><img src="twemoji/smartphone.png" alt="emoji téléphone" /> Installer l'application</h2>
            <p><strong>RIP, le compteur</strong> est maintenant disponible en application Android !</p>
            <p>Vous pouvez ainsi vous tenir informé de l'avancement du référendum ADP. En plus de cela, un <a href="notifications">service de notification</a> permet de recevoir quotidiennement le chiffre mis à jour !</p>
        </section>
        <section class="centered">
            <div class="content">
                <div class="demi"><a href="https://play.google.com/store/apps/details?id=li.dav.riplecompteur" target="_blank" rel="noopener noreferrer me"><span class="smartphone"><img src="img/rip-le-compteur-application-android.png" alt="Application Android RIP, le compteur." title="Capture d'écran de l'application Android du compteur" /></span></a></div>
                <div class="demi">
                    <h3><a href="https://play.google.com/store/apps/details?id=li.dav.riplecompteur" target="_blank" rel="noopener noreferrer me"><img src="img/icon-192.png" alt="Logo RIP, le compteur" title="Logo de l'application" style="height: 100px; margin: 20px auto;"/></a><br/>RIP, le compteur</h3>
                    <p><a href="https://play.google.com/store/apps/details?id=li.dav.riplecompteur" target="_blank" rel="noopener noreferrer me" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'application pour Android</a></p>
                </div>
            </div>
        </section>
        <section>
            <h2><img src="twemoji/poing.png" alt="emoji poing" /> Contribuer au comptage</h2>
            <p>Vous pouvez aider à améliorer cette statistique en installant un module sur votre navigateur (sur ordinateur).</p>
            <p>Le site du ministère de l'intérieur ne permet pas d'obtenir le nombre de signatures total. Pour avoir ce chiffre, il faudrait que chacun compte les signatures affichées sur le site web. Heureusement, un script permet de le faire à votre place et de transmettre ce résultat à ce site web. L'objectif est de centraliser le comptage fait par les citoyens qui ont installé l'extension sur leur navigateur et ainsi d'avoir le nombre de signataires.</p>
            <p>Après avoir installé l'extension, il vous faudra visiter les pages listant les signatures sur le site du ministère et résoudre les captcha. L'extension détectera automatiquement les signatures. Une liste des digrammes (couples de lettres) à mettre à jour est disponible <a href="contribuer">sur cette page</a>. Merci !</p>
        </section>
        <section class="centered">
            <div class="content">
                <div class="demi">
                    <h3>Etape 1</h3>
                    <p><a href="rip_le_compteur-2.1-fx.xpi" class="btn centered" style="text-align:center"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox<br/>(téléchargement direct)</a></p>
                    <p><a href="https://addons.mozilla.org/en-US/firefox/addon/rip-le-compteur/" target="_blank" rel="noopener noreferrer me" class="btn" style="text-align:center"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox<br/>(via le magasin)</a></p>
                    <p>ou</p>
                    <p><a href="https://chrome.google.com/webstore/detail/rip-le-compteur/hoopnflmlpfbbmmfiocfghejeecenbkn" target="_blank" rel="noopener noreferrer me" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Chrome</a></p>
                    <p>Version 2.1 (mise à jour le 19/08/19)</p>
                </div><div class="demi">
                    <h3>Etape 2</h3>
                    <p>Tous les jours à partir de 10h du matin :</p>
                    <p><a href="contribuer" class="btn">Mettre à jour des digrammes</a></p>
                </div>
            </div>
        </section>

</main>
<?php include("footer.inc"); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "WebSite",
  "url": "https://rip-le-compteur.dav.li/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://rip-le-compteur.dav.li/liste_communes?q={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<script>
    $(function() {
        $("#avionGraph").tooltip();

        $("#progressbar>span").each(function() {
            $(this).prop("Counter", 0).animate({
                Counter: $(this).width()
            }, {
                duration: 1000,
                easing: "easeOutQuad",
                step: function(now) {
                    $(this).css("width", Math.ceil(now));
                }
            });
        });
        $("h1>span").css("width", $('h1>span').width());
        $("h1>span").each(function() {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).attr("data-total")
            }, {
                duration: 1000,
                easing: "easeOutQuad",
                step: function(now) {
                    $(this).html(Math.ceil(now).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;"));
                }
            });
        });
        
        $("#note_estimation>button").click(function(){
            $(this).hide();
            $("#note_estimation>span").show()
        });
        
        $("#signer").css("right","25px");
        $("#signer").animate({
            rigth: "25px"
        }, 300);
    });
</script>
</body>

</html>
<?php } ?>