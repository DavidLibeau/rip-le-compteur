<?php include("preprocess.php");

$title="Signatures du RIP par vote aux présidentielles";
$description="Corrélation statistiques entre le nombre de signatures par commune et leur vote au premier de l'élection présidentielle de 2017.";
include("head.inc");

$stats_pres=json_decode(file_get_contents("geo/cache-stats-geo.txt"),true)["stats_pres"];

?>
<style>
    table td {
        text-align: center;
        width: 20%;
    }
    table td:fisrt-child{
        width: auto;
    }
</style>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/stats.png" alt="emoji stats" /> Statistiques de vote</h2>
        <p>Avec les résultats du premier tour de l'élection présidentielle de 2017.</p>
    </section>
    
    <section>
        <p>En croisant les résultats du premier tour de l'élection présidentielle de 2017 et le taux de signature du référendum d'initiative partagée par commune, nous pouvons essayer de trouver une corrélation. Le calcul du <a href="https://fr.wikipedia.org/wiki/Corr%C3%A9lation_(statistiques)#Coefficient_de_corr%C3%A9lation_lin%C3%A9aire_de_Bravais-Pearson" target="_blank" rel="noopener noreferrer">coefficient de corrélation</a> permet d'obtenir un indice statistique sur cette corrélation.</p>
        <p>Plus ce coefficient est proche de 0, moins la corrélation est forte. Un coefficient négatif indique que la corrélation est inversement proportionnelle. On admet une corrélation forte si le coefficient est suppérieur à 0,5 ou inférieur à -0,5.</p>
        <p>Comme les données sont par commune, la corrélation du vote reste moyennement fiable pour une généralisation nationale.</p>
        <p>Ces calculs ont été fait sur les communes ayant au moins une signature. Les signatures comptées sur le site web du ministère de l'Intérieur fournissent des données de mauvaise qualité. Les communes ayant un nom similaire ou les nouvelles communes ne sont pas correctement listées. Sur ces communes les votes et le nombre d'inscrits ont été additionnés. Les français inscrits à l'étranger n'ont pas été pris en compte dans les calculs du coefficient de corrélation.</p>
        <p>Les données par communes proviennent du compteur <a href="https://compteur.rip/" target="_blank" rel="noopener noreferrer">Compteur.RIP</a>.</p>
    </section>
    
    <section>
        <h3>Calculs</h3>
        <table class="content">
            <thead>
                <tr>
                    <td></td>
                    <td title="Mélenchon, Hamon, Poutou, Arthaud">Gauche</td>
                    <td title="Macron">Centre</td>
                    <td title="Fillon">Droite</td>
                    <td title="Le Pen, Dupont-Aignan">Extrême droite</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Coefficient de corrélation</td>
                    <td><?php echo(number_format($stats_pres["gauche"], 3, ',', ' ')); ?></td>
                    <td><?php echo(number_format($stats_pres["centre"], 3, ',', ' ')); ?></td>
                    <td><?php echo(number_format($stats_pres["droite"], 3, ',', ' ')); ?></td>
                    <td><?php echo(number_format($stats_pres["extDroite"], 3, ',', ' ')); ?></td>
                </tr>
            </tbody>
        </table>
        <h3>Lecture</h3>
        <?php
            unset($stats_pres["abstentionnistes"]);
            $deTexteFormat=[
                "gauche"=>"de gauche",
                "centre"=>"macronistes",
                "droite"=>"de droite",
                "extDroite"=>"d'extrême droite",
            ];
            asort($stats_pres,SORT_NUMERIC);
        ?>
        <p>Les électeur·rice·s <?php echo($deTexteFormat[end(array_keys($stats_pres))]); ?> sont ceux qui ont le plus signé. Les électeur·rice·s <?php echo($deTexteFormat[array_keys($stats_pres)[0]]); ?> sont ceux qui ont le moins signé. Et les électeur·rice·s <?php echo($deTexteFormat[array_keys($stats_pres)[2]]); ?> ont plus signé que les électeur·rice·s <?php echo($deTexteFormat[array_keys($stats_pres)[1]]); ?>.</p>
    </section>

</main>
<?php include("footer.inc"); ?>
    <script>
        $(function() {

        });
    </script>
</body>
</html>