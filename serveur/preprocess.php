<?php include("func.php");

$getData=getData();
$total=$getData["total"];
$total_digramme=$getData["total_digramme"];
$total_difference=$getData["total_difference"];
$estimation_signatures_nonaffichees=$getData["estimation_signatures_nonaffichees"];
$pourcentage=$getData["pourcentage"];
$precis=$getData["precis"];

$bdd_dated=$getData["bdd_dated"];
$bdd_best=$getData["bdd_best"];
$bdd_last=$getData["bdd_last"];
$bdd_difference=$getData["bdd_difference"];
$totalContrib=$getData["totalContrib"];
$contribUnique=$getData["contribUnique"];

$stats=getStats();



$now=new DateTime("now");

$bdd_maj_priorite=[];

$maximum=null;

$table_popularite="";
$bdd_sorted = $bdd_best;
uasort($bdd_sorted, function($a, $b) {
    return $b['compteur'] <=> $a['compteur'];
});
foreach ($bdd_sorted as $s => $donnees) {
    $table_popularite.='<tr><td><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><td class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</th></tr>';
    if($maximum==null){
        $maximum=$donnees["compteur"];
    }
}

$total_a_maj=0;
$table_maj="";
$bdd_sorted = $bdd_last;
uasort($bdd_sorted, function($a, $b) {
    return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
});
$date10h=new DateTime("10:00");
$date10h=$date10h->setTime(10,00,0);
foreach ($bdd_sorted as $s => $donnees) {
    $opacity='';
    $dateDonnees=DateTime::createFromFormat('d/m/y H:i:s', $donnees['date']);
    if($dateDonnees==false || getTotalInterval($dateDonnees->diff($now))>24*60*60 || (getTotalInterval($date10h->diff($now))>0 && getTotalInterval($dateDonnees->diff($date10h))>0)){
        $total_a_maj++;
        if($donnees["compteur"]>=500){
            $bdd_maj_priorite[]=$s;
        }
    }else{
        $opacity=' style="opacity:0.5" ';
    }
    $section=str_replace("_","",$s);
    $table_maj.='<tr><td'.$opacity.'><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$donnees["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><td'.$opacity.'><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$donnees["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$donnees['date'].'</a> <span class="badge">'.$bdd_best[$s]["compteur"].'</span></th></tr>';
}

$preciser=0;
$table_preciser="";
$bdd_sorted = $bdd_best;
uasort($bdd_sorted, function($a, $b) {
    return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
});
foreach ($bdd_sorted as $s => $donnees) {
    if ($donnees['compteur'] > 200 && (!$donnees['is_last'] || $donnees['nb_pages']<$bdd_last[$s]['nb_pages'])) {
        $bdd_maj_priorite[]=$s;
        $preciser++;
        $section=str_replace("_","",$s);
        $table_preciser.='<tr><td><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$section[0].'/'.$section.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><td><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$donnees['date'].' n\'est pas précis</a> <span class="badge">'.$donnees["compteur"].'</span></th></tr>';
    }
}

$bdd_maj_priorite=array_replace($bdd_maj_priorite,array_keys($bdd_difference));


$aggregate=getAggregate();

$ctrip_bdd=$aggregate["compteur.rip"]["bdd"];
$vincib_bdd=$aggregate["adp.sonntag.fr"]["bdd"];
$adprip_bdd=$aggregate["adprip.fr"]["bdd"];
