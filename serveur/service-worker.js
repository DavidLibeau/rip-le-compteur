var CACHE = 'cache-update-and-refresh-v4';

self.addEventListener('install', function (evt) {
    console.log('The service worker is being installed.');
    evt.waitUntil(caches.open(CACHE).then(function (cache) {
        cache.addAll([
            'index',
            './',
            'verifier',
            'historique',
            'liste_communes',
            'statistiques_vote',
            'faq',
            'infos_referendum',
            'mentions_legales',
            'notifications',
            'statistiques_contributions',
        ]);
    }));
});

self.addEventListener('fetch', (event) => {
    if(event.request.url.indexOf(".xpi")==-1){
        event.respondWith(async function () {
            const cache = await caches.open(CACHE);
            const cachedResponse = await cache.match(event.request);
            const networkResponsePromise = fetch(event.request);

            event.waitUntil(
                updateCache(event.request)
                .then(refreshClient)
            );

            return cachedResponse || networkResponsePromise;
        }());
    }
});


function fromCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return cache.match(request);
    });
}

function updateCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return fetch(request).then(function (response) {
            return cache.put(request, response.clone()).then(function () {
                return response;
            });
        });
    });
}

function refreshClient(response) {
    return self.clients.matchAll().then(function (clients) {
        clients.forEach(function (client) {
            response.text().then(function (body) {
                var message = {
                    type: 'refresh',
                    url: response.url,
                    body: body
                };
                client.postMessage(JSON.stringify(message));
            });
        });
    });
}