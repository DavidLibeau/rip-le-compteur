<?php

include("func.php");

$secret="secret";
$clientId=hash("sha256", $secret.$_SERVER["REMOTE_ADDR"]);


function cestincorrect(){
    echo("Données incorrectes ! :( ".$clientId);
    file_put_contents("data/error.txt", date("d\/m\/y H:i:s").",".$clientId."\r", FILE_APPEND);
}


if(isset($_POST["consultationId"]) && isset($_POST["section"]) && isset($_POST["compteur"])){
    if(is_numeric($_POST["consultationId"]) && ((strlen($_POST["section"])==2 && (ctype_alpha($_POST["section"]) || strpos($_POST["section"],"_")) || $_POST["section"]=="single")) && is_numeric($_POST["compteur"])){
        echo("Données reçues ! :) ".$clientId);
        $data=date("d\/m\/y H:i:s").",".$clientId.",".$_POST["consultationId"].",".$_POST["section"].",".$_POST["compteur"];
        if(isset($_POST["nb_pages"]) && isset($_POST["is_last"]) && is_numeric($_POST["nb_pages"]) && is_numeric($_POST["is_last"])){//v1.3
                $data.=",".$_POST["nb_pages"].",".$_POST["is_last"];
        }
        //date,section,compteur,nb_pages,is_last
        $handle = fopen("data/data-2.txt", "ab");
        fwrite($handle, $data."\r\n");
        fclose($handle);
        
        getData("now",true);
    }else{
        cestincorrect();
    }
}else{
    cestincorrect();
}
