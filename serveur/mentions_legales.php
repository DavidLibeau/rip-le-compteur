<?php 
include("preprocess.php");
$title="Mentions légales";
$description="Les mentions légales du compteur collaboratif rip-le-compteur.dav.li.";
$custom_header='<link  href="lib/fotorama-4.6.4/fotorama.css" rel="stylesheet">';
include("head.inc");
?>


<main>
    
    <section class="centered">
        <h2><img src="twemoji/loi.png" alt="emoji loi" /> Mentions légales</h2>
        <p>Mise à jour le 07/08/19</p>
    </section>
    <section>
        <p><i>La forme masculine a été utilisée uniquement pour alléger le texte.</i></p>
        <h3>Avertissement et conditions d'utilisation</h3>
        <p>L'auteur de ce site web, de l'application mobile et de l'extension ne peut être tenu responsable d'une mauvaise utilisation ou d'une utilisation malicieuse des outils ou données délivrés par ce site web et l'application mobile.</p>
        <p>Les données de ce site web et de l'application mobile sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>, <a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank" rel="noopener noreferrer">code source</a>) et sont soumis à leur licence respective.</p>
        <p>Les utilisateurs de l'extension de navigateur sont contributeurs et publient les données qu'ils récoltent sous licence Creative Commons Attribution.</p>
        <p>L'auteur de ce site web, de l'application mobile et de l'extension ne fournit aucune garantie quant à la validité des données fournies par ce site web et l'application mobile.</p>
        <p>En utilisant ce site web ou l'application Android et en téléchargement l'extension pour navigateur, les utilisateurs donnent leur approbation concernant ces conditions d'utilsation et leur consentement à l'utilisation et la transmission des données nécessaires au bon fonctionnement de ce site web, de l'application mobile et de l'extension pour navigateur.</p>
        <p>Ce site web, l'application mobile et l'extension de navigateur peuvent enregistrer les données de connexion de l'utilisateur et notamment son adresse IP. Ce site web utilise des cookies nécessaires à son fonctionnement. Aucune donnée n'est vendue. Certaines données sont en <a href="data" target="_blank">libre accès</a>. L'adresse IP de l'utilisateur est anonymisée (pseudonymisation avec un algorithme sha256 avec ajout d'une chaine de caractères secrete) avant d'être sauvegardée.</p>
        <p>Avant d'utiliser l'extension de navigateur, l'utilisateur doit s'informer de son fonctionnement en consultant le code source en libre accès (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank" rel="noopener noreferrer">à cette adresse</a>) ou en demandant des informations à l'auteur de l'extension, si l'utilisateur a des questions.</p>
        <p>L'utilisateur peut à tout moment exercer ses droits auprès de l'auteur du site web, de l'application mobile et de l'extension de navigateur. L'auteur du site web, de l'application mobile et de l'extension de navigateur aura 30 jours pour répondre et exercer les droits de l'utilisateur.</p>
        <p>Le <a href="/minitel/generate_fake_captcha.php?content=Ceci est un test" target="_blank">générateur de faux captcha</a> propose de générer une image à partir d'un texte fourni par l'utilisateur. L'utilisateur est donc seul responsable des messages qu'il publie.</p>
        <h3>Contact</h3>
        <p>L'auteur de ce site web, de l'application mobile et de l'extension de navigateur peut être contacté <a href="https://davidlibeau.fr/Contact" target="_blank" rel="noopener noreferrer">à cette adresse</a> ou par mail directement à l'adresse suivante : <strong style="display:inline-block">rip-le-compteur(a)davidlibeau.fr</strong>.</p>
        <h3>Hébergeur</h3>
        <p>Le site web et l'application mobile sont hébergés par OVH :<br/>
        2 rue Kellermann - 59100 Roubaix - France
        </p>
        <h3>Services tiers</h3>
        <p>Ce site web ou l'application Android n'utilise pas de services tiers et donc ne transmet pas de données à des tiers.</p>
        <p>Cependant, le service de notification permettant de recevoir des notifications même lorsque le site web ou l'application n'est pas ouverte est obligé d'utiliser des services spéciaux fournis par le navigateur de l'utilisateur. Ainsi, si l'utilisateur utilise le navigateur Google Chrome ou l'application Android, le service de notification utilisera les serveurs de Google. Si l'utilisateur utilise le navigateur Firefox, les serveurs de Mozilla seront utilisés. Ces serveurs recevront les notifications et une adresse interne correspondant à votre navigateur. Le service de notification fourni par ce site web et l'application Android est totalement facultatif et arrêtable sans condition, à tout moment (sauf lorsque le service est indisponible pour des raisons techniques) et en un clic sur le même bouton utilisé lors de la souscription.</p>
    </section>
    <section class="centered">
        <p><a href="/" class="btn"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil</a></p>
    </section>

</main>
<?php include("footer.inc"); ?>
    <script>
        $(function() {
        });
    </script>
</body>

</html>