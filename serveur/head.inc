<?php
if(strpos($_SERVER["REQUEST_URI"],"rip-le-compteur")){
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: https://rip-le-compteur.dav.li".str_replace("/rip-le-compteur","",$_SERVER["REQUEST_URI"]));
}
?>

<?php if(!isset($isXml) && !$isXml){ ?>
<!DOCTYPE html>
<?php } ?>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link rel="manifest" href="/manifest.json" />
    <?php
    $currentUrl=end(explode("/",$_SERVER["REQUEST_URI"]));
    if(is_numeric(strpos($currentUrl,"index"))){
        echo('<link rel="canonical" href="https://rip-le-compteur.dav.li/" />');
    }elseif(is_numeric(strpos($currentUrl,".php"))){
        $canonicalUrl=str_replace(".php","",$currentUrl);
        echo('<link rel="canonical" href="https://rip-le-compteur.dav.li/'.$canonicalUrl.'" />');
    }
    ?>
    <?php if(!empty($title)){ ?>
        <title><?php echo($title); ?></title>
        <meta property="og:title" content="<?php echo($title); ?>" />
    <?php }else{ ?>
        <title>Compteur collaboratif des signatures du référendum ADP et agrégateur/vérificateur de tous les comptages</title>
        <meta property="og:title" content="Compteur collaboratif des signatures du référendum ADP et agrégateur/vérificateur de tous les comptages" />
    <?php } ?>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://rip-le-compteur.dav.li/" />
    <meta property="og:image" content="https://rip-le-compteur.dav.li/compteur-adp-rip-image.png" />
    <?php if(!empty($description)){ ?>
        <meta property="og:description" content="<?php echo($description); ?>" />
        <meta name="description" content="<?php echo($description); ?>" />
    <?php }else{ ?>
        <meta property="og:description" content="Combien de signatures y-a-t-il sur le référendum ADP ? Vérifiez le nombre de signatures sur le compteur collaboratif des signatures pour le référendum d'initiative partagée contre la privatisation des aéroports de Paris. Statistique mise à jour en temps réel, tous les jours, avec les contributions de volontaires et à l'agrégation de tous les comptages." />
        <meta name="description" content="Combien de signatures y-a-t-il sur le référendum ADP ? Vérifiez le nombre de signatures sur le compteur collaboratif des signatures pour le référendum d'initiative partagée contre la privatisation des aéroports de Paris. Statistique mise à jour en temps réel, tous les jours, avec les contributions de volontaires et à l'agrégation de tous les comptages." />
    <?php } ?>
    <meta name="keywords" content="compteur, signatures, référendum, ADP, RIP, aéroport de paris, aéroport, paris, privatisation, nombre de signatures, agrégateur, vérifier, libération, checknews, David Libeau" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@DavidLibeau" />
    <meta name="twitter:creator" content="@DavidLibeau" />
    <?php if(!empty($title)){ ?>
        <meta name="twitter:title" content="<?php echo($title); ?>" />
    <?php }else{ ?>
        <meta name="twitter:title" content="Compteur collaboratif des signatures du référendum ADP" />
    <?php } ?>
    <?php if(!empty($description)){ ?>
        <meta name="twitter:description" content="<?php echo($description); ?>" />
    <?php }else{ ?>
        <meta name="twitter:description" content="Combien de signatures y-a-t-il sur le référendum ADP ? Toutes les données sur https://rip-le-compteur.dav.li/ !" />
    <?php } ?>
    <meta name="twitter:image" content="https://rip-le-compteur.dav.li/compteur-adp-rip-image.png" />
    <meta property="og:locale" content="fr_FR" />
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="theme-color" content="#3c3c3c" />
    <link rel="stylesheet" href="/style.css" />
    <link rel="stylesheet" href="/forkawesome/1.1.7/css/fork-awesome.min.css" />
    <link rel="stylesheet" href="//dav.li/jquery/ui/jquery-ui.min.css" />
    <?php 
    if(isset($custom_header)){
        echo($custom_header);
    }
    ?>
    <meta name="cacheId" content="<?php echo(hash("sha256",$total_digramme.json_encode($stats).json_encode($aggregate["liberation"]).json_encode($ctrip_bdd).json_encode($adprip_bdd))); ?>" />
</head>

<body>
    <div id="toast"></div>
    <header>
        <a href="/notifications" id="openNotifications" class="btn" aria-label="Ouvrir le menu Notification"><i class="fa fa-bell" aria-hidden="true"></i></a>
        <button id="openMenu" class="btn" aria-label="Ouvrir le menu de Navigation"><i class="fa fa-bars" aria-hidden="true"></i></button>
        <h2><a href="/"><img src="twemoji/signature.png" alt="emoji signature" /> RIP, le compteur.</a></h2>
    </header>
    <nav>
        <button id="closeMenu" class="btn" aria-label="Fermer le menu"><i class="fa fa-close" aria-hidden="true"></i></button>
        <ul>
            <li><a href="/" title="Page d'accueil du compteur collaboratif"><img src="twemoji/accueil.png" alt="emoji accueil" class="emoji" /> Accueil</a></li>
            <li><a href="/notifications" title="Service de notification"><img src="twemoji/cloche.png" alt="emoji cloche" class="emoji" /> Notifications <span class="badge warning">beta</span></a></li>
            <li class="nav-categorie">Toutes les données</li>
            <li><a href="/historique" title="L'historique des signatures jour par jour"><img src="twemoji/stats.png" alt="emoji stats" class="emoji" /> Historique du compteur</a></li>
            <li><a href="/liste_communes" title="Les signatures du référendum commune par commune"><img src="twemoji/village.png" alt="emoji village" class="emoji" /> Données par commune</a></li>
            <li><a href="/statistiques_vote" title="Corrélations statistiques avec le vote aux présidentielles de 2017"><img src="twemoji/vote.png" alt="emoji vote" class="emoji" /> Données par vote</a></li>
            <li><a href="/statistiques_aeroport" title="Statistiques par aéroport"><img src="twemoji/avion_atterrissage.png" alt="emoji avion atterrissage" class="emoji" /> Données par aéroport <span class="badge warning">beta</span></a></li>
            <li class="nav-categorie">Contribuer</li>
            <li><a href="/contribuer" title="Le tableau de bord des contributeurs/contributrices"><img src="twemoji/poing.png" alt="emoji poing" class="emoji" /> Aider à compter</a></li>
            <li><a href="/verifier" title="Le vérificateur de tous les autres comptages"><img src="twemoji/verification.png" alt="emoji verification" class="emoji" /> Verifier</a></li>
            <li><a href="/comptage_perso" title="Comptage décentralisé"><img src="twemoji/dossiers.png" alt="emoji dossiers" class="emoji" /> Comptage décentralisé</a></li>
            <li class="nav-categorie">Plus d'informations...</li>
            <li><a href="/infos_referendum" title="Informations sur le référendum ADP"><img src="twemoji/signer.png" alt="emoji signer" class="emoji" /> Qu'est-ce que le RIP ?</a></li>
            <li><a href="/faq" title="Informations sur ce site web"><img src="twemoji/ampoule.png" alt="emoji ampoule" class="emoji" /> À propos de ce site web</a></li>
            <li><a href="/statistiques_contributions" title="Les statistiques de contributions"><img src="twemoji/contributions.png" alt="emoji stats contributions" class="emoji" /> Statistiques des contributions</a></li>
            <li><a href="/mentions_legales" title="Les conditions d'utilisation, etc."><img src="twemoji/loi.png" alt="emoji loi" class="emoji" /> Mentions légales</a></li>
        </ul>
    </nav>