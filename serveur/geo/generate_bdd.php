<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/


include("func.php");

$communes_nouvelles=[];
$line=0;
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("communes_nouvelles.csv",'r');
while ( ($data = fgetcsv($handle, 0)) !== FALSE ) {
    $line++;
    if($line!=1){
       $communes_nouvelles[$data[3]]=$data[1];
    }

}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);

//var_dump($communes_nouvelles);

$bdd_communes_reverse_normalise=[];
$bdd_communes_euro=[];
$bdd_insee_euro=[];
$line=0;
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("communes_europeennes_2019.csv",'r');
while ( ($data = fgetcsv($handle, 0)) !== FALSE ) {
    $line++;
    if($line!=1 && $data[0][1]!="Z"){
        $bdd_communes_reverse_normalise[normaliser($data[3])]=$data[3];
        
        if(!isset($bdd_communes_euro[normaliser($data[3])])){
            $bdd_communes_euro[normaliser($data[3])]=[[$data[0].$data[2],$data[4]]];
        }else{
            array_push($bdd_communes_euro[normaliser($data[3])],[$data[0].$data[2],$data[4]]);
        }
        
        if(!isset($bdd_insee_euro[$data[0].$data[2]])){
            $bdd_insee_euro[$data[0].$data[2]]=$data[3];
        }else{
            echo("Error : ".$data[0].$data[2]." / ".$data[3]);
        }
    }

}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);


$bdd_communes_insee=[];
$bdd_insee_insee=[];
$line=0;
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("communes_INSEE.csv",'r'); // source : https://statistiques-locales.insee.fr 14/04/2019
while ( ($data = fgetcsv($handle, 0, ";")) !== FALSE ) {
    $line++;
    if($line>3 && $data[0][1]!="Z"){
        $bdd_communes_reverse_normalise[normaliser($data[1])]=$data[1];

        if(!isset($bdd_communes_insee[normaliser($data[1])])){
            $bdd_communes_insee[normaliser($data[1])]=[[$data[0],$data[2]]];
        }else{
            array_push($bdd_communes_insee[normaliser($data[1])],[$data[0],$data[2]]);
        }
        
        if(!isset($bdd_insee_insee[$data[0]])){
            $bdd_insee_insee[$data[0]]=$data[1];
        }else{
            echo("Error : ".$data[0]." / ".$data[2]);
        }
    }

}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);

$erreurs=[];
foreach($bdd_insee_euro as $code_insee => $nom_commune){
    if(empty($bdd_insee_insee[$code_insee])){
        echo("Erreur euro : ".$code_insee." not found in insee bdd.\n");
    }else{
        if(normaliser($nom_commune)!=normaliser($bdd_insee_insee[$code_insee])){
            echo("Erreur euro : ".$code_insee." ".normaliser($nom_commune)." != ".normaliser($bdd_insee_insee[$code_insee])."\n");
            $erreurs[]=$code_insee;
        }
    }
}

foreach($bdd_insee_insee as $code_insee => $nom_commune){
    if(empty($bdd_insee_euro[$code_insee])){
        echo("Erreur insee : ".$code_insee." not found in euro bdd.\n");
    }else{
        if(normaliser($nom_commune)!=normaliser($bdd_insee_euro[$code_insee])){
            if(!in_array($code_insee,$erreurs)){
                echo("Erreur insee : ".$code_insee." ".normaliser($nom_commune)." != ".normaliser($bdd_insee_euro[$code_insee])."\n");
            }
        }
    }
}

//communes_presidentielles_2017.csv
$indexPres=["CodeInsee","CodeDepartement","Département","Commune","Inscrits","Abstentions","Abstentions_ins","Votants","Votants_ins","Blancs","Blancs_ins","Blancs_vot","Nuls","Nuls_ins","Nuls_vot","Exprimés","Exprimés_ins","Exprimés_vot","LE PEN","MÉLENCHON","MACRON","FILLON","DUPONT-AIGNAN","LASSALLE","HAMON","ASSELINEAU","POUTOU","ARTHAUD","CHEMINADE","LE PEN.ins","MÉLENCHON.ins","MACRON.ins","FILLON.ins","DUPONT-AIGNAN.ins","LASSALLE.ins","HAMON.ins","ASSELINEAU.ins","POUTOU.ins","ARTHAUD.ins","CHEMINADE.ins","LE PEN.exp","MÉLENCHON.exp","MACRON.exp","FILLON.exp","DUPONT-AIGNAN.exp","LASSALLE.exp","HAMON.exp","ASSELINEAU.exp","POUTOU.exp","ARTHAUD.exp","CHEMINADE.exp"];

$bdd_communes_pres=[];
$line=0;
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("communes_presidentielles_2017.csv",'r'); //from https://github.com/datactivist/presidentielle2017
while ( ($data = fgetcsv($handle, 0)) !== FALSE ) {
    $line++;
    if($line!=1 && $data[0][1]!="Z"){
        $nom_commune=$data[array_search("Commune",$indexPres)];
        $code_insee=$data[array_search("CodeInsee",$indexPres)];
        $voixGauche=intval($data[array_search("HAMON",$indexPres)]+$data[array_search("POUTOU",$indexPres)]+$data[array_search("ARTHAUD",$indexPres)]+$data[array_search("MÉLENCHON",$indexPres)]);
        $voixCentre=intval($data[array_search("MACRON",$indexPres)]);
        $voixDroite=intval($data[array_search("FILLON",$indexPres)]);
        $voixExtDroite=intval($data[array_search("LE PEN",$indexPres)]+$data[array_search("DUPONT-AIGNAN",$indexPres)]);
        $voixAutres=intval($data[array_search("LASSALLE",$indexPres)]+$data[array_search("CHEMINADE",$indexPres)]+$data[array_search("ASSELINEAU",$indexPres)]);
        $abstentionnistes=intval($data[array_search("Abstentions",$indexPres)]);
        $inscrits=intval($data[array_search("Inscrits",$indexPres)]);
        $communejson=[$code_insee,$nom_commune,$inscrits,$abstentionnistes,$voixGauche,$voixCentre,$voixDroite,$voixExtDroite,$voixAutres];
        if(!isset($bdd_communes_pres[normaliser($nom_commune)])){
            $bdd_communes_pres[normaliser($nom_commune)]=[$communejson];
        }else{
            array_push($bdd_communes_pres[normaliser($nom_commune)],$communejson);
        }
    }

}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);

$bdd_communes_pres_pourcentages=[];
foreach($bdd_communes_pres as $villenormalise => $commune){
    if(sizeof($commune)>1){
        $codes_insee=array_column($commune,0);
        $inscrits=array_sum(array_column($commune,2));
        $pourcentage_abstentions=array_sum(array_column($commune,3))/$inscrits*100;
        $pourcentage_gauche=array_sum(array_column($commune,4))/$inscrits*100;
        $pourcentage_centre=array_sum(array_column($commune,5))/$inscrits*100;
        $pourcentage_droite=array_sum(array_column($commune,6))/$inscrits*100;
        $pourcentage_extDroite=array_sum(array_column($commune,7))/$inscrits*100;
        $pourcentage_autres=array_sum(array_column($commune,8))/$inscrits*100;
    }else{
        $codes_insee=$commune[0][0];
        $inscrits=$commune[0][2];
        $pourcentage_abstentions=$commune[0][3]/$inscrits*100;
        $pourcentage_gauche=$commune[0][4]/$inscrits*100;
        $pourcentage_centre=$commune[0][5]/$inscrits*100;
        $pourcentage_droite=$commune[0][6]/$inscrits*100;
        $pourcentage_extDroite=$commune[0][7]/$inscrits*100;
        $pourcentage_autres=$commune[0][8]/$inscrits*100;
    }
    $nom_commune=$commune[0][1];
    $communejson=[$codes_insee,$nom_commune,$inscrits,$pourcentage_abstentions,$pourcentage_gauche,$pourcentage_centre,$pourcentage_droite,$pourcentage_extDroite,$pourcentage_autres];
    $bdd_communes_pres_pourcentages[$villenormalise]=$communejson;
}


file_put_contents("bdd_communes_euro.json",json_encode($bdd_communes_euro));
file_put_contents("bdd_communes_insee.json",json_encode($bdd_communes_insee));
file_put_contents("bdd_insee_euro.json",json_encode($bdd_insee_euro));
file_put_contents("bdd_insee_insee.json",json_encode($bdd_insee_insee));

file_put_contents("bdd_communes_pres.json",json_encode($bdd_communes_pres));
file_put_contents("bdd_communes_pres_pourcentages.json",json_encode($bdd_communes_pres_pourcentages));

file_put_contents("bdd_communes_reverse_normaliser.json",json_encode($bdd_communes_reverse_normalise));

//header('Content-Type: application/json');
//echo(json_encode($bdd_communes_insee));

echo("Done.");