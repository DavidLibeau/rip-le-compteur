<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include("func.php");


$bdd_error=[];

foreach($compteurrip_bdd as $ville => $signatures){
    $recherche=searchInDb($ville);

    if(empty($recherche["inscrits"])){
        if(!empty($communes_nouvelles[$ville])){
            $recherche=searchInDb($communes_nouvelles[$ville]);
            if(empty($recherche["inscrits"])){
                if(isset($bdd_communes_reverse_normaliser[$communes_nouvelles[$ville]])){
                    echo("Error: ".$ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");

                }else{
                    echo($ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");
                }
            }
        }else{
            if(isset($bdd_communes_reverse_normaliser[$ville])){
                echo("Error: ".$ville."\n");
            }else{
                $bdd_error[$ville]="compteurrip";
            }
        }
    }
}

foreach($vincib_bdd as $ville => $signatures){
    $recherche=searchInDb($ville);

    if(empty($recherche["inscrits"])){
        if(!empty($communes_nouvelles[$ville])){
            $recherche=searchInDb($communes_nouvelles[$ville]);
            if(empty($recherche["inscrits"])){
                if(isset($bdd_communes_reverse_normaliser[$communes_nouvelles[$ville]])){
                    echo("Error: ".$ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");

                }else{
                    echo($ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");
                }
            }
        }else{
            if(isset($bdd_communes_reverse_normaliser[$ville])){
                echo("Error: ".$ville."\n");
            }else{
                if(isset($bdd_error[$ville])){
                    $bdd_error[$ville].="+vincib";
                }else{
                    $bdd_error[$ville]="vincib";
                }
            }
        }
    }
}

foreach($adpripfr_bdd as $ville => $signatures){
    $recherche=searchInDb($ville);

    if(empty($recherche["inscrits"])){
        if(!empty($communes_nouvelles[$ville])){
            $recherche=searchInDb($communes_nouvelles[$ville]);
            if(empty($recherche["inscrits"])){
                if(isset($bdd_communes_reverse_normaliser[$communes_nouvelles[$ville]])){
                    echo("Error: ".$ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");

                }else{
                    echo($ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");
                }
            }
        }else{
            if(isset($bdd_communes_reverse_normaliser[$ville])){
                echo("Error: ".$ville."\n");
            }else{
                if(isset($bdd_error[$ville])){
                    $bdd_error[$ville].="+adpripfr";
                }else{
                    $bdd_error[$ville]="adpripfr";
                }
            }
        }
    }
}

//var_dump($bdd_error);


$nominatim_communes=json_decode(file_get_contents("nominatim_communes.json"),true);
$nominatim_insee_error=json_decode(file_get_contents("nominatim_insee_error.json"),true);

$nominatim_resolved_count=0;
$nominatim_insee_resolved_count=0;


/*foreach($nominatim_insee_error as $insee => $nom){
    //echo($nom." :");
    if($insee!="undefined"){
        if(isset($insee_to_commune_euro[$insee])){
            $nominatim_insee_resolved_count++;
            //echo($insee_to_commune_euro[$insee][0]);
            $bdd_communes_nominatim[$nom]=$insee_to_commune_euro[$insee];
        }elseif(isset($insee_to_commune_insee[$insee])){
            $nominatim_insee_resolved_count++;
            //echo($insee_to_commune_insee[$insee][0]);
            $bdd_communes_nominatim[$nom]=$insee_to_commune_insee[$insee];
        }
    }
    //echo("<br/>");
}*/

$nominatim_communes_normalise=[];
foreach($nominatim_communes as $ville => $ville_alt){
    $nominatim_communes_normalise[normaliser($ville)]=$ville_alt;
}

$table_to_check="";
$table_to_check_total=0;


foreach($bdd_error as $ville => $source){
    if(isset($bdd_communes_euro[normaliser($nominatim_communes_normalise[normaliser($ville)])])){
        $bdd_communes_nominatim[$ville]=$nominatim_communes_normalise[normaliser($ville)];
        $nominatim_resolved_count++;
    }elseif(isset($bdd_communes_insee[normaliser($nominatim_communes_normalise[normaliser($ville)])])){
        $bdd_communes_nominatim[$ville]=$nominatim_communes_normalise[normaliser($ville)];
        $nominatim_resolved_count++;
    }else{
        if(isset($nominatim_communes[normaliser($ville)])){
            $table_to_check.="<tr><td>$ville</td><td>".normaliser($ville)."</td><td></td><td>(Non trouvé dans les bdd) </td></tr>";
        }else{
            $table_to_check.="<tr><td>$ville</td><td>".normaliser($ville)."</td><td></td><td></td></tr>";
        }
        $table_to_check_total++;   
    }
}

file_put_contents("bdd_communes_nominatim.json",json_encode($bdd_communes_nominatim));

    
    /*if(isset($bdd_communes_euro[$nominatim_communes[$ville]])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_euro[$nominatim_communes[$ville]];
        $nominatim_resolved_count++;
    }elseif(isset($bdd_communes_insee[$nominatim_communes[$ville]])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_insee[$nominatim_communes[$ville]];
        $nominatim_resolved_count++;
    }elseif(isset($bdd_communes_euro[strtr($nominatim_communes[$ville], $withoutDash)])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_euro[strtr($nominatim_communes[$ville], $similarChars)];
        $nominatim_resolved_count++;
    }elseif(isset($bdd_communes_insee[strtr($nominatim_communes[$ville], $withoutDash)])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_insee[strtr($nominatim_communes[$ville], $similarChars)];
        $nominatim_resolved_count++;
    }if(isset($bdd_communes_euro[strtr($nominatim_communes[$ville], $withoutDash)])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_euro[strtr($nominatim_communes[$ville], $withoutDash)];
        $nominatim_resolved_count++;
    }elseif(isset($bdd_communes_insee[strtr($nominatim_communes[$ville], $withoutDash)])){
        $bdd_communes_nominatim[$ville]=$bdd_communes_insee[strtr($nominatim_communes[$ville], $withoutDash)];
        $nominatim_resolved_count++;
    }*/
    
    
    /*$ville=strtr($ville, $normalizeChars);
    //$ville_similar=strtr($ville, $similarChars);
    //$ville_withoutDash=strtr($ville, $withoutDash);
    if(isset($bdd_communes_euro[$ville])){
        
    }elseif(isset($bdd_communes_euro[$communes_nouvelles[$ville]])){
        //found
    }elseif(isset($bdd_communes_euro[$ville_similar])){
        
    }elseif(isset($bdd_communes_euro[$communes_nouvelles[$ville_similar]])){
        //found
    }elseif(isset($bdd_communes_euro[$ville_withoutDash])){
        
    }elseif(isset($bdd_communes_euro[$communes_nouvelles[$ville_withoutDash]])){
        //found
    }elseif(isset($bdd_communes_insee[$ville])){
        
    }elseif(isset($bdd_communes_insee[$communes_nouvelles[$ville]])){
        //found
    }elseif(isset($bdd_communes_insee[$ville_similar])){
        
    }elseif(isset($bdd_communes_insee[$communes_nouvelles[$ville_similar]])){
        //found
    }elseif(isset($bdd_communes_insee[$ville_withoutDash])){
        
    }elseif(isset($bdd_communes_insee[$communes_nouvelles[$ville_withoutDash]])){
        //found
    }elseif(isset($bdd_communes_nominatim[$ville])){
        
    }else{
        if($ville!="" && !strpos($ville,"Arrondissement")){
            if(isset($nominatim_communes[$ville])){
                $table_to_check.="<tr><td>$ville</td><td></td><td>(Non trouvé dans les bdd) </td></tr>";
            }else{
                $table_to_check.="<tr><td>$ville</td><td></td><td></td></tr>";
            }
            $table_to_check_total++;
        }
    }*/


echo($nominatim_resolved_count." / ".sizeof($nominatim_communes));
echo("<br/>");
echo($nominatim_insee_resolved_count." / ".sizeof($nominatim_insee_error));

?>


<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Verifier geo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#0070C0">
    </head>
    <body>
        <div id="download"></div>
        <div>
            <button id="checkNominatim">Lancer la vérification Nominatim</button>
        </div>
        <table>
            <thead>
                <tr>
                    <th>Nom de la commune (<?php echo($table_to_check_total); ?>)</th>
                    <th>Nom normalisé</th>
                    <th>Nom trouvé</th>
                    <th>Autres infos trouvées</th>
                </tr>
            </thead>
            <tbody>
                <?php echo($table_to_check); ?>
            </tbody>
        </table>

        <script src="//dav.li/jquery/3.1.1.min.js"></script>
        <script>
        var bdd_found={};
        var bdd_error={};
        
        var v=1;
        function checkNominatim(){
            //var nom_ville=villes[v].replace("\u009","oe");
            var $tr_ville=$("tbody tr:nth-child("+v+")");
            var nom_ville=$tr_ville.children("td:nth-child(1)").text();
            var url = "https://nominatim.openstreetmap.org/search.php?q="+nom_ville+"&addressdetails=1&accept-language=fr&format=json";
            console.log(url);
            $.ajax({
                url: url,
                dataType: "json"
            }).done(function(nominatim) {
                console.log(nominatim);
                if(nominatim[0]){
                    var alt_nom_ville=undefined;
                    var rang_nominatim;
                    for(var n=0;n<nominatim.length;n++){
                        if(n>4){
                            break;
                        }
                        if(nominatim[n]["address"]["country"]=="France" && (nominatim[n]["type"]=="administrative" || nominatim[n]["type"]=="village")){
                            if(!alt_nom_ville && nominatim[n]["address"]["city"]!=nom_ville){
                                alt_nom_ville=nominatim[n]["address"]["city"];
                                rang_nominatim=n;
                            }
                            if(!alt_nom_ville && nominatim[n]["address"]["village"]!=nom_ville){
                                alt_nom_ville=nominatim[n]["address"]["village"];
                                rang_nominatim=n;
                            }
                        }
                    }
                    if(!alt_nom_ville){
                        $tr_ville.children("td:nth-child(3)").append(nominatim[0]["display_name"]+" ");
                        //if(nominatim[0]["address"]["country"]=="France"){
                            var url="https://www.openstreetmap.org/api/0.6/relation/"+nominatim[0]["osm_id"];
                            console.log(url);
                            $.ajax({
                                url: url,
                                dataType: "xml"
                            }).done(function(osm_xml) {
                                $osm_xml=$(osm_xml);
                                alt_nom_ville=$osm_xml.find('tag[k="alt_name"]').attr("v");
                                if($osm_xml.find('tag[k="alt_name"]').length){
                                    $tr_ville.children("td:nth-child(2)").html(alt_nom_ville);
                                    bdd_found[nom_ville]=alt_nom_ville;
                                }else{
                                    var code_insee=$osm_xml.find('tag[k="ref:INSEE"]').attr("v");
                                    var wikipedia_link=$osm_xml.find('tag[k="wikipedia"]').attr("v");
                                    if(code_insee==undefined || bdd_error[code_insee]==undefined){
                                        $tr_ville.children("td:nth-child(3)").html("<i>Introuvable (de différent)</i>");
                                        bdd_error[code_insee]=nom_ville;
                                    }else{
                                        $tr_ville.children("td:nth-child(3)").html("<i>Introuvable (de différent) & code INSEE similaire !</i>");
                                    }
                                    $tr_ville.children("td:nth-child(4)").append(code_insee+' <a href="https://fr.wikipedia.org/wiki/'+wikipedia_link+'" target="_blank">Page Wikipedia</a>');
                                }
                            });
                        /*}else{
                            $tr_ville.children("td:nth-child(2)").html("<i>Introuvable (rien en France)</i>");
                            //bdd_error.push([nom_ville]);
                        }*/
                    }else{
                        $tr_ville.children("td:nth-child(3)").html(alt_nom_ville);
                        if(rang_nominatim<=5){
                            bdd_found[nom_ville]=alt_nom_ville;
                        }else{
                            $tr_ville.children("td:nth-child(3)").append("(A vérifier) rang nominatim : "+rang_nominatim);
                        }
                    }
                }else{
                    $tr_ville.children("td:nth-child(3)").append("<i>Introuvable (OSM : not found)</i>");
                    //bdd_error.push([nom_ville]);
                }
                v++;
                if($("tbody tr:nth-child("+v+")").length){
                    setTimeout(checkNominatim, 1000);
                }else{
                    console.log("Done");
                    bdd_found = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(bdd_found));
                    $('<a href="data:' + bdd_found + '" download="nominatim_communes.json">Download bdd_found</a><br/>').appendTo("#download");
                    bdd_error = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(bdd_error));
                    $('<a href="data:' + bdd_error + '" download="nominatim_insee_error.json">Download bdd_error</a><br/>').appendTo("#download");
                }
            });
        }
        $("#checkNominatim").click(function(){
            $(this).parent().remove();
            checkNominatim();
        });
        
        </script>
    </body>
</html>
