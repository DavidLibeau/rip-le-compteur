<?php

include("func.php");

if(!empty($_GET["format"]) && $_GET["format"]=="json"){
    header('Content-Type: application/json');
    if(!empty($_GET["q"])){
        echo(json_encode(searchInDb($_GET["q"])));
    }else{
        echo(json_encode(["error"=>"Query ?q= not provided"]));
    }
}else{
    if(!empty($_GET["q"])){
        $recherche=searchInDb($_GET["q"]);
        if(isset($recherche["code_insee_to_commune"])){
            ?>
            <h2>Code insee transformé en nom de commune :</h2>
            <ul>
            <?php
                foreach($recherche["code_insee_to_commune"] as $source => $data){
                    echo("<li><h3>Source : $source</h3><p>$data</p></li>");
                }
            ?>
            </ul>
            <?php
        }
        
        echo("<p>");
        if(isset($recherche["nom_commune"])){
            echo('<span class="chip"><span>Nom de la commune</span><strong>'.$recherche["nom_commune"]."</strong></span>");
        }
        if(isset($recherche["nom_commune_custom"])){
            echo('<span class="chip"><span>Nom de la commune rectifié (erroné sur le site du ministère)</span><strong>'.$recherche["nom_commune_custom"]."</strong></span>");
        }
        if(isset($recherche["nom_commune_nouvelle"])){
            echo('<span class="chip"><span>Nom de la commune nouvelle</span><strong>'.$recherche["nom_commune_nouvelle"]."</strong></span>");
        }
        if(isset($recherche["nom_communes_deleguees"])){
            if($recherche["nom_communes_deleguees"][0]!=$recherche["nom_commune_nouvelle"]){
                echo('<span class="chip"><span>Nom de la commune nouvelle tel qu\'affiché sur le site du ministère</span><strong>'.$recherche["nom_communes_deleguees"][0]."</strong></span>");
            }
            $i=1;
            foreach($recherche["nom_communes_deleguees"] as $communes_deleguee){
                echo('<span class="chip"><span>Commune déléguée '.$i.'</span><strong>'.$communes_deleguee."</strong></span>");
                $i++;
            }
        }
        if(isset($recherche["nom_commune_nominatim"])){
            echo('<div class="chip"><span>Nom de la commune (via API Nominatim)</span><strong>'.$recherche["nom_commune_nominatim"]."</strong></div>");
        }
        echo("</p>");
        
        if(isset($recherche["inscrits"])){
            ?>
            <h2>Inscrits :</h2>
            <ul>
            <?php
                foreach($recherche["inscrits"] as $source => $data){
                    echo("<li><h3>Source : $source</h3>");
                    echo("<ul>");
                    foreach($data as $homonyme){
                        echo("<li><h4>Code INSEE : ".$homonyme["code_insee"]."</h4><p>".$homonyme["inscrits"]."</p></li>");
                    }
                    echo("</ul>");
                    echo("</li>");
                }
            ?>
            </ul>
            <?php
            if(isset($recherche["signatures"])){
                ?>
                <h2>Signatures :</h2>
                <ul>
                <?php
                    foreach($recherche["signatures"] as $source => $data){
                        echo("<li><h3>Source : $source</h3><p>$data</p><p class=\"petit\">Données du ".$recherche["infos_sources"][$source]["date"]." sous licence ".$recherche["infos_sources"][$source]["licence"].".</p></li>");
                    }
                ?>
                </ul>
                <?php
            }
        }
    }else{
        if(isset($_GET["q"])){
            echo('<strong>Erreur</strong> La requête est vide.');
        }else{
            echo('Ajoutez un paramètre "?q=". Et pour avoir le résultat en format json, ajoutez "&format=json".');
        }
    }
}
