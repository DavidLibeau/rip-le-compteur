<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include("func.php");

$osm_data=json_decode(file_get_contents("osm_data.json"), true);

$output_features=[];
$stats_communes_signatures=0;
$stats_communes=0;
$communes_0=[];
$communes_insee=[];
$communes_nouvelles=[];

foreach($osm_data["features"] as $feature){
    $this_feature=[];
    $this_feature["type"]=$feature["type"];
    $this_feature["geometry"]=$feature["geometry"];
    $this_feature["id"]=$feature["id"];
    $this_feature["properties"]["code_insee"]=$feature["properties"]["ref:INSEE"];
    $this_feature["properties"]["nom"]=$feature["properties"]["name"];
    $recherche=searchInDb($feature["properties"]["name"]);
    if($recherche["signatures"]["compteur.rip"]){
        $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
        if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
            $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
        }else{
            $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
        }
        $stats_communes_signatures++;
    }else{
        if($recherche["nom_commune_nouvelle"]){
            $recherche=searchInDb($recherche["nom_commune_nouvelle"]);
            $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
            if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
                $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
            }else{
                $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
            }
            $stats_communes_signatures++;
        }else{
            $recherche=searchInDb($feature["properties"]["ref:INSEE"]);
            if($recherche["signatures"]["compteur.rip"]){
                $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
                if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
                    $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
                }else{
                    $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
                }
                $stats_communes_signatures++;
            }else{
                $communes_0[]=[$feature["properties"]["ref:INSEE"],$feature["properties"]["name"]];
            }
        }
    }
    $stats_communes++;
    $output_features[]=$this_feature;
}


$output=[
    "type"=>"FeatureCollection",
    "copyright"=>"Geoloc data : ODbL www.openstreetmap.org. Signatures : compteur.rip",
];

if(!empty($_GET["display"]) && $_GET["display"]=="full"){
    $output["stats"]=$stats_communes_signatures."/".$stats_communes." : ".($stats_communes-$stats_communes_signatures);
    $output["communes_0"]=$communes_0;
}

$output["features"]=$output_features;

header('Content-Type: application/json');
echo(json_encode($output));


file_put_contents("geo_data.json",json_encode($output));


//echo("Done.");