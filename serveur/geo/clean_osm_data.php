<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include("func.php");

$osm_data=json_decode(file_get_contents("osm_export.geojson"), true);

$output_features=[];

foreach($osm_data["features"] as $feature){
    $this_feature=[];
    $this_feature["type"]=$feature["type"];
    $this_feature["properties"]=$feature["properties"];
    $this_feature["geometry"]=$feature["geometry"];
    $this_feature["id"]=$feature["id"];
    $this_feature["properties"]["name"]=str_replace("Mairie d'","",str_replace("Maire de ","",$feature["properties"]["name"]));
    unset($this_feature["properties"]["@changeset"]);
    unset($this_feature["properties"]["@user"]);
    unset($this_feature["properties"]["@uid"]);
    $output_features[]=$this_feature;
}


$output=[
    "type"=>"FeatureCollection",
    "copyright"=>"Data : ODbL www.openstreetmap.org. Exported by David Libeau.",
    "features"=>$output_features
];


header('Content-Type: application/json');
echo(json_encode($output));
