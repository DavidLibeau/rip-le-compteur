<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>Carte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="stylesheet" href="leaflet/leaflet.css" />
    <style>
        #map {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
        }
    </style>
</head>

<body>
    <div id="map"></div>
    <script src="//dav.li/jquery/3.4.1.min.js"></script>
    <script src="leaflet/leaflet.js"></script>
    <script src="leaflet/rbush.min.js"></script>
    <script src="leaflet/leaflet.canvas-markers.js"></script>
    <script>
        layerFactory(L);
        var map = L.map('map').setView([46.495, 2.207], 6);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'GeoLoc : ODbL <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Signatures : Compteur.RIP'
        }).addTo(map);

        var canvasLayer = L.canvasIconLayer({}).addTo(map);
        var markers = [];

        var icon = L.icon({
            iconUrl: 'leaflet/point.png',
            iconSize: [10, 10],
            iconAnchor: [5, 5]
        });

        $.ajax({
            url: "geo_data.json",
            dataType: "json"
        }).done(function(data) {
            console.log("GeoJson loaded");
            L.geoJSON(data, {
                style: function(feature) {
                    return {
                        color: feature.properties.color
                    };
                },
                onEachFeature: function(feature, layer) {
                    if (feature.geometry.type == "Point") {
                        markers.push(L.marker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
                            icon: icon
                        }).bindPopup(feature.properties.nom + " : " + feature.properties.signatures + " signatures (" + feature.properties.pourcentage + " %)"));
                    }
                }
            });
            canvasLayer.addLayers(markers);
        });
    </script>
</body>

</html>