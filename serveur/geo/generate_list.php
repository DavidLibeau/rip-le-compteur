<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include("func.php");

$source="compteur.rip";


function nanIfEmpty($data){
    if(empty($data)){
        return "NaN";
    }else{
        return $data;
    }
}

function normaliser_lite($str){
    if(strpos($str,"(")){
        $prefix = substr($str,strpos($str,"(")+1,strpos($str,")")-strpos($str,"(")-1);
        $str = $prefix.str_replace(substr($str,strpos($str,"("),strpos($str,")")-strpos($str,"(")+1),"",$str);
    }
    $str=trim($str);
    $espaces = array("\r\n", "\n", "\r", "\t");
    $str=str_replace($espaces,"",$str);
    $normaliserStrtr=array(
        ''=>'oe', ''=>'oe', '_'=>'', "’"=>"'", '<'=>'', '>'=>'', '&'=>'',
    );
    $str=strtr($str, $normaliserStrtr);
    return $str;
}


function addToList($nom_commune, $recherche){
    global $bdd_list;
    global $bdd_list_csv;
    global $source;
    
    $nom_commune_normalise=normaliser($nom_commune);
    if(!isset($bdd_list[$nom_commune_normalise])){
        $bdd_list[$nom_commune_normalise]=$recherche;
        
        $nom_commune_merged[normaliser_lite($nom_commune)]=1;
        if(isset($recherche["nom_commune_nouvelle"])){
            $nom_commune_merged[$recherche["nom_commune_nouvelle"]]=1;
        }
        if(isset($recherche["nom_commune_custom"])){
            $nom_commune_merged[$recherche["nom_commune_custom"]]=1;
        }
        if(isset($recherche["nom_commune_nominatim"])){
            $nom_commune_merged[$recherche["nom_commune_nominatim"]]=1;
        }
        if(isset($recherche["nom_communes_deleguees"])){
            foreach($recherche["nom_communes_deleguees"] as $nom_commune_deleguee){
                $nom_commune_merged[$nom_commune_deleguee]=1;
            }
        }
        $nom_commune_merged_txt="";
        $i=0;
        foreach($nom_commune_merged as $nom => $data){
            if($i!=0){
                $nom_commune_merged_txt.=" / ";
            }
            $nom_commune_merged_txt.=$nom;
            $i++;
        }
        
        if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
            $inscrits=$recherche["total_inscrits"]["insee"];
            $pourcentage=$recherche["pourcentages"]["insee"][$source];
        }else{
            $inscrits=$recherche["total_inscrits"]["euro2019"];
            $pourcentage=$recherche["pourcentages"]["euro2019"][$source];
        }
        
        $sautDeLigne="";
        if($bdd_list_csv!=""){
            $sautDeLigne="\n";
        }
        $bdd_list_csv.=$sautDeLigne.$nom_commune_merged_txt.",".$recherche["signatures"][$source].",".$pourcentage.",".$inscrits.",".$recherche["homonymes_count"];
    }
}

$bdd_error=[];
$bdd_list=[];
$bdd_list_csv="";

/*foreach($compteurrip_bdd as $ville => $signatures){
    if(normaliser($ville)!=""){
        if(!strpos($ville,"Arrondissement")){
            $recherche=searchInDb($ville, false);

            if(empty($recherche["inscrits"])){
                /*if(!empty($communes_nouvelles[$ville])){
                    $recherche=searchInDb($communes_nouvelles[$ville]);
                    if(empty($recherche["inscrits"])){
                        if(isset($bdd_communes_reverse_normaliser[$communes_nouvelles[$ville]])){
                            echo("Error: ".$ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");

                        }else{
                            echo($ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");
                        }
                    }else{
                        addToList()
                    }
                }else{
                    if(isset($bdd_communes_reverse_normaliser[$ville])){
                        echo("Error: ".$ville."\n");
                    }else{
                        $bdd_error[$ville]="compteurrip";
                    }
                }
            }else{
                addToList($ville,$recherche);
            }
        }
    }
}*/

foreach($compteurrip_bdd as $ville => $signatures){
    if(normaliser($ville)!=""){
        if(strpos($ville,"Arrondissement")){
            $ville=explode(" ",$ville)[0];
        }
        $recherche=searchInDb($ville, false);
        addToList($ville,$recherche);
    }
}

/*foreach($vincib_bdd as $ville => $signatures){
    if(normaliser($ville)!=""){
        if(!strpos($ville,"Arrondissement")){
            $recherche=searchInDb($ville, false);

            if(empty($recherche["inscrits"])){
                /*if(!empty($communes_nouvelles[$ville])){
                    $recherche=searchInDb($communes_nouvelles[$ville]);
                    if(empty($recherche["inscrits"])){
                        if(isset($bdd_communes_reverse_normaliser[$communes_nouvelles[$ville]])){
                            echo("Error: ".$ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");

                        }else{
                            echo($ville." : ".$communes_nouvelles[$ville]." : ".normaliser($communes_nouvelles[$ville])."\n");
                        }
                    }
                }else{
                    if(isset($bdd_communes_reverse_normaliser[$ville])){
                        echo("Error: ".$ville."\n");
                    }else{
                        if(isset($bdd_error[$ville])){
                            $bdd_error[$ville]="compteurrip+vincib";
                        }else{
                            $bdd_error[$ville]="vincib";
                        }
                    }
                }
            }else{
                addToList($ville,$recherche);
            }
        }
    }
}*/

file_put_contents("bdd_list.json",json_encode($bdd_list));
file_put_contents("bdd_list.csv",$bdd_list_csv);

echo("Done.");

?>
