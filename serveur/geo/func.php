<?php

//Bdd statiques

$communes_nouvelles=[];
$line=0;
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen("communes_nouvelles.csv",'r');
while ( ($data = fgetcsv($handle, 0)) !== FALSE ) {
    $line++;
    if($line!=1){
        if(!isset($communes_nouvelles[$data[3]])){
            $communes_nouvelles[$data[3]]=$data[1]; 
        }
    }
}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);
$communes_nouvelles_normalise=[];
foreach($communes_nouvelles as $commune => $commune_nouvelle){
    if(isset($commune)){
        if(normaliser($commune)==""){
        
        }else{
            $communes_nouvelles_normalise[normaliser($commune)]=$commune_nouvelle;
        }
    }
}
$communes_nouvelles_reverse=[];
foreach($communes_nouvelles as $commune => $commune_nouvelle){
    if(isset($commune)){
        if(normaliser($commune)==""){
        
        }else{
            if(isset($communes_nouvelles_reverse[normaliser($commune_nouvelle)])){
                $communes_nouvelles_reverse[normaliser($commune_nouvelle)][]=$commune;
            }else{
                $communes_nouvelles_reverse[normaliser($commune_nouvelle)]=[$commune];
            }
        }
    }
}

$communes_custom=json_decode(file_get_contents("communes_custom.json"),true);

$bdd_communes_euro=json_decode(file_get_contents("bdd_communes_euro.json"),true);
$insee_to_commune_euro=json_decode(file_get_contents("bdd_insee_euro.json"),true);

$bdd_communes_insee=json_decode(file_get_contents("bdd_communes_insee.json"),true);
$insee_to_commune_insee=json_decode(file_get_contents("bdd_insee_insee.json"),true);

$bdd_communes_reverse_normalise=json_decode(file_get_contents("bdd_communes_reverse_normaliser.json"),true);

$bdd_communes_pres=json_decode(file_get_contents("bdd_communes_pres_pourcentages.json"),true);

$bdd_communes_nominatim=json_decode(file_get_contents("bdd_communes_nominatim.json"),true);
$bdd_communes_nominatim_normalise=[];
foreach($bdd_communes_nominatim as $commune => $commune_nouvelle){
    if(isset($commune_nouvelle)){
        if(normaliser($commune)==""){
        
        }else{
            $bdd_communes_nominatim_normalise[normaliser($commune)]=$commune_nouvelle;
        }
    }
}
$bdd_communes_nominatim_reverse_normalise=[];
foreach($bdd_communes_nominatim as $commune => $commune_nouvelle){
    if(isset($commune_nouvelle)){
        if(normaliser($commune)==""){
        
        }else{
            $bdd_communes_nominatim_reverse_normalise[normaliser($commune_nouvelle)]=$commune;
        }
    }
}


$normalizeChars = array(
    ''=>'œ', ''=>'Œ',
);

$similarChars = array(
    'œ'=>'oe', 'Œ'=>'Oe', 'É'=>'E',
);

$withoutDash = array(
    '-'=>' ',
);

function normaliser($str){
    $str=strtolower($str);
    $str=trim($str);
    $espaces = array("\r\n", "\n", "\r", "\t");
    $str=str_replace($espaces,"",$str);
    if($str=="" || $str=="fr_etranger"){
        $str="EXPAT";
    }elseif(strpos($str,"arrondissement")){
        $str=explode(" ",$str)[0];
    }
    if(strpos($str,"(")){
        $prefix = substr($str,strpos($str,"(")+1,strpos($str,")")-strpos($str,"(")-1);
        $str = $prefix.str_replace(substr($str,strpos($str,"("),strpos($str,")")-strpos($str,"(")+1),"",$str);
    }
    $normaliserStrtr=array(
        ''=>'oe', ''=>'oe','œ'=>'oe', 'Œ'=>'oe', 'É'=>'e', 'é'=>'e', 'è'=>'e', 'È'=>'e', 'ë'=>'e', 'ê'=>'e', 'ï'=>'i', 'î'=>'i', 'Î'=>'i', 'ÿ'=>'y', 'ù'=>'u', 'û'=>'u', 'ü'=>'u', 'à'=>'a', 'â'=>'a', 'ô'=>'o', 'ç'=>'c', '-'=>'', '_'=>'', "'"=>'', "’"=>'', ' '=>'', '<'=>'', '>'=>'', '&'=>'', '3'=>'trois', '.'=>'', ','=>''
    );
    $str=strtr($str, $normaliserStrtr);
    return $str;
}

function curl_download($Url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_REFERER, "https://rip-le-compteur.dav.li/");
    curl_setopt($ch, CURLOPT_USERAGENT, "Dav.Li");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    curl_close($ch);
 
    return $output;
}

function getTotalInterval($interval){
    $days = $interval->format('%a');
    $seconds = 0;
    if($days){
        $seconds += 24 * 60 * 60 * $days;
    }
    $hours = $interval->format('%H');
    if($hours){
        $seconds += 60 * 60 * $hours;
    }
    $minutes = $interval->format('%i');
    if($minutes){
        $seconds += 60 * $minutes;
    }
    $seconds += $interval->format('%s');
    return $interval->format('%R').$seconds;
}

//Bdd dynamiques

$handle = fopen("cache-geo.txt", "r");
$cache = fread($handle, filesize("cache-geo.txt"));
fclose($handle);
$cache=json_decode($cache,true);

$now=new DateTime("now");
if($cache!="" && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=60*60){
    $adpripfr_bdd=$cache["adprip.fr"]["bdd"];
    $compteurrip_bdd=$cache["compteur.rip"]["bdd"];
    $vincib_bdd=$cache["adp.sonntag.fr"]["bdd"];
    
    $vincib_date=$cache["adp.sonntag.fr"]["date"];
    $compteurrip_date=$cache["compteur.rip"]["date"];
    $adpripfr_date=$cache["adprip.fr"]["date"];
    $vincib_licence=$cache["adp.sonntag.fr"]["licence"];
    $compteurrip_licence=$cache["compteur.rip"]["licence"];
    $adpripfr_licence=$cache["adprip.fr"]["licence"];
}else{
        
    $compteurrip_bdd=json_decode(curl_download("http://api.compteur.rip/communes.json"),true);
    $compteurrip_licence="CC-BY temporaire (Données utilisées avec l'accord du propriétaire de compteur.rip pour rip-le-compteur.dav.li)";
    $compteurrip_date="?";

    $vincib_index=json_decode(curl_download("https://adp.sonntag.fr/index.json"),true);
    $vincib_villes=end($vincib_index)["villes"];
    $vincib_bdd=json_decode(curl_download("https://adp.sonntag.fr/".$vincib_villes),true);
    $vincib_date=date_create(end($vincib_index)["date"])->format("d\/m\/y");
    $vincib_licence="CC0";

    $adpripfr_index=curl_download("https://www.adprip.fr/archive/carte/communes/");
    $adpripfr_index=explode('href="',$adpripfr_index);
    $adpripfr_index_array=[];
    foreach($adpripfr_index as $index){
        $index_link=explode('"',$index)[0];
        if(strpos($index_link,"data_communes.json")){
            $adpripfr_index_array[]=$index_link;
        }
    }
    $adpripfr_last=end($adpripfr_index_array);
    $adpripfr_date=DateTime::createFromFormat('U', explode(".",$adpripfr_last)[0])->format("d\/m\/y");
    $adpripfr_licence="Licence ouverte (v1.0)";
    $adpripfr_bdd_raw=json_decode(curl_download("https://www.adprip.fr/archive/carte/communes/".$adpripfr_last),true);
    
    $adpripfr_bdd=[];
    foreach($adpripfr_bdd_raw as $code_insee => $ville){
        if(isset($ville["nom"])){
            if(normaliser($ville["nom"])==""){

            }else{
                if($ville["h_soutiens"]!=0){
                    $signatures=$ville["h_soutiens"];
                }else{
                    $signatures=$ville["soutiens"];
                }
                if(!empty($signatures) && $signatures>0){
                    $adpripfr_bdd[$ville["nom"]]=$signatures;
                }
            }
        }
    }
    
    $cache=[
        "date"=>date("d\/m\/y H:i:s"),
        "compteur.rip"=>[
            "bdd"=>$compteurrip_bdd,
            "licence"=>$compteurrip_licence,
            "date"=>$compteurrip_date,
        ],
        "adprip.fr"=>[
            "bdd"=>$adpripfr_bdd,
            "licence"=>$adpripfr_licence,
            "date"=>$adpripfr_date,
        ],
        "adp.sonntag.fr"=>[
            "bdd"=>$vincib_bdd,
            "licence"=>$vincib_licence,
            "date"=>$vincib_date,
        ],
    ];
    $handle = fopen("cache-geo.txt", "w");
    fwrite($handle, json_encode($cache));
    fclose($handle);
}

$vincib_bdd_normalise=[];
foreach($vincib_bdd as $ville => $signatures){
    if(isset($ville)){
        $ville_normalise=normaliser($ville);
        if(isset($vincib_bdd_normalise[$ville_normalise])){
            $signatures+=$vincib_bdd_normalise[$ville_normalise]["signatures"];
        }
        $vincib_bdd_normalise[$ville_normalise]=[
            "nom_commune"=>$ville,
            "signatures"=>$signatures
        ];
    }
}


$compteurrip_bdd_normalise=[];
foreach($compteurrip_bdd as $ville => $signatures){
    if(isset($ville)){
        $ville_normalise=normaliser($ville);
        if(isset($compteurrip_bdd_normalise[$ville_normalise])){
            $signatures+=$compteurrip_bdd_normalise[$ville_normalise]["signatures"];
        }
        $compteurrip_bdd_normalise[$ville_normalise]=[
            "nom_commune"=>$ville,
            "signatures"=>$signatures
        ];
    }
}

$adpripfr_bdd_normalise=[];
foreach($adpripfr_bdd as $ville => $signatures){
    if(isset($ville)){
        if(!strpos($ville,"Arrondissement")){
            $ville_normalise=normaliser($ville);
            $adpripfr_bdd_normalise[$ville_normalise]=[
                "nom_commune"=>$ville,
                "signatures"=>$signatures
            ];
        }
    }
}


//stats

$handle = fopen("cache-stats-geo.txt", "r");
$cache = fread($handle, filesize("cache-stats-geo.txt"));
fclose($handle);
$cache=json_decode($cache,true);

$now=new DateTime("now");
if($cache!="" && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=60*60){
    $stats_pres=$cache["stats_pres"];
}else{
    
    //stats présidentielles
    $bdd_communes_pres_compteurrip=[];
    foreach($compteurrip_bdd as $ville => $signatures){
        if(normaliser($ville)!=""){
            if(!strpos($ville,"Arrondissement")){
                //var_dump($ville);
                $recherche=searchInDb($ville);
                //var_dump($recherche);

                if(!empty($recherche["inscrits"])){
                    if(isset($bdd_communes_pres[normaliser($ville)])){
                        $pourcentage_abstentions=$bdd_communes_pres[normaliser($ville)][3];
                        $pourcentage_gauche=$bdd_communes_pres[normaliser($ville)][4];
                        $pourcentage_centre=$bdd_communes_pres[normaliser($ville)][5];
                        $pourcentage_droite=$bdd_communes_pres[normaliser($ville)][6];
                        $pourcentage_extDroite=$bdd_communes_pres[normaliser($ville)][7];
                        if(isset($recherche["pourcentages"]["insee"])){
                            $pourcentage_signataires=$recherche["pourcentages"]["insee"];
                        }elseif(isset($recherche["pourcentages"]["euro2019"])){
                            $pourcentage_signataires=$recherche["pourcentages"]["euro2019"];
                        }
                        $bdd_communes_pres_compteurrip[normaliser($ville)]=[
                            "pourcentage_signataires"=>$pourcentage_signataires["compteur.rip"],
                            "pourcentage_abstentions"=>$pourcentage_abstentions,
                            "pourcentage_gauche"=>$pourcentage_gauche,
                            "pourcentage_centre"=>$pourcentage_centre,
                            "pourcentage_droite"=>$pourcentage_droite,
                            "pourcentage_extDroite"=>$pourcentage_extDroite,
                        ];
                    }
                }
            }
        }
    }
    
    function Corr($x, $y){ //from https://www.php.net/manual/en/function.stats-stat-correlation.php#116529
        $length= count($x);
        $mean1=array_sum($x) / $length;
        $mean2=array_sum($y) / $length;

        $a=0;
        $b=0;
        $axb=0;
        $a2=0;
        $b2=0;

        for($i=0;$i<$length;$i++){
            $a=$x[$i]-$mean1;
            $b=$y[$i]-$mean2;
            $axb=$axb+($a*$b);
            $a2=$a2+ pow($a,2);
            $b2=$b2+ pow($b,2);
        }

        $corr= $axb / sqrt($a2*$b2);

        return $corr;
    }

    $stats_pres=[
        "abstentionnistes"=>Corr(array_column($bdd_communes_pres_compteurrip,"pourcentage_signataires"),array_column($bdd_communes_pres_compteurrip,"pourcentage_abstentions")),
        "gauche"=>Corr(array_column($bdd_communes_pres_compteurrip,"pourcentage_signataires"),array_column($bdd_communes_pres_compteurrip,"pourcentage_gauche")),
        "centre"=>Corr(array_column($bdd_communes_pres_compteurrip,"pourcentage_signataires"),array_column($bdd_communes_pres_compteurrip,"pourcentage_centre")),
        "droite"=>Corr(array_column($bdd_communes_pres_compteurrip,"pourcentage_signataires"),array_column($bdd_communes_pres_compteurrip,"pourcentage_droite")),
        "extDroite"=>Corr(array_column($bdd_communes_pres_compteurrip,"pourcentage_signataires"),array_column($bdd_communes_pres_compteurrip,"pourcentage_extDroite")),
    ];
    
    $cache=[
        "date"=>date("d\/m\/y H:i:s"),
        "stats_pres"=>$stats_pres,
    ];
    $handle = fopen("cache-stats-geo.txt", "w");
    fwrite($handle, json_encode($cache));
    fclose($handle);
}


//Search

function map_bdd($e){
    return [
        "code_insee"=>intval($e[0]),
        "inscrits"=>intval($e[1]),
    ];
}


function searchInDb($query,$returnEstimations=true){
    global $vincib_bdd_normalise;
    global $compteurrip_bdd_normalise;
    global $adpripfr_bdd_normalise;
    
    global $vincib_date;
    global $compteurrip_date;
    global $adpripfr_date;
    global $vincib_licence;
    global $compteurrip_licence;
    global $adpripfr_licence;
    
    global $communes_custom;
    
    global $insee_to_commune_euro;
    global $insee_to_commune_insee;
    
    global $bdd_communes_euro;
    global $bdd_communes_insee;
    
    global $communes_nouvelles_normalise;
    global $communes_nouvelles_reverse;
    
    global $bdd_communes_reverse_normalise;
    
    global $bdd_communes_nominatim_normalise;
    global $bdd_communes_nominatim_reverse_normalise;
    
    $returnData=[];
    
    if(is_numeric($query)){
        $code_insee=$query;
        $returnData["query"]=$code_insee;
        
        $code_insee_to_commune=[];
        
        if(isset($insee_to_commune_euro[$code_insee])){
            $code_insee_to_commune["euro2019"]=$insee_to_commune_euro[$code_insee];
            $ville=normaliser($insee_to_commune_euro[$code_insee]);
        }
        if(isset($insee_to_commune_insee[$code_insee])){
            $code_insee_to_commune["insee"]=$insee_to_commune_insee[$code_insee];
            $ville=normaliser($insee_to_commune_insee[$code_insee]);
        }
        if(normaliser($insee_to_commune_euro[$code_insee])!=normaliser($insee_to_commune_insee[$code_insee])){
            $autre_ville=normaliser($insee_to_commune_euro[$code_insee]);
        }
        $returnData["code_insee_to_commune"]=$code_insee_to_commune;
    }else{
        $ville=normaliser($query);
        $returnData["query"]=$ville;
    }
    
    $vincib_signatures=$vincib_bdd_normalise[$ville]["signatures"];
    $compteurrip_signatures=$compteurrip_bdd_normalise[$ville]["signatures"];
    $adpripfr_signatures=$adpripfr_bdd_normalise[$ville]["signatures"];
    if(isset($autre_ville)){
        if(is_null($vincib_signatures)){
            $vincib_signatures=$vincib_bdd_normalise[$autre_ville]["signatures"];
        }
        if(is_null($compteurrip_signatures)){
            $compteurrip_signatures=$compteurrip_bdd_normalise[$autre_ville]["signatures"];
        }
        if(is_null($adpripfr_signatures)){
            $adpripfr_signatures=$adpripfr_bdd_normalise[$autre_ville]["signatures"];
        }
    }
    
    if(isset($bdd_communes_reverse_normalise[$ville])){
        $returnData["nom_commune"]=$bdd_communes_reverse_normalise[$ville];
    }
    if(isset($communes_nouvelles_normalise[$ville])){
        $returnData["nom_commune_nouvelle"]=$communes_nouvelles_normalise[$ville];
    }
    if(isset($communes_custom[$ville])){
        $returnData["nom_commune_custom"]=$communes_custom[$ville];
    }
    if(isset($communes_nouvelles_reverse[$ville])){
        $returnData["nom_communes_deleguees"]=$communes_nouvelles_reverse[$ville];
        if(empty($vincib_signatures)){
            $vincib_signatures=$vincib_bdd_normalise[normaliser($communes_nouvelles_reverse[$ville][0])]["signatures"];
        }
        if(empty($compteurrip_signatures)){
            $compteurrip_signatures=$compteurrip_bdd_normalise[normaliser($communes_nouvelles_reverse[$ville][0])]["signatures"];
        }
        if(empty($adpripfr_signatures)){
            $adpripfr_signatures=$adpripfr_bdd_normalise[normaliser($communes_nouvelles_reverse[$ville][0])]["signatures"];
        }
    }
    if(isset($bdd_communes_nominatim_normalise[$ville])){
        $returnData["nom_commune_nominatim"]=$bdd_communes_nominatim_normalise[$ville];
    }elseif(isset($bdd_communes_nominatim_reverse_normalise[$ville])){
        $returnData["nom_commune_nominatim"]=$bdd_communes_nominatim_reverse_normalise[$ville];
        if(empty($vincib_signatures)){
            $vincib_signatures=$vincib_bdd_normalise[normaliser($bdd_communes_nominatim_reverse_normalise[$ville])]["signatures"];
        }
        if(empty($compteurrip_signatures)){
            $compteurrip_signatures=$compteurrip_bdd_normalise[normaliser($bdd_communes_nominatim_reverse_normalise[$ville])]["signatures"];
        }
        if(empty($adpripfr_signatures)){
            $adpripfr_signatures=$adpripfr_bdd_normalise[normaliser($bdd_communes_nominatim_reverse_normalise[$ville])]["signatures"];
        }
    }
    
    $inscrits=[];
    $inscrits_merged=[];
    
    if(isset($bdd_communes_euro[$ville])){
        $inscrits["euro2019"]=array_map("map_bdd",$bdd_communes_euro[$ville]);
        $inscrits_merged["euro2019"]=$inscrits["euro2019"];
    }
    if(isset($bdd_communes_euro[normaliser($communes_nouvelles_normalise[$ville])])){
        $inscrits["euro2019_nouvelle_com"]=array_map("map_bdd",$bdd_communes_euro[normaliser($communes_nouvelles_normalise[$ville])]);
        if(!empty($inscrits_merged["euro2019"])){
            $inscrits_merged["euro2019"]=array_merge($inscrits_merged["euro2019"],$inscrits["euro2019_nouvelle_com"]);
        }else{
            $inscrits_merged["euro2019"]=$inscrits["euro2019_nouvelle_com"];
        }
    }
    if(isset($bdd_communes_euro[normaliser($bdd_communes_nominatim_normalise[$ville])])){
        $inscrits["euro2019_nominatim"]=array_map("map_bdd",$bdd_communes_euro[normaliser($bdd_communes_nominatim_normalise[$ville])]);
        if(!empty($inscrits_merged["euro2019"])){
            $inscrits_merged["euro2019"]=array_merge($inscrits_merged["euro2019"],$inscrits["euro2019_nominatim"]);
        }else{
            $inscrits_merged["euro2019"]=$inscrits["euro2019_nominatim"];
        }
    }
    if(isset($bdd_communes_euro[normaliser($communes_custom[$ville])])){
        $inscrits["euro2019_custom"]=array_map("map_bdd",$bdd_communes_euro[normaliser($communes_custom[$ville])]);
        if(!empty($inscrits_merged["euro2019"])){
            $inscrits_merged["euro2019"]=array_merge($inscrits_merged["euro2019"],$inscrits["euro2019_custom"]);
        }else{
            $inscrits_merged["euro2019"]=$inscrits["euro2019_custom"];
        }
    }
    if(isset($bdd_communes_insee[$ville])){
        $inscrits["insee"]=array_map("map_bdd",$bdd_communes_insee[$ville]);
        $inscrits_merged["insee"]=$inscrits["insee"];
    }
    if(isset($bdd_communes_insee[normaliser($communes_nouvelles_normalise[$ville])])){
        $inscrits["insee_nouvelle_com"]=array_map("map_bdd",$bdd_communes_insee[normaliser($communes_nouvelles_normalise[$ville])]);
        if(!empty($inscrits_merged["insee"])){
            $inscrits_merged["insee"]=array_merge($inscrits_merged["insee"],$inscrits["insee_nouvelle_com"]);
        }else{
            $inscrits_merged["insee"]=$inscrits["insee_nouvelle_com"];
        }
    }
    if(isset($bdd_communes_insee[normaliser($bdd_communes_nominatim_normalise[$ville])])){
        $inscrits["insee_nominatim"]=array_map("map_bdd",$bdd_communes_insee[normaliser($bdd_communes_nominatim_normalise[$ville])]);
        if(!empty($inscrits_merged["insee"])){
            $inscrits_merged["insee"]=array_merge($inscrits_merged["insee"],$inscrits["insee_nominatim"]);
        }else{
            $inscrits_merged["insee"]=$inscrits["insee_nominatim"];
        }
    }
    if(isset($bdd_communes_insee[normaliser($communes_custom[$ville])])){
        $inscrits["insee_custom"]=array_map("map_bdd",$bdd_communes_insee[normaliser($communes_custom[$ville])]);
        if(!empty($inscrits_merged["insee"])){
            $inscrits_merged["insee"]=array_merge($inscrits_merged["insee"],$inscrits["insee_custom"]);
        }else{
            $inscrits_merged["insee"]=$inscrits["insee_custom"];
        }
    }
    $inscrits_merged_unique=[];
    foreach($inscrits_merged as $source => $communes){
        foreach($communes as $commune){
            $code_insee=$commune["code_insee"];
            $hasSimilarInsee=false;
            foreach($inscrits_merged_unique[$source] as $commune_merged){
                if($commune_merged["code_insee"]==$code_insee){
                    $hasSimilarInsee=true;
                }
            }
            if(!$hasSimilarInsee){
                $inscrits_merged_unique[$source][]=$commune;
            }
        }
    }
    $inscrits_merged=$inscrits_merged_unique;
    $returnData["inscrits"]=$inscrits;
    
    $compteurs_signatures=[
        "adp.sonntag.fr"=>$vincib_signatures,
        "compteur.rip"=>$compteurrip_signatures,
        "adprip.fr"=>$adpripfr_signatures,
    ];
    $returnData["signatures"]=$compteurs_signatures;
    
    $total_inscrits=[];
    foreach($inscrits_merged as $source => $data){
        $total_inscrits[$source]=0;
        foreach($inscrits_merged[$source] as $ville){
            $total_inscrits[$source]+=$ville["inscrits"];
        }
    }
    $returnData["total_inscrits"]=$total_inscrits;
    $pourcentages=[];
    foreach($total_inscrits as $source => $inscrits){
        foreach($compteurs_signatures as $compteur => $signatures){
            $pourcentages[$source][$compteur]=$signatures/$inscrits*100;
            if($pourcentages[$source][$compteur]==INF){
                $pourcentages[$source][$compteur]=-1;
            }
        }
    }
    $returnData["pourcentages"]=$pourcentages;
    
    $hasHomonymes=false;
    $homonymes_list=[];
    foreach($inscrits_merged as $source => $data){
        if(sizeof($data)>1){
            $hasHomonymes=true;
        }
        foreach($data as $d){
            $homonymes_list[$d["code_insee"]]=true;
        }
    }
    $homonymes_count=sizeof($homonymes_list);
    
    if($hasHomonymes){
        if($returnEstimations){
        $signatures_calculs=[];
            foreach($inscrits_merged as $source => $data){
                foreach($inscrits_merged[$source] as $ville){
                    $signatures_ville=[];
                    $signatures_ville_round=[];
                    $pourcentages=[];
                    foreach($compteurs_signatures as $compteur => $signatures){
                        $signatures_ville[$compteur]=$signatures*$ville["inscrits"]/$total_inscrits[$source];
                        if(is_float($signatures_ville[$compteur])){
                            $signatures_ville_precis[$compteur]=$signatures_ville[$compteur];
                            $signatures_ville[$compteur]=round($signatures*$ville["inscrits"]/$total_inscrits[$source]);
                        }
                        $pourcentages[$compteur]=($signatures*$ville["inscrits"]/$total_inscrits[$source])/$ville["inscrits"]*100;
                    }
                    $signatures_calculs_ville=[
                        "code_insee"=>$ville["code_insee"],
                    ];
                    if(!isset($signatures_ville_precis)){
                        $signatures_calculs_ville["signatures_precis"]=$signatures_ville_precis;
                    }
                    $signatures_calculs_ville["signatures"]=$signatures_ville;
                    $signatures_calculs_ville["pourcentages"]=$pourcentages;
                    $signatures_calculs[$source][]=$signatures_calculs_ville;
                }
            }
            $returnData["calculs_estimations"]=$signatures_calculs;
        }
        $returnData["homonymes_count"]=$homonymes_count;
    }
    
    $returnData["infos_sources"]=[
        "adp.sonntag.fr"=>[
            "licence"=>$vincib_licence,
            "date"=>$vincib_date
        ],
        "compteur.rip"=>[
            "licence"=>$compteurrip_licence,
            "date"=>$compteurrip_date
        ],
        "adprip.fr"=>[
            "licence"=>$adpripfr_licence,
            "date"=>$adpripfr_date
        ]
    ];
    
    return $returnData;
}