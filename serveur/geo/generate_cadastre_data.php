<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include("func.php");

$cadastre_data=json_decode(file_get_contents("communes-cadastre.json"), true);

$output_features=[];
$stats_communes_signatures=0;
$stats_communes=0;
$stats_communes_avec_inscrits=0;
$communes_0_signature=[];
$communes_sans_inscrit=[];
$communes_insee=[];
$communes_nouvelles=[];

foreach($cadastre_data as $feature){
    $this_feature=[];
    $this_feature["type"]="Feature";
    $this_feature["geometry"]=$feature["centre"];
    $this_feature["properties"]["code_insee"]=$feature["code"];
    $this_feature["properties"]["nom"]=$feature["nom"];
    $this_inscrits=false;
    $recherche=searchInDb($feature["nom"]);
    if(sizeof($recherche["total_inscrits"])>0){
        $this_inscrits=true;
    }
    if($recherche["signatures"]["compteur.rip"]){
        $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
        if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
            $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
        }else{
            $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
        }
        $stats_communes_signatures++;
    }else{
        if($recherche["nom_commune_nouvelle"]){
            $recherche=searchInDb($recherche["nom_commune_nouvelle"]);
            $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
            if(sizeof($recherche["total_inscrits"])>0){
                $this_inscrits=true;
            }
            if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
                $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
            }else{
                $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
            }
            $stats_communes_signatures++;
        }else{
            $recherche=searchInDb($feature["code"]);
            if($recherche["signatures"]["compteur.rip"]){
                $this_feature["properties"]["signatures"]=$recherche["signatures"]["compteur.rip"];
                if(sizeof($recherche["total_inscrits"])>0){
                    $this_inscrits=true;
                }
                if(isset($recherche["total_inscrits"]["insee"]) && sizeof($recherche["calculs_estimations"]["insee"])>=sizeof($recherche["calculs_estimations"]["euro2019"])){
                    $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["insee"]["compteur.rip"];
                }else{
                    $this_feature["properties"]["pourcentage"]=$recherche["pourcentages"]["euro2019"]["compteur.rip"];
                }
                $stats_communes_signatures++;
            }else{
                $communes_0_signature[$feature["code"]]=$feature["nom"];
            }
        }
    }
    if($this_inscrits){
        $stats_communes_avec_inscrits++;
    }else{
        $communes_sans_inscrit[$feature["code"]]=$feature["nom"];
    }
    $stats_communes++;
    $output_features[]=$this_feature;
}


$output=[
    "type"=>"FeatureCollection",
    "copyright"=>"Geoloc : Cadastre. Inscrits : Insee au 1er janvier 2019 (https://statistiques-locales.insee.fr/) ou élections européennes 2019 (https://www.data.gouv.fr/fr/datasets/resultats-des-elections-europeennes-2019/). Signatures : CC-BY Discord Insoumis (compteur.rip)",
];

if(!empty($_GET["display"]) && $_GET["display"]=="full"){
    $output["stats"]="Avec signature(s) : ".$stats_communes_signatures."/".$stats_communes." : ".($stats_communes-$stats_communes_signatures)." | Avec inscrit(s) : ".$stats_communes_avec_inscrits."/".$stats_communes." : ".($stats_communes-$stats_communes_avec_inscrits);
    $output["communes_0_signature"]=$communes_0_signature;
    $output["communes_sans_inscrit"]=$communes_sans_inscrit;
}

$output["features"]=$output_features;

header('Content-Type: application/json');
echo(json_encode($output));

file_put_contents("geo_data.json",json_encode($output));

//echo("Done.");