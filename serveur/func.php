<?php

function curl_download($Url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_REFERER, "https://rip-le-compteur.dav.li/");
    curl_setopt($ch, CURLOPT_USERAGENT, "Dav.Li");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    curl_close($ch);
 
    return $output;
}

function getTotalInterval($interval){
    $days = $interval->format('%a');
    $seconds = 0;
    if($days){
        $seconds += 24 * 60 * 60 * $days;
    }
    $hours = $interval->format('%H');
    if($hours){
        $seconds += 60 * 60 * $hours;
    }
    $minutes = $interval->format('%i');
    if($minutes){
        $seconds += 60 * $minutes;
    }
    $seconds += $interval->format('%s');
    return $interval->format('%R').$seconds;
}
function array_median($array) {
  $iCount = count($array);
  if ($iCount == 0) {
    throw new DomainException('Median of an empty array is undefined');
  }
  $middle_index = floor($iCount / 2);
  sort($array, SORT_NUMERIC);
  $median = $array[$middle_index];
  return $median;
}


function getBdd($date="now"){
    $alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    $totalContrib=0;
    $contribUnique=[];
    $bdd_aggregate=[];
    $bdd_digramme=[];
    $bdd_single=[];
    $bdd_dated=[];

    ini_set('auto_detect_line_endings',TRUE);
    $handle = fopen("data/data-aggregate.txt",'r');
    while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
        if(isset($data[0]) && isset($data[3]) && strlen($data[0])==17 && sizeOf($data)<=7 && DateTime::createFromFormat('d/m/y H:i:s', $data[0])) {
            $totalContrib++;
            if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))>=0){
                $thisData=[
                    "date"      => $data[0],
                    "section"   => $data[3],
                    "compteur"  => intval($data[4] ?? -1),
                    "nb_pages"  => intval($data[5] ?? round(intval($data[4] ?? 0)/200)),
                    "is_last"   => true,
                    "aggregate" => true,
                ];
                $bdd_aggregate[$data[3]]=$thisData;
            }
        }

    }
    fclose($handle);
    
    $f=1;
    while(file_exists("data/data-".$f.".txt")){
        $handle = fopen("data/data-".$f.".txt",'r');
        while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
            if(isset($data[0]) && isset($data[3]) && strlen($data[0])==17 && sizeOf($data)<=7 && DateTime::createFromFormat('d/m/y H:i:s', $data[0])) {
                if($data[3]=="single"){
                    $dateDiff=round(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))/3600,PHP_ROUND_HALF_DOWN);

                    $thisData=[
                        "date"      => $data[0],
                        "compteur"  => intval($data[4] ?? -1),
                        "nb_pages"  => intval($data[5] ?? -1),
                        "is_last"   => boolval($data[6] ?? FALSE),
                    ];

                    if($bdd_dated[$dateDiff]==null){
                        $bdd_dated[$dateDiff]=[];
                    }
                    $bdd_dated[$dateDiff][]= $thisData;

                    $bdd_single[] = $thisData;
                }else{
                    if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))>=0){
                        $thisData=[
                            "date"      => $data[0],
                            "section"   => $data[3],
                            "compteur"  => intval($data[4] ?? -1),
                            "nb_pages"  => intval($data[5] ?? -1),
                            "is_last"   => boolval($data[6] ?? FALSE),
                            "aggregate" => false,
                        ];
                        $bdd_digramme[$data[3]][]=$thisData;
                    }
                }
                if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))>=0){
                    $totalContrib++;
                    $contribUnique[$data[1]]="1";
                }
            }

        }
        fclose($handle);
        $f++;
    }

    ini_set('auto_detect_line_endings',FALSE);
    $bdd_alph=[];
    for ($i=0; $i<26; $i++) {//par ordre alphabéthique
        $section=$alphabet[$i]."_";
        $bdd_alph[$section]=$bdd_digramme[$section];
        for ($j=0; $j<26; $j++) {
            $section=$alphabet[$i].$alphabet[$j];
            $bdd_alph[$section]=$bdd_digramme[$section];
        }
    }
    return [
        "bdd_aggregate"=>$bdd_aggregate,
        "bdd_digramme"=>$bdd_alph,
        "bdd_single"=>$bdd_single,
        "bdd_single_dated"=>$bdd_dated,
        "totalContrib"=>$totalContrib,
        "contribUnique"=>sizeof($contribUnique)
    ];
}


function getStatPerso(){
    $secret="secret";
    
    $ipSha256=hash("sha256", $secret.$_SERVER["REMOTE_ADDR"]);
    $ipMd5=hash("sha256", $secret.md5($secret.$_SERVER["REMOTE_ADDR"])); //premières données avant migration vers sha256
        
    $totalContribPerso=0;
    
    $i=0;

    ini_set('auto_detect_line_endings',TRUE);
    $handle = fopen("data/data.txt",'r');
    while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
        if(isset($data[0]) && isset($data[3]) && strlen($data[0])==17 && sizeOf($data)<=7 && DateTime::createFromFormat('d/m/y H:i:s', $data[0])) {
            if($data[1]==$ipSha256 || $data[1]==$ipMd5){
                $totalContribPerso++;
            }
        }

    }
    fclose($handle);
    
    return($totalContribPerso);
}


function getData($date="now",$refreshCache=false){
    $handle = fopen("cache/cache.txt", "r");
    $cache = fread($handle, filesize("cache/cache.txt"));
    fclose($handle);
    $cache=json_decode($cache,true);
    $now=new DateTime("now");
    if(!$refreshCache && $date=="now" && $cache!=null && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=4*60){
        return $cache;
    }else{//refresh cache and provide data
        if($date=="now"){
            $date=$now;
            $isRealtime=true;
        }else{
            $date=DateTime::createFromFormat('d/m/y H:i:s', $date);
            $isRealtime=false;
        }
        
        $getBdd=getBdd($date);
        $bdd_aggregate=$getBdd["bdd_aggregate"];
        $bdd_digramme=$getBdd["bdd_digramme"];
        $bdd_single=$getBdd["bdd_single"];
        $bdd_single_dated=$getBdd["bdd_single_dated"];
        $totalContrib=$getBdd["totalContrib"];
        $contribUnique=$getBdd["contribUnique"];
        
        //calcul du total single
        $h=-1;
        $total_single=-1;
        $precisOnly=true;
        while($total_single==-1){
            $h++;
            
            $contribs=$bdd_single_dated[$h];
            
            if($contribs){
                if($precisOnly){
                    $contribs=array_filter($contribs,function($value) {
                        return $value["is_last"];
                    });
                }
                if(sizeof($contribs)>=2){
                    $compteurs=array_column($contribs, "compteur");
                    $pages=array_column($contribs, "nb_pages");
                    $total_single=end($compteurs);
                    $totalContradiction=array_count_values($compteurs);
                    $page=end($pages);
                }   
            }
            if($h>=72){
                if($precisOnly==false){
                    $total_single=0;
                }else{
                    $h=0;
                    $precisOnly=false;
                }
            }
        }
        
        //méthode digramme
        $bdd_last=[];
        $bdd_best=[];
        foreach ($bdd_digramme as $section => $lignes) {
            $bdd_last[$section] = end($lignes);
            // On parcourt en partant de la fin
            for ($k = count($lignes) - 1; $k >= 0; $k--) {
                if (DateTime::createFromFormat('d/m/y H:i:s', $lignes[$k]['date'])->diff($date)->format("%a")<=1 && $lignes[$k]['is_last']) {
                    $bdd_best[$section] = $lignes[$k];
                    break;
                }
            }
            if(isset($bdd_best[$section]) && isset($bdd_aggregate[$section]) && DateTime::createFromFormat('d/m/y H:i:s', $bdd_aggregate[$section]['date'])->diff(DateTime::createFromFormat('d/m/y H:i:s', $bdd_best[$section]['date']))->format("%a")<=1){
                $bdd_best[$section]=$bdd_aggregate[$section];
            }
            if (!isset($bdd_best[$section])) {
                // On a pas trouvé de compte exact, on prend le dernier
                $bdd_best[$section] = $bdd_last[$section];
            }
            $total_digramme+=$bdd_best[$section]["compteur"];
        }
        
        $total=max($total_digramme,$total_single);
        
        //aggregate
        if($isRealtime){
            $bdd_aggregate=getAggregate();
            $bdd_difference=[];
            foreach($bdd_aggregate as $source => $content){
                if($source!="adp.sonntag.fr"){
                    if(isset($content["bdd"])){
                        foreach($content["bdd"] as $digramme => $signatures){
                            if($digramme!="" && (!$bdd_best[$digramme]["is_last"] || $bdd_best[$digramme]["compteur"]<$signatures)){
                                $bdd_difference[$digramme][$source]=$signatures-$bdd_best[$digramme]["compteur"];
                            }
                        }
                    }
                }
            }
            $total_difference=0;
            foreach($bdd_difference as $digramme => $diff){
                $total_difference+=max($diff);
            }

            $total+=$total_difference;
        }
        
        if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', "25/06/19 23:59:59")->diff($date))>0 && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', "12/08/19 23:59:59")->diff($date))<=0){
            $difference=378477-373772; //différence observée le 25/06
            $pourcentage_nonaffichees=$difference/373772; //calcul du pourcentage de signatures non-affichées (c.f. FAQ)
            $signatures_nonaffichees=round($total*$pourcentage_nonaffichees);
            $total_estimation=round($total*(1+$pourcentage_nonaffichees),0);
            $total_estimation=intval(substr(strval($total_estimation),0,sizeof(strval($total_estimation))-4)."000"); //truncate (we love php)
            $total=$total_estimation;
        }
        
        $pourcentage=round($total/4717396*100,2);
        $precis=$precisOnly;
        
        $data=[
            "date"=>date("d\/m\/y H:i:s"),
            "total"=>$total,
            "total_digramme"=>$total_digramme,
            "total_estimation"=>$total_estimation,
            "estimation_signatures_nonaffichees"=>$signatures_nonaffichees,
            "total_single"=>$total_single,
            "pourcentage"=>$pourcentage,
            "precis"=>$precis,
            "page_single"=>$page,
            "hoursAgo_single"=>$h,
            "contribsCount_single"=>sizeof($contribs),
            "totalContradiction_single"=>$totalContradiction,
            "bdd_dated"=>$bdd_dated,
            "bdd_calculation"=>$contribs,
            "bdd_best"=>$bdd_best,
            "bdd_last"=>$bdd_last,
            "totalContrib"=>$totalContrib,
            "contribUnique"=>$contribUnique,
            "bdd_difference"=>$bdd_difference,
            "total_difference"=>$total_difference
        ];
        if($isRealtime){
            $handle = fopen("cache/cache.txt", "w");
            fwrite($handle, json_encode($data));
            fclose($handle);
        }
        $data["isRealtime"]=$isRealtime;
        return $data;
    }
}


function getStats($refreshCache=false){
    $handle = fopen("cache/cache-stats.txt", "r");
    $cache = fread($handle, filesize("cache/cache-stats.txt"));
    fclose($handle);
    $cache=json_decode($cache,true);
    
    $now=new DateTime("now");
    $dateDebut=DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 23:59:00");
    $days=$dateDebut->diff($now)->format("%a")+1;
    
    if(!$refreshCache && sizeof($cache["stats"])==$days){
        $stats=$cache["stats"];
    }else{
        //Sauvegarder les données agrégées si elles manquent
        $bdd_data=getData();
        $bdd_best=$bdd_data["bdd_best"];
        $bdd_difference=$bdd_data["bdd_difference"];
        $diff_a_ajouter="";
        foreach($bdd_difference as $digramme => $diff){
            $max=max($diff);
            if($diff["compteur.rip"]==$max){
                $source="compteur.rip";
            }elseif($diff["adprip.fr"]==$max){
                $source="adprip.fr";
            }elseif($diff["adp.sonntag.fr"]==$max){
                $source="adp.sonntag.fr";
            }
            $diff_a_ajouter.=date("d\/m\/y H:i:s").",".$source.",8,".$digramme.",".($bdd_best[$digramme]["compteur"]+$max)."\n";
        }
        $handle = fopen("data/data-aggregate.txt", "ab");
        fwrite($handle, $diff_a_ajouter);
        fclose($handle);
        
        $bdd_data=getData();
        $stats=$cache["stats"];
        $dateStat=$dateDebut->modify("-1 day");
        for($d=0;$d<$days;$d++){
            $dateStat->modify("+1 day");
            if(!isset($stats[date_format($dateStat,"d\/m\/y")])){
                $getData=getData(date_format($dateStat,"d\/m\/y")." 23:59:59");
                $localStat=[
                    "total"=>$getData["total"],
                    "pourcentage"=>$getData["pourcentage"],
                    "totalContrib"=>$getData["totalContrib"],
                    "contribUnique"=>$getData["contribUnique"],
                ];
                $stats[date_format($dateStat,"d\/m\/y")]=$localStat;
            }
        }
        $data=[
            "date"=>date("d\/m\/y H:i:s"),
            "stats"=>$stats,
        ];
        $handle = fopen("cache/cache-stats.txt", "w");
        fwrite($handle, json_encode($data));
        fclose($handle);
    }
    return($stats);
}

function getAggregate($refreshCache=false){
    $handle = fopen("cache/cache-aggregate.txt", "r");
    $cache = fread($handle, filesize("cache/cache-aggregate.txt"));
    fclose($handle);
    $cache=json_decode($cache,true);
    
    $now=new DateTime("now");
    if(!$refreshCache && $cache!="" && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=60*60 && ($now->format("G")!=10 || getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=5*60)){
        $data=$cache;
    }else{
        $libeLink="";
        $libeImg="";
        $libeDate="";
        
        $libeChecknewsRss=file_get_contents("http://rss.liberation.fr/rss/100893/");
        $libeChecknewsRss=new SimpleXMLElement($libeChecknewsRss);
        
        foreach ($libeChecknewsRss->entry as $article) {
            if(strpos($article->title,"Référendum ADP")===false && strpos($article->title,"Referendum ADP")===false && strpos($article->title,"Compteur ADP")===false){
                //pas cet article
            }else{
                foreach ($article->link as $link) {
                    if($link["rel"]->__toString()=="alternate"){
                        $libeLink=$link["href"]->__toString();
                    }elseif($link["type"]->__toString()=="image/jpeg" ||  $link["type"]->__toString()=="image/jpg" || $link["type"]->__toString()=="image/png"){
                        $libeImg=explode("?",$link["href"]->__toString())[0];
                    }
                    $libeDate=$article->updated->__toString();
                }
                break;
            }
        }
        file_put_contents("img/liberation-compteur-adp.jpg", file_get_contents($libeImg));
        
        $ctrip_bdd=json_decode(file_get_contents("https://api.compteur.rip/decomptes"),true); //API utilisée avec l'accord du propriétaire pour rip-le-compteur.dav.li
        foreach($ctrip_bdd as $section => $comptage){
            if(strlen($section)==1){
                $ctrip_bdd[$section."_"]=$comptage;
                unset($ctrip_bdd[$section]);
            }
        }
        file_put_contents("img/compteurrip.gif", file_get_contents("https://api.compteur.rip/compteur.gif"));

        $vincib_index=json_decode(file_get_contents("https://adp.sonntag.fr/index.json"),true);
        $vincib_lettres=end($vincib_index)["lettres"];
        $vincib_bdd=json_decode(file_get_contents("https://adp.sonntag.fr/".$vincib_lettres),true);
        $vincib_date=end($vincib_index)["date"];

        $adpripfr_index=curl_download("https://www.adprip.fr/archive/compteur/");
        $adpripfr_index=explode('href="',$adpripfr_index);
        $adpripfr_index_array=[];
        foreach($adpripfr_index as $index){
            $index_link=explode('"',$index)[0];
            if(strpos($index_link,"cache.dat.gz")){
                $adpripfr_index_array[]=$index_link;
            }
        }
        $adpripfr_last=end($adpripfr_index_array);
        $adpripfr_date=DateTime::createFromFormat('U', explode(".",$adpripfr_last)[0])->format("d\/m\/y");
        $adprip_cache="";
        $gz=gzopen("https://www.adprip.fr/archive/compteur/".$adpripfr_last,"r");
        while (!gzeof($gz)) {
            $adprip_cache.=gzread($gz, 4096);
        }
        gzclose($gz);
        $adprip_bdd=[];
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $adprip_cache) as $line){
            $line_array=explode(":",$line);
            if(!empty($line_array[0])){
                $adprip_bdd[$line_array[0]]=($line_array[1]-1)*200+$line_array[2];
            }
        }

        
        $data=[
            "date"=>date("d\/m\/y H:i:s"),
            "liberation"=>[
                "date"=>$libeDate,
                "link"=>$libeLink,
                "img"=>$libeImg,
            ],
            "compteur.rip"=>[
                "bdd"=>$ctrip_bdd,
                "licence"=>"(CC-BY temporaire) Données utilisées avec l'accord du propriétaire de compteur.rip pour rip-le-compteur.dav.li"
            ],
            "adprip.fr"=>[
                "date"=>$adpripfr_date,
                "bdd"=>$adprip_bdd,
                "licence"=>"Licence ouverte (v1.0)"
            ],
            "adp.sonntag.fr"=>[
                "date"=>$vincib_date,
                "bdd"=>$vincib_bdd,
                "licence"=>"CC0"
            ],
        ];
        $handle = fopen("cache/cache-aggregate.txt", "w");
        fwrite($handle, json_encode($data));
        fclose($handle);
    }
    return($data);
}