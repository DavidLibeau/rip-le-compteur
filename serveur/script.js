$(function() {
    //nav
    $("#openMenu").click(function(){
        $("nav").animate({
            left: 0
        }, 200);
        $("main").animate({
            opacity: 0.5
        }, 200);
        if($(window).scrollTop()>50) {
            $("header").animate({
                top: -200,
            }, 200);
        }else {
            $("header").animate({
                opacity: 0.5
            }, 200);
        }
        if($(window).scrollTop()<100) {
            $("footer").animate({
                bottom: -200,
            }, 200);
        }else {
            $("footer").animate({
                opacity: 0.5
            }, 200);
        }
        $("html").css("overflow-y","hidden");
    });
    $("#closeMenu,main").click(function(){
        $("nav").animate({
            left: -350
        }, 200);
        $("main,header").animate({
            opacity: 1
        }, 200);
        if($(window).scrollTop()>50) {
            $("header").animate({
                top: 0,
            }, 200);
        }else {
            $("header").animate({
                opacity: 1
            }, 200);
        }
        if($(window).scrollTop()<100) {
            $("footer").animate({
                bottom: "-" + $(window).scrollTop() + "px",
            }, 200);
        }else {
            $("footer,header").animate({
                opacity: 1
            }, 200);
        }
        $("html").css("overflow-y","auto");
    });
    //footer
    $(window).scroll(function(evt) {
        if($(window).scrollTop()<100) {
            $("footer").css({
                "bottom": "-" + $(window).scrollTop() + "px",
                "position": "fixed"
            });
        } else {
            $("footer").css({
                "bottom": 0,
                "position": "relative"
            });
        }
    });

    //Mobile swipe
    /*
    var previousScreenX=0;
    $(document).on("touchstart", function (evt) {
        previousScreenX=evt.touches[0].screenX;
    });
    $(document).on("touchmove", function (evt) {
        var localScreenX=evt.touches[0].screenX;
        console.log(parseInt($("nav").css("left"),10));
        if(parseInt($("nav").css("left"),10)>=-350 && parseInt($("nav").css("left"),10)<=0){
            console.log(localScreenX-previousScreenX);
            var left=parseInt($("nav").css("left"),10)+(localScreenX-previousScreenX);
            if(left>0){
                left=0;
            }
            if(left<-350){
                left=-350;
            }
            $("nav").css("left",left);
        }
        previousScreenX=localScreenX;
    });
    $(document).on("touchend", function (evt) {
        previousScreenX=0;
        if(parseInt($("nav").css("left"),10)<-175){
            $("nav").animate({
                left: -350
            }, 100);
        }else if(parseInt($("nav").css("left"),10)>=-175){
            $("nav").animate({
                left: 0
            }, 100);
        }else{
            
        }
    });
    */
    
    
    $('[href="https://adprip.fr/"]').click(function(){
        return confirm("Attention. Le site adprip.fr fournit un chiffre brut et une estimation. Nous ne pouvons vérifier que le chiffre brut. L'estimation  affichée sur adprip.fr n'est pas expliquée et utilise des données non sourcées. A un moment dans le calcul, il utilise un facteur 1.0125, correspondant au nombre de signatures non affichées, mais on ne sait pas d'où sort ce facteur. Il a été tantôt indiqué comme le résultat d'un calcul basé sur des données de l'INSEE, alors que les bases de données de l'INSEE ne permettent pas de faire ce calcul. Puis, les développeurs indiquent maintenant qu'il s'agit d'un calcul basé sur des données privées, sans fournir de détail. Malgré plusieurs demandes de transparence (c.f. https://gitlab.com/pierrenn/adprip/issues/68), il est, à l'heure actuelle, difficile de se fier à cette estimation.");
    });
});


function afficherAlerte(message){
    $("#toast").html("<span>"+message+"</span>").css("opacity",1);
    if($(window).scrollTop()<60) {
        $("#toast").animate({
            bottom: $("footer").outerHeight()+20
        }, 300);
    }else{
        $("#toast").animate({
            bottom: 20
        }, 300);
    }
    
    setTimeout(function(){
        $("#toast").animate({
            opacity: 0
        }, 600);
        setTimeout(function(){
            $("#toast").css({
                bottom:-200
            });
        },610);
    },3000);
}