<?php 
include("preprocess.php");
$title="Informations sur le compteur collaboratif";
$description="Toutes les informations utiles sur le compteur collaboratif rip-le-compteur.dav.li !";
$custom_header='<link  href="lib/fotorama-4.6.4/fotorama.css" rel="stylesheet">';
include("head.inc");

$now=new DateTime("now");
if(intval($now->format("H"))>10){
    $aujourdhuiapres10h=$now->format("d\/m\/y");
}else{
    $hier=$now;
    $hier=$hier->modify("-1 day");
    $aujourdhuiapres10h=$hier->format("d\/m\/y");
}

?>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/ampoule.png" alt="emoji ampoule" /> F.A.Q.</h2>
        <p>Questions/réponses</p>
    </section>
    <section>
    <?php
        $faqCompteur=[
            "Qu'est-ce que propose le site web rip-le-compteur.dav.li~?"=>"<p>Ce site web propose un compteur collaboratif des signatures pour le référendum ADP. Il est collaboratif car, en plus d'être un code sous licence libre et créé de façon participative, le receuille des données est collaboratif.</p>
            <p>Ce compteur met l'emphase sur la transparence de la production des données et sur la qualité de ces données. Ainsi, ce compteur multiplie les sources en agrègeant toutes les données disponibles et propose un vérificateur permettant à tout un chacun de vérifier le comptage. La base de données de ce compteur est accessible à tou·te·s et libre d'utilisation car sous licence Creative Commons Attribution. Ce compteur publie l'intégralité des données nécessaires au calcul du chiffre en publiant son cache (les données qu'il utilise en temps réel).</p>",
            "D'où proviennent les données de rip-le-compteur.dav.li~?"=>"<p>Les signatures sont détectées lorsque des contributeurs visitent les pages du site web du Ministère de l'Intérieur dans la section \"<a href=\"https://www.referendum.interieur.gouv.fr/consultation_publique/8\" target=\"_blank\" rel=\"noopener noreferrer\">Consultation des soutiens déposés</a>\". L'extension de navigateur installée sur leur navigateur détecte les signatures, les comptent et les envoient vers ce site web qui centralise toutes les contributions. Les contributeurs doivent visiter les dernières pages de chaque digramme (couple de lettres) pour obtenir le nombre précis de signatures.</p>",
            "Comment devient-on contributeur·rice~? Qui sont les contributeur·rice·s~?"=>"<p>Tout le monde peut être contributeur après avoir installé une extension de navigateur <a href=\"https://addons.mozilla.org/en-US/firefox/addon/rip-le-compteur/\" target=\"_blank\">disponible pour Firefox</a> et <a href=\"https://chrome.google.com/webstore/detail/rip-le-compteur/hoopnflmlpfbbmmfiocfghejeecenbkn\" target=\"_blank\">aussi pour Chrome</a>. Puis, visitez la page \"<a href=\"contribuer\">Contribuer</a>\" pour savoir quel digramme est à mettre à jour ! Chaque jour à 10h du matin, le site web du Ministère est mis à jour. Par conséquent, c'est à ce moment là qu'on a le plus besoin de contributions.</p>
            <p>Lorsque vous êtes contributeur·rice, rendez-vous sur la page \"<a href=\"contribuer\">Contribuer</a>\" après 10h chaque jour et visiter des digrammes. Attention, il ne faut visiter que la dernière page de chaque digramme. Il faut visiter chaque digramme (AA, AB, AC...) mais il est inutile de visiter chaque page (1, 2, 3...). Cliquez sur le bouton \"<strong>>></strong>\" en bas de la page pour accéder à la dernière page !</p>",
            "Comment est calculé le nombre de signatures~? Pourquoi est-ce une estimation~?"=>"<p>Le calcul du total a changé au fur et à mesure des modifications du site web du Ministère. Actuellement, le nombre total de signatures est calculé en additionnant les signatures comptées sur chaque digramme (AA, AB, AC, AD, etc.). Ensuite, une estimation est faite sur les signatures non affichées.</p>
            <p>Nous avons remarqué le 25 juin une différence entre le comptage via la méthode simplifiée mais <a href=\"https://www.liberation.fr/checknews/2019/06/26/referendum-adp-l-interieur-casse-le-compteur_1736096\" target=\"_blank\" rel=\"noopener noreferrer\">bloquée par le Ministère ce même jour</a>, et le chiffre de signatures comptées collaborativement. Nous avions compté 373 772 signatures alors que le site du Ministère affichait une liste de 378 477 signatures juste avant que cette liste ne soit rendue inaccessible. Cette différence de 4705 signatures peut être due à une erreur de comptage de notre part (mais cela est peu probable) ou à des erreurs du côté du Ministère. En effet, nous avons remarqué que leur site web n'affichait pas les signataires qui ont un nom d'un caractère, les signataires qui ont une lettre avec accent dans l'une des deux premières lettres de leur nom et lorsqu'il y avait des erreurs typographiques (comme une espace avant la première lettre). Par conséquent, au nombre de signatures détectées, il est ajouté une estimation du nombre de signatures non-affichées calculée comme ceci :<br/>
            <strong>total_avec_estimation = signature_détectées_aujourdhui * ( 1 + ( signatures_méthode_une_page_25juin - signatures_méthode_collab_25juin ) / signatures_méthode_collab_25juin )</strong><br/>
            <i>Avec </i>signatures_méthode_une_page_25juin<i>, le nombre total de signatures calculé via l'ancienne méthode au 25 juin, soit la valeur constante : 378477.</i><br/>
            <i>Avec </i>signatures_méthode_collab_25juin<i>, le nombre total de signatures détectées collaborativement au 25 juin, soit la valeur constante : 373772.</i><br/>
            <i>Avec </i>signatures_détectées_aujourdhui<i>, les signatures détectées au moment du calcul.</i><br/>
            <i>Donc, avec les données d'aujourd'hui :</i><br/>
            <strong>total_avec_estimation = $total_digramme * ( 1 + ( 378477 - 373772 ) / 373772 )</strong><br/>
            <i>Soit :</i><br/>
            <strong>total_avec_estimation = $total_digramme * ( 1 + 4705 / 373772 )</strong><br/>
            <i>Soit (avec arrondi de la division à 4 décimales) :</i><br/>
            <strong>total_avec_estimation = $total_digramme * ( 1 + 0.0126 )</strong><br/>
            <i>Et donc :</i><br/>
            <strong>total_avec_estimation = ".($total_digramme+$estimation_signatures_nonaffichees)."</strong></p>
            <p>Chiffre qui est au final arrondi au millier inférieur, pour ne pas surévaluer le résulat. Comme nous n'avons la différence de signatures entre la méthode une page (bloquée par le Ministère) et la méthode collaborative que pour un seul jour (le 25 juin), cette statistique reste moyennement précise mais donne, à l'heure actuelle, des résultats convaincants.</p>",
            "Ce compteur est-il fiable~? Est-il mieux que les autres compteurs~?"=>"<p>Ce compteur est collaboratif. Des contributeurs volontaires envoient les données affichées sur le site web. Il y a donc un risque que ces données soient manipulées ou erronées. Cependant, des sécurités sont en place et, pour le moment, aucune contribution n'est apparue comme incohérente. Pour résoudre les captcha (système anti-robot), ce site web n'utilise pas de programme automatique, mais se base sur les contributions de volontaires. Ainsi, ce compteur sera toujours accessible, même si le Ministère rend leur système anti-robot plus complexe. Aussi, ce compteur n'utilise pas de travailleurs pauvres comme le fait adprip.fr qui fait appel à des micro-travailleurs du <a href=\"https://anti-captcha.com/\" target=\"_blank\" rel=\"noopener noreferrer\">Venezuela, de l'Indonésie, du Vietnam ou encore de l'Inde</a> pour résoudre les captchas.<br/>Sur ce compteur, uniquement les volontaires font le travail. Et les données produites par ces contributeurs sont à tout le monde (car publiées sous licence Creative Commons Attribution) ! Pour faire simple, sur ce compteur, c'est comme si vous comptiez vous-même les signatures mais, qu'au lieu de les garder pour vous, vous partagiez vos chiffres. Dans cet esprit de coopération, l'intégralité du code source de ce site web est <a href=\"https://framagit.org/DavidLibeau/rip-le-compteur\" target=\"_blank\" rel=\"noopener noreferrer\">open source et libre</a>. Bien évidement, nous veillons à respecter votre vie privée le plus possible lorsque vous utilisez ce site web.</p>
            <p>Le vérificateur permet de vérifier les données avec tous les autres compteurs et même avec vos propres données. En comparant les sources, cela permet ainsi d'être certain de la validité des chiffres avancés.</p>",
            "Si les données des autres compteurs sont fiables, pourquoi continuer de résoudre les captchas tous les jours~?"=>"<p>La résolution quotidienne des captchas pour récupérer les données sur le site du ministère n'est plus obligatoire. Cela permet simplement de vérifier les données des autres compteurs mais cette tâche fastidieuse est facultative car ce compteur utilise les données disponibles sur les autres compteurs pour se mettre à jour automatiquement.</p>
            <p>En revanche, comme ce compteur utilise des contributions et non un algorithme de reconnaissance d'image, il sera toujours utilisable. En effet, dans le futur, le ministère de l'Intérieur pourrait complexifier ses systèmes anti-robot rendant le comptage indisponible avec les algorithmes des autres compteurs. si tel était le cas, ce compteur collaboratif pourrait être le seul à fonctionner. C'est pour cela qu'il reste en ligne.</p>",
            "Je suis développeur·euse, comment j'utilise les données de ce compteur~?"=>"<p>Les données brutes sont accessibles ici : <a href=\"data/data.txt\" target=\"_blank\">base de données compteur</a>, <a href=\"data/error.txt\" target=\"_blank\">base de données erreur</a>, <a href=\"data/data-aggregate.txt\" target=\"_blank\">base de données agrégation</a>. Une API est <a href=\"https://framagit.org/DavidLibeau/rip-le-compteur/blob/master/serveur/api\" target=\"_blank\" rel=\"noopener noreferrer\">également disponible</a>. Voici des exemples : <a href=\"api.php?endpoint=cache\" target=\"_blank\">cache intégral</a>, <a href=\"https://rip-le-compteur.dav.li/api.php?endpoint=total\" target=\"_blank\">total maintenant</a>, <a href=\"https://rip-le-compteur.dav.li/api.php?endpoint=total&date=19/06/19-01:00:00\" target=\"_blank\">total à date</a>, <a href=\"api.php?endpoint=stats\" target=\"_blank\">statistiques</a> ! Vous pouvez utiliser ces données librement (licence Creative Commons Attribution) dans la limite du raisonable.</p><p>Un jeu de données a également été publié sur la plateforme DataGouv : \"<a href=\"https://www.data.gouv.fr/fr/datasets/signatures-pour-le-referendum-dadp-par-jour/\" target=\"_blank\" rel=\"noopener noreferrer\">Signatures pour le référendum d'ADP par jour </a>\".</p>"
        ];
    ?>
        <?php
        foreach($faqCompteur as $question => $reponse){
        ?>
            <h3><?php echo(str_replace("~","&nbsp;",$question)); ?></h3>
            <?php echo(str_replace("~","&nbsp;",$reponse)); ?>
        <?php
        }
        ?>
        <h3>Revue de presse</h3>
        <p>
            Voici quelques unes des citations dans la presse de ce compteur :
        </p>
        <ul class="linkList">
            <li><a href="https://www.liberation.fr/checknews/2019/06/19/referendum-adp-le-compteur-approche-des-200-000-soutiens_1734734" target="_blank" rel="noopener noreferrer">(19/06/19) Référendum ADP : le compteur approche des 200 000 soutiens</a></li>
            <li><a href="https://www.liberation.fr/checknews/2019/06/21/referendum-adp-le-compteur-approche-les-260-000-soutiens_1735247" target="_blank" rel="noopener noreferrer">(21/06/19) Référendum ADP : le compteur approche les 260 000 soutiens </a></li>
            <li><a href="https://www.nouvelobs.com/politique/20190621.OBS14749/deja-plus-de-250-000-signatures-pour-le-referendum-sur-la-privatisation-d-adp.html" target="_blank" rel="noopener noreferrer">(21/06/19) Déjà plus de 250 000 signatures pour le référendum sur la privatisation d’ADP</a></li>
            <li><a href="https://www.liberation.fr/checknews/2019/06/22/referendum-adp-le-compteur-depasse-les-286-000-soutiens-et-les-6_1735509" target="_blank" rel="noopener noreferrer">(22/06/19) Référendum ADP : le compteur dépasse les 286 000 soutiens (et les 6%)</a></li>
            <li><a href="https://www.liberation.fr/checknews/2019/06/26/referendum-adp-l-interieur-casse-le-compteur_1736096" target="_blank" rel="noopener noreferrer">(26/06/19) Référendum ADP : l'Intérieur casse le compteur</a></li>
            <li><a href="https://www.nouvelobs.com/societe/20190626.OBS14951/le-ministere-de-l-interieur-bloque-le-compteur-du-referendum-adp.html" target="_blank" rel="noopener noreferrer">(26/06/19) Le ministère de l’Intérieur bloque le compteur du référendum ADP</a></li>
            <li><a href="https://www.liberation.fr/checknews/2019/06/26/referendum-adp-le-compteur-atteint-les-390-000-soutiens_1736266" target="_blank" rel="noopener noreferrer">(26/06/19) Référendum ADP : le compteur atteint les 390 000 soutiens</a></li>
            <li><a href="https://www.liberation.fr/checknews/2019/06/27/referendum-adp-le-compteur-passe-la-barre-des-400-000-soutiens_1736492" target="_blank" rel="noopener noreferrer">(27/06/19) Référendum ADP : le compteur passe la barre des 400 000 soutiens</a></li>
            <li><a href="https://www.lesnumeriques.com/vie-du-net/rip-ministere-interieur-tue-compteurs-soutiens-n88783.html" target="_blank" rel="noopener noreferrer">(28/06/19) Privatisation d'ADP : l'Intérieur tue les compteurs de soutiens</a></li>
            <li><a href="http://www.lamarseillaise.fr/analyses-de-la-redaction/dossier-du-jour/77250-referendum-d-initiative-citoyenne-le-cap-des-10-de-signatures-franchi" target="_blank" rel="noopener noreferrer">(09/07/19) Référendum d’initiative citoyenne : le cap des 10 % de signatures franchi</a></li>
            <li><a href="http://www.lefigaro.fr/conjoncture/lance-depuis-un-mois-le-referendum-sur-la-privatisation-d-adp-patine-20190724" target="_blank" rel="noopener noreferrer">(24/07/19) Lancé depuis un mois, le référendum sur la privatisation d’ADP patine</a></li>

        </ul>
        <p>
            Pour suivre l'évolution de ce site web, David Libeau (le principal développeur de ce site web) publie des <a href="https://twitter.com/DavidLibeau/status/1140884206983241728" target="_blank" rel="noopener noreferrer">tweets de mise à jour ici</a>.
        </p>
    </section>
    <section class="centered">
            <p><a href="contribuer" class="btn">Contribuer !</a></p>
    </section>

</main>
<?php include("footer.inc"); ?>
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [
    <?php
    $quetionNb=0;
    foreach($faqCompteur as $question => $reponse){
        if($quetionNb!=0){
            echo(",");
        }
        $quetionNb++;
        ?>
        {
            "@type": "Question",
            "name": "<?php echo(str_replace("~","&nbsp;",$question)); ?>",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "<?php echo(str_replace("~","&nbsp;",htmlspecialchars($reponse))); ?>"
            }
        }
        <?php
    }
    ?>]
}
</script>
    <script>
        $(function() {
        });
    </script>
</body>

</html>