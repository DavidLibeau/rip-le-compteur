<?php include("preprocess.php");

if(strpos($_SERVER["REQUEST_URI"],".rss")){
    $formatDate="D, d M Y H:i:s T";
}else{
    $formatDate="d/m/y";
}

$items=[];

$now=new DateTime("now");
if(intval($now->format("H"))>10 && intval($now->format("i"))>30){
    $aujourdhuiapres10h=$now;
    $aujourdhuiapres10h=$aujourdhuiapres10h->setTime(10,00,0);
    if(sizeof($bdd_maj_priorite)==0){ 
        $id=$aujourdhuiapres10h->getTimestamp();
        $items[$id]=[
            "title"=>number_format($total, 0, ',', '&#160;')." signatures aujourd'hui",
            "description"=>"Nous comptons ".number_format($total, 0, ',', '&#160;')." signatures aujourd'hui pour le référendum ADP, soit ".number_format($pourcentage, 2, ',', '')."&#160;% des 4&#160;717&#160;396 signatures nécessaires.",
            "link"=>"https://rip-le-compteur.dav.li/notifications_".$id,
            "date"=>$aujourdhuiapres10h->format($formatDate),
            "dateIso"=>$aujourdhuiapres10h->format(DateTime::ATOM),
            "keywords"=>["ChiffreDuJour"]
        ];
    }
}


foreach($stats as $date => $stat){
    $id=DateTime::createFromFormat('d/m/y H:i:s', $date." 10:00:00")->getTimestamp();
    while(isset($items[$id])){
        $id++;
    }
    $items[$id]=[
        "title"=>number_format($stat["total"], 0, ',', '&#160;')." signatures aujourd'hui",
        "description"=>"Nous comptons ".number_format($stat["total"], 0, ',', '&#160;')." signatures aujourd'hui pour le référendum ADP, soit ".number_format($stat["pourcentage"], 2, ',', '')."&#160;% des 4&#160;717&#160;396 signatures nécessaires.",
        "link"=>"https://rip-le-compteur.dav.li/notifications_".$id,
        "date"=>DateTime::createFromFormat("d/m/y H:i:s", $date." 10:00:00")->format($formatDate),
        "dateIso"=>DateTime::createFromFormat("d/m/y H:i:s", $date." 10:00:00")->format(DateTime::ATOM),
        "keywords"=>["ChiffreDuJour"]
    ];
}

if(!empty($_GET["filter"])){
    if(is_numeric($_GET["filter"])){
        $items=[$items[$_GET["filter"]]];
    }else{
        $itemsFiltered=[];
        foreach($items as $id => $item){
            $inKeywords=false;
            foreach($item["keywords"] as $keyword){
                if($keyword==$_GET["filter"]){
                    $inKeywords=true;
                }
            }
            if($inKeywords){
                $itemsFiltered[$id]=$item;
            }
        }
    }
}

krsort($items);
    
if(!empty($_GET["format"]) && $_GET["format"]=="json"){
    header('Content-Type: application/json');
    echo(json_encode($items));
}else{

$filtersDescription=[
    "ChiffreDuJour"=>"Les statistiques quotidiennes du compteur de signatures."
];


if(strpos($_SERVER["REQUEST_URI"],".rss")){
    header("Content-type: text/rss+xml");
    echo('<?xml version="1.0" encoding="UTF-8" ?>');
}else{
    header("Content-type: text/xml");
    echo('<?xml version="1.0" encoding="UTF-8" ?>');
    echo('<?xml-stylesheet type="text/xsl" href="notifications.style.xsl"?>');
}


?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

    <channel>
<?php if(!empty($_GET["filter"])){ ?>
<?php if(isset($filtersDescription[$_GET["filter"]])){ ?>
        <title>Notifications #<?php echo($_GET["filter"]); ?></title>
        <link>https://rip-le-compteur.dav.li/notifications_<?php echo($_GET["filter"]); ?></link>
        <description><?php echo($filtersDescription[$_GET["filter"]]); ?></description>
        <category><?php echo($_GET["filter"]); ?></category>
        <atom:link href="https://rip-le-compteur.dav.li/notifications_<?php echo($_GET["filter"]); ?>" rel="self" type="application/rss+xml" />
<?php }elseif(is_numeric($_GET["filter"])){ ?>
        <title>Élément @<?php echo($_GET["filter"]); ?></title>
        <link>https://rip-le-compteur.dav.li/notifications_<?php echo($_GET["filter"]); ?></link>
        <description></description>
        <category>SoloItem</category>
        <atom:link href="https://rip-le-compteur.dav.li/notifications_<?php echo($_GET["filter"]); ?>" rel="self" type="application/rss+xml" />
<?php } }else{ ?>
        <title>Notifications</title>
        <link>https://rip-le-compteur.dav.li/notifications</link>
        <description>Toutes les notifications du compteur collaboratif des signatures du référendum ADP.</description>
        <atom:link href="https://rip-le-compteur.dav.li/notifications" rel="self" type="application/rss+xml" />
<?php } ?>

        <language>fr-fr</language>
<?php foreach($items as $id => $item){ ?>
        <item>
            <guid isPermaLink="false"><?php echo($id); ?></guid>
            <title><?php echo($item["title"]); ?></title>
            <link><?php echo($item["link"]); ?></link>
            <pubDate><?php echo($item["date"]); ?></pubDate>
<?php if(sizeof($items)==1){ ?>
            <dateIso><?php echo($item["dateIso"]); ?></dateIso>
<?php } ?>
            <description><?php echo($item["description"]); ?></description>
<?php foreach($item["keywords"] as $keyword){ ?>
            <category><?php echo($keyword); ?></category>
<?php } ?>
        </item>
<?php } ?>
    </channel>

</rss>
<?php } ?>