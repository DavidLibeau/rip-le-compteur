if (!browser) {
    var browser = chrome;
}

console.log("RIP, le compteur.");

if(location.href.indexOf("rip-le-compteur.dav.li/comptage_perso")!=-1){
    $("#contenu_custom").html('<p><i class="fa fa-refresh fa-spin fa-fw"></i> Chargement en cours...</p>');
    browser.storage.local.get(null,function(data){
        var alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var compteur="";
        var boutons="";
        var pourcentageTemporaire=0;
        var body="";
        if(data["A_"]){
            var bdd_digrammes={};
            var bdd_digrammes_csv="";
            var bdd_digrammes_html="";
            var total=0;
            for(var i=0; i<26; i++){
                section=alphabet[i]+"_";
                bdd_digrammes[section]=data[section];
                bdd_digrammes_csv+=section+","+data[section]+"\n";
                if(data[section]==undefined){
                    bdd_digrammes_html+="<tr><td>"+section+"</td><td>?</td></tr>";
                }else{
                    bdd_digrammes_html+="<tr><td>"+section+"</td><td>"+data[section]+"</td></tr>";
                }
                if(data[section]=="-" || data[section]==undefined){
                    pourcentageTemporaire++;
                }else{
                    total+=data[section];
                }
                for(var j=0; j<26; j++){
                    section=alphabet[i]+alphabet[j];
                    bdd_digrammes[section]=data[section];
                    bdd_digrammes_csv+=section+","+data[section]+"\n";
                    if(data[section]==undefined){
                        bdd_digrammes_html+="<tr><td>"+section+"</td><td>?</td></tr>";
                    }else{
                        bdd_digrammes_html+="<tr><td>"+section+"</td><td>"+data[section]+"</td></tr>";
                    }
                    if(data[section]=="-" || data[section]==undefined){
                        pourcentageTemporaire++;
                    }else{
                        total+=data[section];
                    }
                }
            }
            var comptageTemporaire="";
            if(pourcentageTemporaire>0){
                console.log(pourcentageTemporaire);
                comptageTemporaire="(Comptage temporaire : "+Intl.NumberFormat("fr-FR",{style:"percent", minimumFractionDigits:2}).format((702-pourcentageTemporaire)/702)+") ";
            }
            boutons+='<p class="centered"><a class="btn" href="data:text/csv;charset=utf-8,'+encodeURI(bdd_digrammes_csv)+'" download="comptage_signatures_RIP.csv"><i class="fa fa-download" aria-hidden="true"></i> Télécharger vos données par digramme en format CSV</a></p>';
            body='<div class="content"><table><thead><tr><th>Digramme</th><th>Comptage</th></tr></thead><tbody>'+bdd_digrammes_html+'</tbody></table></div>';
            compteur+='<h1>'+Intl.NumberFormat("fr-FR",{useGrouping:true}).format(total)+'</h1><p class="centered">'+comptageTemporaire+'Signatures comptées pour le référendum ADP. Soit '+Intl.NumberFormat("fr-FR",{style:"percent", minimumFractionDigits:2}).format(total/4717396)+' des 4 717 396 signatures nécessaires.</p>';
        }else{
            body='<p style="font-weight:bold"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aucune donnée à afficher.</p><p>Vous avez bien installé l\'extension mais pas encore lancé d\'extraction.</p><p>Cliquez sur l\'icone de l\'extension en haut à droite du navigateur. Allez sur l\'onglet "Extraire", puis lancez l\'extraction.</p><p>Vous pouvez paramétrer une extraction complète (beaucoup plus long) ou rester sur un comptage simple (les données géographiques ne seront pas téléchargées). En ajoutant plusieurs onglets, l\'extraction sera plus rapide.</p><p>Revenez ensuite sur cette page pour télécharger vos données.</p>';
        }
        if(data[":A_:1"]){
            var bdd_complet={};
            var bdd_geo={};
            for(d in data){
                if(d[0]==":"){
                    var section=d.replace(":","");
                    bdd_complet[section]=data[d];
                    for(v in data[d]){
                        var ville = data[d][v];
                        if(bdd_geo[ville]){
                            bdd_geo[ville]++;
                        }else{
                            bdd_geo[ville]=1;
                        }
                    }
                }
            }
            boutons+='<p class="centered"><a class="btn" href="data:text/json;charset=utf-8,'+encodeURI(JSON.stringify(bdd_complet))+'" download="signatures_RIP_complet.json"><i class="fa fa-download" aria-hidden="true"></i> Télécharger vos données complètes en format JSON</a></p>';
            boutons+='<p class="centered"><a class="btn" href="data:text/json;charset=utf-8,'+encodeURI(JSON.stringify(bdd_geo))+'" download="signatures_RIP_communes.json"><i class="fa fa-download" aria-hidden="true"></i> Télécharger vos données par commune en format JSON</a></p>';
        }
        $("#contenu_custom").html(compteur+boutons+body);
    });
}else{
    if($('[alt="Maintenance"]+h2').text()=="Maintenance en cours"){
        setTimeout(function(){
            window.location.reload();
        },Math.floor((Math.random() * 500) + 10));
    }
    var compteur;
    var nb_pages;
    var is_last;

    var urlpath = location.pathname.split("/")
    var section = "??";
    var compteur = "??"
    var serveur = "??";
    if (urlpath[4].length == 1 && urlpath[3]==urlpath[4]) {
        section = urlpath[4]+"_";
    } else if (urlpath[4].length == 2 && urlpath[3]==urlpath[4][0]) {
        section = urlpath[4];
    }

    browser.storage.local.get(null,function(data){
        if(data.tab){
            if(data.tab=="contrib"){
                compter(true);
            }else if(data.tab=="extraire"){
                if(data.extraction && (data.extraction=="enCours" || data.extraction=="terminée")){
                    compter(false);
                }
            }
        }
    });

    browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.data == "ping") {
            sendResponse({
                section: section,
                signatures: compteur,
                serveur: serveur
            });
        }else if (request.data == "lancer-extraction") {
            continuerExtraction();
        }
    });

    var alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    function continuerExtraction(){
        browser.storage.local.get(null, function(data) {
            if(data.extraction && (data.extraction=="enCours" || data.extraction=="terminée")){
                var section=undefined;
                var fin=true;
                for(var i=0; i<26; i++){
                    section=alphabet[i]+"_";
                    if(data[section]==undefined){
                        fin=false;
                        break;
                    }
                    for(var j=0; j<26; j++){
                        section=alphabet[i]+alphabet[j];
                        if(data[section]==undefined){
                            fin=false;
                            break;
                        }
                    }
                    if(data[section]==undefined){
                        fin=false;
                        break;
                    }
                }
                if(fin){
                    if(data[section]!="-"){
                        console.log(data);
                        browser.storage.local.set({extraction:"terminée"}, function(){
                            console.log("Extraction terminée !");
                        });
                    }else{
                        console.log("Extraction terminée mais en attente des autres onglets...");
                    }
                }else{
                    var comptage={};
                    comptage[section]="-";
                    browser.storage.local.set(comptage, function(){
                        if(data.extraction=="enCours"){
                            console.log(">> "+section);
                            section=section.replace("_","");
                            window.location="https://www.referendum.interieur.gouv.fr/consultation_publique/8/"+section[0]+"/"+section;
                        }
                    });
                }
            }
        });
    }

    function compter(contribuer=true){
        if ($("#formulaire_consultation_publique>.well:nth-of-type(4)>:first-child").prop("tagName") == "TABLE" || $("#formulaire_consultation_publique>.well:nth-of-type(4) h2").text() == "Aucun soutien n'est accepté jusqu'à ce jour") {
            if (section == undefined) {
                console.error("RIP, le compteur : section undefined");
            } else {
                compteur=$("#formulaire_consultation_publique>.well:nth-of-type(4) tbody>tr").length;
                nb_pages=1;
                is_last=1;
                if ($(".pagination").length != 0) {
                    if ($(".pagination .last a").length || compteur == 0) { //évaluation du nombre de soutien sur la base du nb de pages / compteur==0 pour le cas où on est bien trop loin
                        nb_pages = parseInt($(".pagination .last a").attr("href").split("=")[1]);
                        compteur = nb_pages * 200 - 100;
                        is_last = 0;
                    } else { //Cas de la consultation d'une dernière page
                        nb_pages = parseInt($(".pagination .current").text());
                        compteur = (nb_pages - 1) * 200 + $("#formulaire_consultation_publique>.well:nth-of-type(4) tbody>tr").length;
                    }
                }
                if ($("#formulaire_consultation_publique>.well:nth-of-type(4) h2").text() == "Aucun soutien n'est accepté jusqu'à ce jour") {
                    compteur = 0;
                }
                console.log(compteur);
                if(contribuer){
                    $.post("https://rip-le-compteur.dav.li/depot.php", {
                            consultationId: urlpath[2],
                            section: section,
                            compteur: compteur,
                            nb_pages: nb_pages,
                            is_last: is_last
                        },
                        function (returnedData) {
                            console.log(returnedData);
                            if (returnedData.indexOf("onnées reçues ! :)")) {
                                serveur = "ok";
                            } else {
                                serveur = "error";
                            }
                        }).fail(function (error) {
                        console.log(error);
                        serveur = "error";
                    });
                }else{
                    if(compteur>0){
                        browser.storage.local.get(null,function(data){
                            if(data.option_complet && data.option_complet=="true"){
                                var villes=[];
                                $("table>tbody td:nth-child(3)").each(function(){
                                    villes.push($(this).text());
                                });
                                var comptage={};
                                if($(".pagination .current").length){
                                    var currentPage=parseInt($(".pagination .current").text())
                                }else{
                                    var currentPage=1;
                                }
                                comptage[":"+section+":"+currentPage]=villes;
                                browser.storage.local.set(comptage,function(){
                                    if(is_last){
                                        var comptage={};
                                        comptage[section]=compteur;
                                        browser.storage.local.set(comptage,function(){
                                            setTimeout(continuerExtraction, Math.floor((Math.random() * 500) + 10));
                                        });
                                    }else{
                                        var pageSuivante=parseInt($(".pagination .next a").attr("href").split("=")[1])
                                        section=section.replace("_","");
                                        window.location="https://www.referendum.interieur.gouv.fr/consultation_publique/8/"+section[0]+"/"+section+"?page="+pageSuivante;
                                    }
                                });
                            }else{
                                if(is_last){
                                    var comptage={};
                                    comptage[section]=compteur;
                                    browser.storage.local.set(comptage,function(){
                                        setTimeout(continuerExtraction, Math.floor((Math.random() * 500) + 10));
                                    });
                                }else{
                                    section=section.replace("_","");
                                    window.location="https://www.referendum.interieur.gouv.fr/consultation_publique/8/"+section[0]+"/"+section+"?page="+nb_pages;
                                }
                            }
                        });
                    }else{
                        var comptage={};
                        comptage[section]=compteur;
                        browser.storage.local.set(comptage,function(){
                            setTimeout(continuerExtraction, Math.floor((Math.random() * 500) + 10));
                        });
                    }
                }
            }
        }
    }
}