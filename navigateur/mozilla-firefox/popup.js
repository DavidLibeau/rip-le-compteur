if (!browser) {
    var browser = chrome;
}

function refresh(){
    browser.tabs.query({active: true,currentWindow: true}, function (tabs) {
        var tab = tabs[0];
        if (tab.url.indexOf("referendum.interieur.gouv.fr/consultation_publique/8/")!=-1) {
            browser.tabs.sendMessage(tab.id, {data: "ping"}, function (response) {
                if(response){
                    var d = new Date();
                    lastMessage = d.getTime();
                    $("#localDigramme>span").html(response.section);
                    $("#signatures>span:first-child").html(response.signatures + " signatures comptées");
                    if (response.section == "??") {
                        $("#signatures>span:last-child").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Cliquez sur une section');
                        $("#extraire button").hide();
                        $("#extraire div:first-of-type").html('Pour extraire la liste des signatures :');
                        $("#extraire div:last-of-type").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Cliquez sur une section');
                    }
                    if (response.signatures != "??") {
                        if (response.serveur == "ok") {
                            $("#signatures>span:last-child").html('<i class="fa fa-check" aria-hidden="true"></i> Reçues par le serveur ! Merci');
                        } else if (response.serveur == "error") {
                            $("#signatures>span:last-child").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erreur d\'envoi');
                        } else {
                            $("#signatures>span:last-child").html('<i class="fa fa-cog fa-spin fa-fw"></i> En cours d\'envoi...');
                        }
                    }
                }else{
                    $("#signatures>span:last-child").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Résolvez le captcha');
                    $("#extraire button").hide();
                    $("#extraire div:first-of-type").html('Pour extraire la liste des signatures :');
                    $("#extraire div:last-of-type").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Résolvez le captcha ou rechargez la page');
                }
            });
            browser.storage.local.get(null,function(data){
                if(data.extraction){
                    extraction=data.extraction;
                    if(data.extraction=="enCours"){
                        browser.tabs.sendMessage(tab.id, {data: "lancer-extraire"}, function (response) {});
                    }
                }
            });
        }else{
            $("#signatures>span:last-child").html('<i class="fa fa-info-circle" aria-hidden="true"></i> Visitez la liste publique du RIP</span>');
            
            $("#extraire button").hide();
            $("#extraire div:first-of-type").html('Pour extraire la liste des signatures :');
            $("#extraire div:last-of-type").html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/A/A">Rendez-vous sur la liste publique du RIP</a> !');
            updateLinks();
        }

        $.get({
            url: "https://rip-le-compteur.dav.li/contribuer.php?popup",
            dataType: "json"
        }).done(function (data) {
            if (data.a_faire > 0) {
                $("#nextDigramme>a").attr("href", data.lien);
                $("#nextDigramme>a").html(data.digramme + '<i class="fa fa-angle-double-right" aria-hidden="true"></i>');
                browser.browserAction.setBadgeText({text: "" + data.a_faire});
                browser.browserAction.setBadgeTextColor({color: "#3c3c3c"});
                browser.browserAction.setBadgeBackgroundColor({color: "#ffdc5d"});
            } else {
                $("#nextDigramme>a").css("cursor", "not-allowed");
                if (tab.url.indexOf("referendum.interieur.gouv.fr/consultation_publique/8/")==-1) {
                    $("#signatures>span:last-child").html('<i class="fa fa-info-circle" aria-hidden="true"></i> Plus rien à compter !</span>');
                }
                browser.browserAction.setBadgeText({text: ""});
            }
            $("#a_faire").html(data.a_faire);
        });
    });
}

refresh();

setInterval(function(){
    if($("#signatures>span:last-child .fa-cog").length || $("#signatures>span:last-child .fa-exclamation-triangle").length){
        refresh();
    }
},1000);


function updateLinks(){
    $("a").off("click");
    $("a").click(function(evt){
        var link=$(this).attr("href");
        evt.preventDefault();
        if(link!=""){
            browser.tabs.query({active: true,currentWindow: true}, function (tabs) {
                var tab = tabs[0];
                if (tab.url.indexOf("referendum.interieur.gouv.fr/consultation_publique/8/")!=-1 && link.indexOf("dav.li")==-1) {
                    browser.tabs.update(tab.id, {url: link});
                }else{
                    browser.tabs.create({url: link});
                }
            });
        }
    });
}
updateLinks();


//tabs
$("nav li:first-child").click(function(){
    $(this).addClass("active");
    $("nav li:last-child").removeClass("active");
    $("#contrib").show();
    $("#extraire").hide();
    browser.storage.local.set({tab:"contrib"}, function(){});
});
$("nav li:last-child").click(function(){
    $(this).addClass("active");
    $("nav li:first-child").removeClass("active");
    $("#contrib").hide();
    $("#extraire").show();
    browser.storage.local.set({tab:"extraire"}, function(){});
    browser.storage.local.get(null,function(data){
        if(data.extraction){
            extraction=data.extraction;
            if(data.extraction=="enCours"){
                browser.tabs.sendMessage(tab.id, {data: "lancer-extraire"}, function (response) {});
            }
        }
    });
});


//options
$('[name="complet"]').click(function(){
    if($(this).is(":checked")){
        var checkedValue="true";
    }else{
        var checkedValue="false";
    }
    browser.storage.local.set({option_complet:checkedValue}, function(){}); 
});
$('[name="onglets"]').click(function(){
    browser.storage.local.set({option_onglets:$(this).val()}, function(){}); 
});


//button
var extraction="pause";
$("#extraire button").click(function(evt){
    if(extraction=="pause"){
        extraction="enCours";
        browser.storage.local.set({extraction:"enCours"}, function(){});
        browser.tabs.query({active: true,currentWindow: true}, function (tabs) {
            var tab = tabs[0];
            browser.tabs.sendMessage(tab.id, {data: "lancer-extraction"}, function (response) {});
        });
        browser.storage.local.get(null,function(data){
            if(data.option_onglets){
                for(var i=1;i<data.option_onglets;i++){
                    browser.tabs.create({url: "https://www.referendum.interieur.gouv.fr/consultation_publique/8/A/A"});
                }
            }
        });
    }else if(extraction=="enCours"){
        extraction="pause";
        browser.storage.local.clear(function(){});
        browser.storage.local.set({extraction:"pause"}, function(){});
        browser.storage.local.set({tab:"extraire"}, function(){});
    }else if(extraction=="terminée"){
        extraction="pause";
        browser.storage.local.clear(function(){});
        browser.storage.local.set({extraction:"pause"}, function(){});
        browser.storage.local.set({tab:"extraire"}, function(){});
    }
});


browser.storage.local.get(null,function(data){
    console.log(data);
    if(data.tab){
        if(data.tab=="contrib"){
            $("nav li:first-child").addClass("active");
            $("nav li:last-child").removeClass("active");
            $("#contrib").show();
            $("#extraire").hide();
        }else if(data.tab=="extraire"){
            $("nav li:first-child").removeClass("active");
            $("nav li:last-child").addClass("active");
            $("#contrib").hide();
            $("#extraire").show();
        }
    }else{
        browser.storage.local.set({tab:"contrib"}, function(){});
    }
    if(data.option_complet){
        if(data.option_complet=="true"){
            $('[name="complet"]').prop("checked",true);
        }else{
            $('[name="complet"]').prop("checked",false);
        }
    }
    if(data.option_onglets){
        $('[name="onglets"]').val(data.option_onglets);
    }
    if(data.extraction){
        extraction=data.extraction;
        if(data.extraction=="enCours"){
            $("#extraire button").attr("title","Annuler l'extraction");
            $("#extraire button").html('<i class="fa fa-times" aria-hidden="true"></i>');
            $("#extraire button").show();
            $("#extraire div").html("");
            $("#extraire div:first-of-type").html('<i class="fa fa-cog fa-spin fa-fw"></i> En cours d\'extraction...');
        }else if(data.extraction=="terminée"){
            $("#extraire button").attr("title","Effacer les données de la dernière extraction");
            $("#extraire button").html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            $("#extraire button").show();
            $("#extraire div").html("");
            $("#extraire div:first-of-type").html('<i class="fa fa-spell-check" aria-hidden="true"></i> Extraction terminée !');
            $("#extraire div:last-of-type").html('<i class="fa fa-download" aria-hidden="true"></i> <a href="https://rip-le-compteur.dav.li/comptage_perso">Téléchargez vos données</a>');
            updateLinks();
        }
    }
});